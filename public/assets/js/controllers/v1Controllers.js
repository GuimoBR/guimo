/**
 * Created by jorge on 22/11/2015.
 */
var v1Controllers = angular.module('v1Controllers',[]);

/**LANDING PAGE CONTROLLER**/
v1Controllers.controller('landingPageCtrl',['$scope','$http',
    function($scope,$http){
        $scope.data = null;
        $scope.emailForm = null;
        $scope.success  = null;
        $scope.error = null;

        $scope.sendEmail = function(){
            if($scope.emailForm.$valid){
                var formdata = {
                    email: $scope.data.email
                };
                $http.post('http://guimo.dev/emails/store',formdata).
                    success(function(response){
                        $scope.data = null;
                        $scope.success = response.message;
                    }).
                    error(function(response){
                        $scope.error = response.message;
                    });
            }
        };

        $('[data-typer-targets]').typer();

    }]);