<!DOCTYPE html>
<html>
<head>
    <title>Guimo Store</title>
    <meta charset="utf-8">
    <meta name="description" content="Guimo Store é a loja oficial para você comprar que quer Guimo agora mesmo!Acesse e veja todas as opções À venda">
    <meta name="abstract" content="Guimo o smart toy revolucionário e educativo que você sempre quis ter">
    <meta name="keywords" content="smart,toy,guimo,smartoy,smart-toy,startup,revolucionar,startup,brinquedo,criança,child,inteligente,kids,educativo,divertido">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(env('APP_ENV') == 'production')
    <link rel="shortcut icon" type='image/x-icon' href="{{secure_asset('assets/images/ico/favicon.ico')}}">
    @else
        <link rel="shortcut icon" type='image/x-icon' href="{{asset('assets/images/ico/favicon.ico')}}">
    @endif



    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/css/bootstrap-material-design.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/css/ripples.min.css">
        @if(env('APP_ENV') == 'production')
            <link rel="stylesheet" href="{{secure_asset('assets/css/store.css')}}">
        @else
            <link rel="stylesheet" href="{{asset('assets/css/store.css')}}">
        @endif
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    @yield('style')

</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('store.index')}}">
                @if(env('APP_ENV') == 'production')
                <img src="{{secure_asset('assets/images/logo_guimo_50px.png')}}" alt="Guimo" style="margin-top:-13px;">
                    @else
                    <img src="{{asset('assets/images/logo_guimo_50px.png')}}" alt="Guimo" style="margin-top:-13px;">
                    @endif

            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{route('store.index')}}">Guimo Store</a>
                </li>
                <li class="dropdown">
                    <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Categorias
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        @forelse($categories as $category)
                            <li><a href="{{route('store.category',['slug'=>$category->slug])}}">{{$category->name}}</a></li>
                            @empty
                            <li><a href="#">Nenhuma Categoria <i class="fa fa-frown-o"></i></a></li>
                        @endforelse
                    </ul>
                </li>
                <li>
                    <a href="#">Sobre</a>
                </li>
                <li>
                    <a href="#">Contato</a>
                </li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(!Session::has('id'))
                    <li><a href="{{route('cart.show',['cartid'=>'0'])}}"><i class="fa fa-shopping-basket fa-lg"></i> &nbsp; Cesta de Compras (0)</a></li>
                @else
                <li><a href="{{route('cart.show',['cartid'=>$session['id']])}}"><i class="fa fa-shopping-basket fa-lg"></i> &nbsp; Cesta de Compras ({{count($session['cart'])}})</a></li>
                @endif
                @if(is_null($user))
                <li><a href="{{route('login.index')}}">Login &nbsp; <i class="fa fa-sign-in fa-lg"></i></a></li>
                @else
                    <li class="dropdown">
                        <a data-target="#" class="dropdown-toggle" data-toggle="dropdown">Minha Conta
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('profile',['id'=>$user->id])}}"><i class="fa fa-user"></i> &nbsp; Perfil</a></li>
                            <!--<li><a href="javascript:void(0)">Another action</a></li>
                            <li><a href="javascript:void(0)">Something else here</a></li>
                            <li class="divider"></li>-->
                            <li><a href="{{route('logout')}}">Logout <i class="fa fa-sign-out"></i> </a></li>
                        </ul>
                    </li>
                        @endif
                <!--<li class="dropdown">
                    <a href="http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)">Action</a></li>
                        <li><a href="javascript:void(0)">Another action</a></li>
                        <li><a href="javascript:void(0)">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="javascript:void(0)">Separated link</a></li>
                    </ul>
                </li>-->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<div class="container-fluid">
    <ul class="breadcrumb">
        Você está em:
        @foreach($breadcumbs as $breadcumb)
            <li>
                @if(!is_null($breadcumb['slug']))
                <a href="{{route($breadcumb['link'],['cslug'=>$breadcumb['slug']])}}">{{$breadcumb['name']}}</a>
                    @else
                    {{$breadcumb['name']}}
                @endif
            </li>
        @endforeach
    </ul>

    <div class="row">

        <div class="col-lg-3 col-md-3 hidden-xs">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Menu</h3></div>
                <div class="panel-body">
                    <div class="list-group">
                        <a href="{{route('store.index')}}" class="list-group-item"><i class="fa fa-home"></i>  &nbsp; Home</a>
                        @if(is_null($user))
                            <a href="{{route('login.index')}}" class="list-group-item"><i class="fa fa-sign-in"></i>  &nbsp; Login </a>
                            @else
                            <a href="{{route('logout')}}" class="list-group-item"><i class="fa fa-sign-out"></i>  &nbsp; Logout </a>
                        @endif
                    </div>
                    <hr>
                    <div class="list-group">
                        <!--<a href="#" class="list-group-item">Category 1</a>
                        <a href="#" class="list-group-item">Category 2</a>
                        -->
                        <h3 class="panel-title">Categorias</h3>
                        @if(!is_null($categories))
                        @foreach($categories as $category)
                            <a href="{{route('store.category',['slug'=>$category->slug])}}" class="list-group-item"> <i class="fa fa-long-arrow-right"></i> &nbsp; {{ $category->name }}</a>
                        @endforeach
                            @else
                            <a href="#" class="list-group-item">Nenhuma categoria</a>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                @yield('content')
            <hr>
        </div>
    </div>
</div>
<div class="container-fluid">

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-8">
                <p class="text-left">Copyright &copy; Guimo {{date('Y')}}</p>
            </div>
            <div class="col-lg-4">
                <p class="text-right">Métodos de pagamento aceitos <br>
                    @if(env('APP_ENV') == 'production')
                    <img src="{{secure_asset('assets/images/payments_methods.png')}}" alt="Formas de pagamento" height="100">
                        @else
                        <img src="{{asset('assets/images/payments_methods.png')}}" alt="Formas de pagamento" height="100">
                        @endif
                </p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

@if(env('APP_ENV') == 'production')
<script src="{{secure_asset('assets/js/jquery.min.js')}}"></script>
<script src="{{secure_asset('assets/js/bootstrap.min.js')}}"></script>
@else
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
@endif
<script src="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js"></script>
@yield('script')

<script>
    $.material.init();
</script>


</body>
</html>