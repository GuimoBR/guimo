<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository
 * @package namespace Guimo\Repositories;
 */
interface ProductRepository extends RepositoryInterface
{
    //
}
