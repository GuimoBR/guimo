/** RESTRITO PAGE CONTROLLER **/

var dashboardControllers = angular.module('dashboardControllers',[]);

dashboardControllers.controller('AuthController',['$scope','$auth','$state','$http','ngToast','$cookies','$window',
    function($scope,$auth,$state,$http,ngToast,$cookies,$window,userService){


        $scope.restritoForm = null;
        $scope.data = null;
        $scope.message  = null;
        $scope.error = null;
        $scope.login = function(){

            if($scope.restritoForm.$valid){
                var toastLoging = ngToast.info({
                    content:'Logando..',
                    timeout:6000
                });
                var credentials = {
                    email: $scope.data.email,
                    password:$scope.data.password
                };

                $auth.login(credentials).then(function(data){
                    ngToast.dismiss(toastLoging);
                    toastLoging = ngToast.success({
                        content: 'sucesso',
                        animation:'slide'
                    });


                   $state.go('dashboard',{});
                });
            }
        };


    }]);

/** DASHBOARD PAGE CONTROLLER **/
dashboardControllers.controller('DashboardController',['$scope','$auth','$state','$http','$cookies','$window','ngToast',
    function($scope,$auth,$state,$http,$cookies,$window,ngToast,userService){
        $scope.interessados = null;
        $scope.emails = null;
        $scope.error = null;
            console.log(userService);
        if(!$auth.isAuthenticated()){
            $state.go('/');
        }else {

            $http.get('http://guimo.dev/emails').success(function (emails) {
                //console.log(emails)
            }).error(function (error) {
                //console.log(error);
            });
        }
    }]);
