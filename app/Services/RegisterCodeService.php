<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 23:32
 */

namespace Guimo\Services;


use Guimo\Entities\UserInfo;
use Guimo\Entities\User;
use Guimo\Repositories\RegisterCodeRepository;
use Guimo\Repositories\UserRepository;
use Guimo\Repositories\UserInfoRepository;
use Guimo\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;

class RegisterCodeService
{
    /**
     * @var UserRepository
     */
    protected $repository;
    /**
     * UserService constructor.
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(RegisterCodeRepository $repository)
    {

        $this->repository = $repository;

    }

    public function searchByToken($token,$situation){
        return $this->repository->findWhere(['token'=>$token,'is_accessible'=>$situation]);
    }

    public function all(){
        return $this->repository->all();
    }

    public function insert($data){
        $code = $this->repository->create($data);
        $code['created_at'] = $data['created_at'];
        return $code->save();
    }

    public function updateAccessCode($id,$data){
        $code = $this->repository->update($data,$id);
        $code['accessed_at'] = $data['accessed_at'];
        return $code->save();
    }


}
