<!DOCTYPE html>
<html lang="pt-br" ng-app="dashboardApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Guimo - Área Restrita</title>
    <link rel="stylesheet" href="{{asset('assets/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/icheck/flat/blue.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/restrito.css')}}">
    <link rel="shortcut icon" type='image/x-icon' href="{{asset('assets/images/ico/favicon.ico')}}">

</head>
<body>
<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">

        <div id="login" class="animate form">
            @if(count($errors)>0)
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                    </ul>
                </div>
            @endif
            <section class="login_content">
                <form method="post" name="login" action="{{route('login')}}">
                    <h1 class="text-primary">Guimo Login</h1>
                    {{csrf_field()}}
                    <div class="form-group has-error">
                        <input type="email" name="email" class="form-control" placeholder="Email" required />
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Senha" required/>
                    </div>
                    <div class="form-group">
                        <span><input type="checkbox" name="remember"> Permanecer Logado</span>
                    </div>
                    <div>
                        <button class="btn btn-primary" type="submit">Logar</button>
                        <!--<a class="reset_pass" href="#">Lost your password?</a>-->
                    </div>
                    <div class="clearfix"></div>
                    <div class="separator">

                        <!--<p class="change_link">New to site?
                            <a href="#toregister" class="to_register"> Create Account </a>
                        </p>-->
                        <div class="clearfix"></div>
                        <br />
                        <div>
                            <h2 class="text-primary"><img src="{{asset('assets/images/logo_guimo_50px.png')}}" alt="Guimo">Guimo</h2>

                            <p>©2015 All Rights Reserved. Guimo!</p>
                        </div>
                    </div>
                </form>
                <!-- form -->
            </section>
            <!-- content -->
        </div>
    </div>
</div>
<script src="{{asset('assets/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/icheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-red'
        });
    })
</script>
</body>
</html>