<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RegisterCode extends Model implements Transformable
{
    use TransformableTrait;

    public $timestamps = false;

    protected $table = 'register_code';

    protected $fillable = ['token','is_accessible'];

}
