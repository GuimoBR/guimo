@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Emails padrão criados ate o momento</h2>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @if($errors->has())

                        <div class="alert alert-callout alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p><strong>Atenção, alguns erros foram detectados:</strong></p>
                            <ul class="list-unestyled">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-callout alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p>{{Session::get('message')}}</p>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <form action="{{route('emails.postSend')}}" method="POST" class="form">
                            <div class="card-head">
                                <header>Emails</header>
                                <div class="tools">
                                    {{--<div class="btn-group">
                                        <a href="{{route('emails.status')}}" class="btn btn-default-dark btn-flat" type="submit">Status dos Envios <i class="fa fa-info-circle"></i></a>
                                    </div>--}}
                                    <div class="btn-group">
                                        <button class="btn btn-default-dark btn-flat ink-reaction" type="submit" @if(count($emailTypes) < 1) disabled @endif;>Enviar Emails <i class="fa fa-arrow-right"></i></button>
                                    </div>
                                </div>
                            </div><!--end .card-head -->
                            <div class="card-body style-default-bright">
                                {{csrf_field()}}
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control tags" data-role="tagsinput" name="emails" id="emails">
                                        <label for="emails" class="control-label">Destinatários </label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="emailType">Email a enviar: </label>
                                        <select name="emailType" id="emailType" class="form-control">
                                            @foreach($emailTypes as $emailType)
                                                <option value="{{$emailType->id}}">{{$emailType->name}} de {{$emailType->from}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Emails cadastrados</header>
                        </div>

                        <div class="card-body style-default-bright">
                            <table class="table table-stripped responsive-utilities" id="emails_table">
                                <thead>
                                <tr class="headings">

                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Data de Cadastro</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($userEmails as $email)
                                    @if($email->status >= 1)
                                        <tr class="even pointer">
                                            <td>
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" value="{{$email->id}}" data-email="{{$email->email}}" name="emailscheckbox">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                {{$email->email}}
                                            </td>
                                            <td>
                                                {{$email->created_at->format('d/m/Y H:i:s')}}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endsection

        @section('script')
            <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('materialadmin/assets/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
            <script>
                $(document).ready(function(){

                    $("input").click(function(){
                        if($(this).prop('checked')){
                            $("#emails").tagsinput('add',$(this).data('email'));
                        }else{
                            $("#emails").tagsinput('remove',$(this).data('email'));
                        }
                    });

                    var oTable = $('#emails_table').dataTable({
                        "dom": 'T<"clear">lfrtip',
                        "order": [[1, 'asc']],
                        "colVis": {
                            "buttonText": "Columns",
                            "overlayFade": 0,
                            "align": "right"
                        },
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                            "lengthMenu": '_MENU_ entries per page',
                            "search": '<i class="fa fa-search"></i>',
                            "paginate": {
                                "previous": '<i class="fa fa-angle-left"></i>',
                                "next": '<i class="fa fa-angle-right"></i>'
                            }
                        }
                    });

                });

            </script>
@endsection