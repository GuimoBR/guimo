{{csrf_field()}}
<div class="form-group floating-label">
    <label for="name" >Nome da categoria</label>
    <input type="text" class="form-control" name="name"  id="name" value="@if(!is_null($category)){{$category->name}}@endif">

</div>
<div class="form-group ">
    <label for="description">Descrição </label>
    <textarea name="description" class=" form-control" id="description" cols="30" rows="5">@if(!is_null($category)){!! $category->description !!}@endif</textarea>
</div>
<div class="form-group">
    <label for="slug">URL da Categoria</label>
    <input type="text" class="form-control" name="slug" id="slug" readonly>
</div>