@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/TableTools.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Lista de todas os produtos cadastrados</h2>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @if($errors->has())

                        <div class="alert alert-callout alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p><strong>Atenção, alguns erros foram detectados:</strong></p>
                            <ul class="list-unestyled">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-callout alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p>{{Session::get('message')}}</p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Categorias</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <a class="btn btn-default-dark btn-flat btn-block ink-reaction" data-toggle="modal" data-target="#new_category">Nova Categoria <i class="fa fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body style-default-bright">
                            <p class="text-danger">Atenção, alguns produtos podem estar cadastrados em <strong>mais de uma categoria</strong>, portanto, não considere o numero total de produtos cadastrados por categoria para calcular o total de produtos no geral.</p>
                            <div class="col-md-12">
                                <table class="table table-stripped table order-column table-striped hover" id="categories" data-swftools="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf')}}">
                                    <thead>
                                    <tr class="headings">

                                        <th class="text-center">#</th>
                                        <th class="text-center">Nome</th>
                                        <th class="text-center">Descrição</th>
                                        <th class="text-center">Nº de Produtos</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr class="text-center">
                                            <td>{{$category->id}}</td>
                                            <td><a href="{{route('admin.store.category',['slug'=>$category->slug])}}">{{$category->name}}  <i class="fa fa-edit"></i></a></td>
                                            <td>{{$category->description}}</td>
                                            <td>{{count($category->products)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="new_category">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Nova Categoria</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.store.new_category')}}" class="form" method="POST" id="create_category">
                                @include('store.form_new_category',['category'=>null])
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary" id="submit">Criar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('assets/js/string_to_slug.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script>
        $(document).ready(function(){

            $("#submit").click(function(){
                $("#create_category").submit();
            });

            $("#name").keyup(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            $("#name").change(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            @if(count($categories) > 0)
            var oTable = $('#categories').dataTable({
                "dom": 'T<"clear">lfrtip',
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "tableTools": {
                    "sSwfPath": $('#categories').data('swftools')
                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
            @endif

        });
    </script>
@endsection