<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->delete();
        $users = array(
            ['name'=>'Jorge David','email'=>'jorgejunior@guimo.toys','password'=>Hash::make('210191Jj'),'privileges'=>10],
            ['name'=>'Caio','email'=>'caiorocha@guimo.toys','password'=>Hash::make('Guimo2015'),'privileges'=>10],
            ['name'=>'Willian','email'=>'willianuehara@guimo.toys','password'=>Hash::make('Guimo2015'),'privileges'=>10],
            ['name'=>'Thiago','email'=>'thiagosouza@guimo.toys','password'=>Hash::make('Guimo2015'),'privileges'=>10]
        );

        foreach($users as $user){
            DB::table('users')->insert($user);
        }

        DB::table('users_info')->delete();
        $users_info = array(
            ['user_id'=>1,'country_id'=>1],
            ['user_id'=>2,'country_id'=>1],
            ['user_id'=>3,'country_id'=>1],
            ['user_id'=>4,'country_id'=>1]
        );

        foreach($users_info as $info){
            DB::table('users_info')->insert($info);
        }
    }
}
