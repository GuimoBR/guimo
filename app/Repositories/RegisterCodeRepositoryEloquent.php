<?php

namespace Guimo\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Guimo\Repositories\RegisterCodeRepository;
use Guimo\Entities\RegisterCode;
use Guimo\Validators\RegisterCodeValidator;

/**
 * Class RegisterCodeRepositoryEloquent
 * @package namespace Guimo\Repositories;
 */
class RegisterCodeRepositoryEloquent extends BaseRepository implements RegisterCodeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RegisterCode::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
