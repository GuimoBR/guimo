<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //$this->call(UserTableSeeder::class);
        $this->call(countries_table_seeder::class);
        $this->call(UsersTableSeeder::class);
       // $this->call(ProductTableSeeder::class);
       // $this->call(CategoryTableSeeder::class);
       // $this->call(CategoryProductTableSeeder::class);
       // $this->call(OrdersTableSeeder::class);
       // $this->call(ShoppingCartTableSeeder::class);
       // $this->call(ShoppingCartProductsTableSeeder::class);
        Model::reguard();
    }
}
