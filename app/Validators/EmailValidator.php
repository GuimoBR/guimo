<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 22:38
 */

namespace Guimo\Validators;


use Prettus\Validator\LaravelValidator;

class EmailValidator extends LaravelValidator
{

    protected $rules = [
        'name' =>  'required|max:80',
        'from' =>  'required|email|max:120',
        'subject' => 'required|max:80',
        'body'  => 'required'
    ];

    protected $messages = [
        'required'  => 'O campo :attribute é obrigatório',
        'max'       => 'O campo :attribute não pode ultrapassar :max caracteres',
        'email'     => 'O campo :attribute deve ser preenchido com um email'
    ];

    protected $attributes = [
        'email' => 'e-mail',
        'from'  => 'De',
        'subject'   => 'Título',
        'body'  => 'Mensagem'
    ];
}