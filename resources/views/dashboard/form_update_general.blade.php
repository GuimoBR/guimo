{{csrf_field()}}
{{method_field('PUT')}}
<div class="row">
    <div class="form-group">
        <div @if($errors->has('name')) class="has-error" @endif>
            <label for="name" class="control-label col-lg-offset-2 col-md-1 col-sm-3 col-xs-12">Nome:</label>
            <div class="col-lg-7 col-md-8 col-sm-6 col-xs-12">
                <input type="text" name="name" id="name" class="form-control col-md-3" value="{{$profile->name}}">
                @if($errors->has('name')) <span id="helpBlock2" class="help-block">{{$errors->first('name')}}</span>@endif
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">

        <label for="email" class="control-label col-lg-offset-2 col-md-1 col-sm-3 col-xs-12">Email:</label>
        <div class="col-lg-7 col-md-8 col-sm-6 col-xs-12">
            <input type="email" name="email" id="email" class="form-control col-md-3" value="{{$profile->email}}" readonly>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div @if($errors->has('old_password')) class="has-error" @endif>
            <label for="old_password" class="control-label col-lg-offset-2 col-md-1 col-sm-3 col-xs-12">Senha Antiga:</label>
            <div class="col-lg-7 col-md-8 col-sm-6 col-xs-12">
                <input type="password" name="old_password" id="old_password" class="form-control col-md-3">
                @if($errors->has('old_password')) <span id="helpBlock2" class="help-block">{{$errors->first('old_password')}}</span>@endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div @if($errors->has('password')) class="has-error" @endif>
            <label for="password" class="control-label col-lg-offset-2 col-md-1 col-sm-3 col-xs-12">Nova Senha:</label>
            <div class="col-lg-7 col-md-8 col-sm-6 col-xs-12">
                <input type="password" name="password" id="password" class="form-control col-md-3">
                @if($errors->has('password')) <span id="helpBlock2" class="help-block">{{$errors->first('password')}}</span>@endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-md-offset-5">
        <button id="submitGeneral" class="btn btn-primary btn-lg" type="submit">Altualizar dados </button>
    </div>
</div>