<?php

namespace Guimo\Http\Controllers;

use Guimo\Repositories\EmailRepository;
use Guimo\Repositories\UserEmailsRepository;
use Guimo\Services\EmailService;
use Illuminate\Http\Request;

use Guimo\Http\Requests;

use Guimo\Email;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;



class EmailController extends Controller
{
    /**
     * @var EmailRepository
     */
    private $repository;
    /**
     * @var EmailService
     */
    private $service;
    /**
     * @var UserEmailsRepository
     */
    private $userEmailsRepository;

    private $user = null;
    /**
     * EmailController constructor.
     * @param EmailRepository $repository
     * @param EmailService $service
     */
    public function __construct(EmailRepository $repository, EmailService $service,UserEmailsRepository $userEmailsRepository)
    {

        $this->repository = $repository;
        $this->service = $service;
        $this->userEmailsRepository = $userEmailsRepository;
        $this->user = Auth::user();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $emails = $this->repository->all();


        return view('dashboard.emails_index',['user'=>$this->user,'emails'=>$emails]);//compact('user','emails'));


    }

    public function remove(){

        $emails = $this->repository->all();
        return view('dashboard.emails_remove',['user'=>$this->user,'emails'=>$emails]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view("dashboard.email_template",['user'=>$this->user]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $createEmail = $this->service->createEmailTemplate($request->all());
        if(is_bool($createEmail)){
            return redirect('admin/emails/')->with('message','Email criado com sucesso');
        }else{
            return redirect('admin/emails/')->withErrors($createEmail);
        }
    }

    public function sendForm(){

        $userEmails = $this->userEmailsRepository->all();
        $emailTypes = $this->repository->all();

        return view("dashboard.email_send",['user'=>$this->user,'userEmails'=>$userEmails,'emailTypes'=>$emailTypes]);//compact('user','userEmails','emailTypes'));
    }

    public function sendEmail(Request $request)
    {
        $data = $this->service->getEmail($request->emailType);
        $data['to'] = $request->emails;


        $sendEmail = $this->service->sendRaw($data['attributes']);
        if(is_bool($sendEmail)){
            return redirect('admin/emails/send')->with('message','Emails enviados com sucesso');
        }else{
            return redirect('admin/emails/send')->withErrors($sendEmail);
        }


    }

    public function status(){
        $user = Auth::user();
        $emailList = $this->service->getEmailsStatus();
        return view('dashboard.email_status',['user'=>$this->user,'emailList'=>$emailList]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = Auth::user();
        $email = $this->service->getEmail($id);

        return view('dashboard.emails_show',['user'=>$this->user,'email'=>$email]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
