<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 24/12/15
 * Time: 01:48
 */

namespace Guimo\Repositories;


use Guimo\Entities\FlappyGuimo;
use Prettus\Repository\Eloquent\BaseRepository;

class FlappyGuimoRepositoryEloquent extends BaseRepository implements FlappyGuimoRepository
{

    public function model(){
        return FlappyGuimo::class;
    }
}