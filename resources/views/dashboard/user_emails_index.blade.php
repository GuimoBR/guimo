@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/TableTools.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Lista de Emails Cadastrados</h2>
        </div>
        <div class="section-body">

            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Lista de Emails cadastrados na Landing Page</header>
                        </div>
                        <div class="card-body style-default-bright">
                            <table id="emails" class="table order-column table-striped hover" data-swftools="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf')}}">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Data de Cadastro</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($emails as $email)
                                    @if($email->status >= 1)
                                        <tr class="gradeA">
                                            <td>
                                                {{$email->id}}
                                            </td>
                                            <td>
                                                {{$email->email}}
                                            </td>
                                            <td>
                                                {{$email->created_at->format('d/m/Y H:i:s')}}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>

@endsection

@section('script')
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script>
        $(document).ready(function() {

            var oTable = $('#emails').DataTable({
                "dom": 'T<"clear">lfrtip',
                "order": [[1, 'asc']],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "tableTools": {
                    "sSwfPath": $('#emails').data('swftools')
                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        })
    </script>
@endsection