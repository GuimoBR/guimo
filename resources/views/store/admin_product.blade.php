@extends('layouts.dashboard2')
@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Informações do produto <a href="{{route('admin.store.products')}}" class="pull-right text-sm"><i class="fa fa-arrow-circle-left"></i> Voltar</a></h2>

        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @if($errors->has())

                        <div class="alert alert-callout alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p><strong>Atenção, alguns erros foram detectados:</strong></p>
                            <ul class="list-unestyled">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                            <div class="alert alert-callout alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <p>{{Session::get('message')}}</p>
                            </div>
                        @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>{{$product->name}}</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#edit_product"><i class="fa fa-edit"></i> Editar Este Produto</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body style-default-bright">
                            <div class="row">
                                <div class="col-md-4"  style="border-right:1px solid #333">
                                    <img src="{{url('http://store.guimo.dev/image/'.$product->slug.'/'.$product->thumb_image)}}" alt="{{$product->name}}" style="height: 150px; width: 320px;" />
                                </div>
                                <div class="col-md-8">
                                    <p class="lead"> R$ {{$product->price}}  @if($product->discount_percent > 0) <small>desconto de {{$product->discount_percent}}%</small>
                                        <i class="fa fa-long-arrow-right"></i> <span class="text-medium text-success">R$ {{$product->final_price}}</span>
                                        @endif
                                        @if($product->is_avaible)<span class="pull-right">Produto Disponível para venda </span>@else <span class="pull-right text-danger"> Produto Indisponivel para venda </span>@endif
                                    </p>
                                    <p class="text-lg">
                                        Categorias: <br>
                                        @foreach($product->categories as $category)
                                            <span class="label label-info">{{$category->name}}</span>
                                        @endforeach
                                    </p>
                                    <p class="lead">Em estoque:  <small>{{$product->stock}}</small></p>
                                    {{--<p class="lead">
                                        Descrição do produto:

                                    </p>
                                    <p class="well scroll" style="height: auto;">
                                        {!! nl2br($product->description) !!}
                                    </p>--}}
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="card style-default-bright">
                                        <div class="card-head">
                                            <header>Descrição do Produto</header>
                                            <div class="tools">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <div class="card-body height-4 scroll">
                                            <p>{!! nl2br($product->description) !!}</p>
                                        </div>
                                    </div>
                                    </div>

                                </div>
                                @if(count($product->images) >0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="lead">Imagens enviadas</p>
                                        @foreach($product->images as $image)
                                            <img src="{{url('http://store.guimo.dev/image/'.$product->slug.'/'.$image->file_name)}}" alt="{{$image->file_name}}" style="height: 150px; width: 350px;">
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="edit_product" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Editar {{$product->name}}</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.store.update_product',['slug'=>$product->slug])}}" method="POST" id="update_product" enctype="multipart/form-data">
                                {{method_field('PUT')}}
                                @include('store.form_new_product',['product'=>$product])
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary" id="submit">Editar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('assets/js/string_to_slug.js')}}"></script>
    <script>
        $(document).ready(function(){

            $("#submit").click(function(){
                $("#update_product").submit();
            });

            var discount = 1 - ($("#discount_percent").val()/100);
            var final_price = $("#price").val() * discount;
            $("#final_price").val(final_price.toFixed(2));

            $("#name").keyup(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            $("#name").change(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            $("#price").keyup(function(){
                var discount = 1 - ($("#discount_percent").val()/100);
                var final_price = $(this).val() * discount;
                $("#final_price").val(final_price.toFixed(2));
            });

            $("#discount_percent").keyup(function(){
                var discount = 1 - ($("#discount_percent").val()/100);
                var final_price = $("#price").val() * discount;
                $("#final_price").val(final_price.toFixed(2));
            });

            $("#discount_percent").change(function(){
                var discount = 1 - ($("#discount_percent").val()/100);
                var final_price = $("#price").val() * discount;
                $("#final_price").val(final_price.toFixed(2));
            });
            $('.card-head .tools .btn-collapse').on('click', function (e) {
                var card = $(e.currentTarget).closest('.card');
                materialadmin.AppCard.toggleCardCollapse(card);
            });
        });
    </script>

@endsection