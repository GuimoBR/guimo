<?php

namespace Guimo\Http\Controllers;

use Illuminate\Http\Request;

use Guimo\Http\Requests;
use Guimo\Http\Controllers\Controller;
use Auth;
use Validator;

class LoginController extends Controller
{
    /**
     * Login attempt
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::check()){
            return redirect('/dashboard');
        }else{
            return view('login.login');
        }


        //print_r($request->all());
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->flush();
        return redirect('/login');
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'email'     =>  'required|email',
            'password'  =>  'required'
        ],[
            'email.required'  =>  'O campo email deve ser preenchido',
            'password.required'=>   'O campo senha deve ser preenchido',
            'email'     =>  'O campo email deve ser preenchido com um email valido!'
        ]);
        $remember = false;

        if($request->input('remember') && $request->input('remember') == 'on'){
            $remember = true;
        }

        if($validator->fails()){
            return redirect('/login')->withErrors($validator)->withInput($request->all());
        }else {
            if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $remember)) {
                return redirect()->intended('/dashboard');
            } else {
                return redirect('/login')->withErrors(['login'=>'Email ou senha invalidos!']);
            }
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
