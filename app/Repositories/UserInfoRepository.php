<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsersInfoRepository
 * @package namespace Guimo\Repositories;
 */
interface UserInfoRepository extends RepositoryInterface
{
    //
}
