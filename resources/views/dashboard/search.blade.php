@extends('layouts.dashboard2')

@section('content')
    <section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li class="active">Resultados da busca</li>
        </ol>
    </div>
        <div class="section-body contain-lg">
            <div class="card tabs-left style-default-light">
                <!-- BEGIN SEARCH BAR -->
                <div class="card-body style-primary no-y-padding">
                    <form class="form form-inverse" role="search" action="{{route('admin.search')}}">
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <div class="input-group-content">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Digite o que procura" value="@if(!is_null($name)){{$name}}@endif">
                                    <div class="form-control-line"></div>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-floating-action btn-default-bright" type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div><!--end .form-group -->
                    </form>
                </div><!--end .card-body -->
                <!-- END SEARCH BAR -->
                <!-- BEGIN TAB RESULTS -->
                <ul class="card-head nav nav-tabs tabs-accent" data-toggle="tabs">
                    <li @if(!is_null($general)) class="active" @endif><a href="#geral">Geral</a></li>
                    @if($user->privileges >= 10)
                    <li @if(!is_null($admins)) class="active" @endif><a href="#admins">Admins</a></li>
                        @endif
                </ul>
                <!-- BEGIN TAB CONTENT -->
                <div class="card-body tab-content style-default-bright">
                    <div class="tab-pane @if(!is_null($general)) active @endif" id="geral">
                        @if(is_null($general))
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-lg">Nenhum Resultado encontrado</p>

                                <ul class="pagination">
                                    <li class="disabled"><a href="#">«</a></li>
                                    <li class="disabled"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                    <li class="disabled"><a href="#">»</a></li>
                                </ul>
                            </div>
                        </div>
                            @endif
                    </div>
                    @if($user->privileges >= 10)
                    <div class="tab-pane @if(!is_null($admins)) active @endif" id="admins">
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- BEGIN PAGE HEADER -->
                                <div class="margin-bottom-xxl">
                                    <span class="text-light text-lg">Resultados da pesquisa <strong>{{count($admins)}}</strong></span>
                                </div><!--end .margin-bottom-xxl -->
                                <!-- BEGIN RESULT LIST -->
                                <div class="list-results list-results-underlined">
                                    @foreach($admins as $admin)
                                        <div class="col-xs-12">
                                            @if(!is_null($admin->info->profile_image))
                                            <img class="pull-left width-3" src="{{url('/')}}/files/{{$admin->id}}/{{$admin->info->profile_image}}" alt="{{$admin->name}}" />
                                            @else
                                                <img class="pull-left width-3" src="{{url('/')}}/files/{{$admin->id}}/default.png" alt="{{$admin->name}}" />
                                                @endif
                                            <p>
                                                <a class="text-medium text-lg text-primary" href="{{route('profile',['id'=>$admin->id])}}">{{$admin->name}}</a><br/>
                                                <a class="opacity-75" href="{{route('profile',['id'=>$admin->id])}}">{{route('profile',['id'=>$admin->id])}}</a>
                                            </p>
                                            <div class="contain-xs pull-left">
                                                @if($admin->privileges >= 10)
                                                    <p class="text-warning">Administrador</p>
                                                    @endif
                                            </div>
                                        </div><!--end .col -->
                                        @endforeach
                                </div>
                                <!-- BEGIN PAGING -->
                                {{$admins->render()}}
                                <!-- END PAGING -->
                            </div>
                        </div>
                    </div>
                        @endif
                </div>

            </div>

        </div>

    </section>


@endsection