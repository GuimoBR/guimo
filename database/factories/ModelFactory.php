<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Guimo\Entities\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Guimo\Entities\Category::class,function(Faker\Generator $faker){
    $word = $faker->words(rand(1,3),true);
   return [
       'name'=>$word,
       'description'=>$faker->sentence(rand(9,13)),
       'slug'=>str_slug($word),
   ];
});

$factory->define(Guimo\Entities\Product::class,function(Faker\Generator $faker){
    $word = $faker->words(rand(1,3),true);
    return [
        'slug'=>str_slug($word),
        'name'=>$word,
        'description'=>$faker->sentence(rand(9,13)),
        'price'=>rand(100,250),
        'thumb_image'=>'default.jpg',
        'is_avaible'=>false,
        'stock'=>rand(0,50),
    ];
});

$factory->define(Guimo\Entities\CategoriesProducts::class,function(){
   return [
       'category_id'=>rand(1,5),
       'product_id'=>rand(1,15)
   ];
});

$factory->define(Guimo\Entities\Order::class,function(Faker\Generator $faker){
    return [
        'user_id'=>rand(1,4),
        'status'=>rand(0,5),
        'status_info'=>$faker->word,
        'total_price'=>rand(500,1350),
        'discount_percent'=>0,
        'prom_code'=>'',
        'created_at'=>$faker->dateTimeThisYear,
        'obs'=>$faker->sentence(rand(4,6)),
    ];
});

$factory->define(Guimo\Entities\ShoppingCart::class,function(Faker\Generator $faker){
   return[
       'order_id'=>rand(1,5),
       'product_quantity'=>rand(1,5),
   ];
});

$factory->define(Guimo\Entities\ShoppingCartProducts::class,function(Faker\Generator $faker){
    return [
        'shopping_cart_id'=>rand(1,8),
        'product_id'=>rand(1,15),
    ];
});
