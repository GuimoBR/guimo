<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain'=>'www.'.config('app.url')],function() {

    Route::group(['prefix'=>\Mcamara\LaravelLocalization\Facades\LaravelLocalization::setLocale()],function(){


        Route::get('/login',[
            'as'=>'login.index',
            'uses'=>'LoginController@index',
        ]);


        Route::post('/login',[
            'as'=>'login',
            'uses' =>'LoginController@login',
        ]);

        Route::get('/logout',[
            'as'=>'logout',
            'uses'=>'LoginController@logout',
            'middleware'=>'auth'
        ]);
        include_once('Routes/dashboard.routes.php');
        include_once('Routes/admin.routes.php');
        include_once ('Routes/landingpage.routes.php');
        include_once ("Routes/store.routes.php");

    });

});


Route::group(['domain'=>config('app.url')],function(){

    Route::group(['prefix'=>\Mcamara\LaravelLocalization\Facades\LaravelLocalization::setLocale()],function(){

        Route::get('/login',[
            'as'=>'login.index',
            'uses'=>'LoginController@index',
        ]);


        Route::post('/login',[
            'as'=>'login',
            'uses' =>'LoginController@login',
        ]);

        Route::get('/logout',[
            'as'=>'logout',
            'uses'=>'LoginController@logout',
            'middleware'=>'auth'
        ]);
        include('Routes/landingpage.routes.php');
        include('Routes/dashboard.routes.php');
        include('Routes/admin.routes.php');
    });

});


/*API ROUTES*/

Route::group(['prefix' => 'api_v1/'], function() {
    Route::resource('/authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('/authenticate', 'AuthenticateController@authenticate');

    Route::post('/emails',['as'=>'lpsaveemail','uses'=>'Apiv1Controller@saveEmail']);
    Route::get('/emails',['as'=>'lpgetemail','uses'=>'Apiv1Controller@getEmails']);
    Route::get('/emails/created','Apiv1Controller@getEmailsCreated');
    Route::put('/emails/status','Apiv1Controller@updateStatus');

    Route::put('/profile/changePass','Apiv1Controller@changePass');

    Route::put('/profile/changeName','Apiv1Controller@changeName');

    Route::post('/flappyguimo/points','Apiv1Controller@flappyGuimoSendPoints');

    Route::get('/flappyguimo/points','Apiv1Controller@flappyGuimoGetPoints');
    Route::put('/flappyguimo/resetRanking','Apiv1Controller@flappyGuimoResetRanking');


    Route::post('/validate/cpf','Apiv1Controller@validateCPF');

    Route::get('/correios/consultaCep/{cep}','Apiv1Controller@correiosConsultaCep');

    Route::get('/correios/consultaPreco/{cep}','Apiv1Controller@correiosConsultaPreco');
    Route::get('/correios/restrear/{codigo}','Apiv1Controller@correiosRastrear');

});

Route::get('/testemail',function(){
   return view('emails.userlinkemail');
});



