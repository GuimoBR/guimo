<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserEmailRepository
 * @package namespace Guimo\Repositories;
 */
interface UserEmailsRepository extends RepositoryInterface
{
    //
}
