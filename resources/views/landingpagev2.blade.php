<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Guimo Toy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Guimo educação e diversão de outro mundo!">
    <meta name="abstract" content="Guimo é o smart toy que veio de outro planeta para ensinar programação e divertir as crianças">
    <meta name="keywords" content="smart,toy,guimo,smartoy,smart-toy,startup,revolucionar,startup,brinquedo,criança,child,inteligente,kids,educativo,divertido">
    <meta name="author" content="">


    <!--[if lt IE 9]>
    <script src="{{secure_asset('v2/js/html5shiv.js')}}"></script>
    <![endif]-->

    @if(env('APP_ENV') == 'production')
    <link rel="shortcut icon" type='image/x-icon' href="{{secure_asset('v2/images/ico/favicon.ico')}}">

    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="{{secure_asset('v2/css/bootstrap.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/css/jpreloader.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/css/animate.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/css/flexslider.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/css/plugin.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/css/prettyPhoto.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/css/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/css/owl.theme.css')}}" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,900">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic">
    <link rel="stylesheet" href="{{secure_asset('v2/css/style.css')}}" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="{{secure_asset('v2/css/color.css')}}" type="text/css">

    <!-- revolution slider -->
    <link rel="stylesheet" href="{{secure_asset('v2/rs-plugin/css/settings.css')}}" type="text/css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,500,600,700,900,400">
    <link rel="stylesheet" href="{{secure_asset('v2/css/revsettings.css')}}" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="{{secure_asset('v2/fonts/font-awesome-4.5.0/css/font-awesome.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/fonts/elegant_font/HTML_CSS/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{secure_asset('v2/fonts/et-line-font/style.css')}}" type="text/css">

    <!-- custom style css -->
    <link rel="stylesheet" href="{{secure_asset('v2/css/custom-style.css')}}" type="text/css">
        @else
        <link rel="stylesheet" href="{{asset('v2/css/bootstrap.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/css/jpreloader.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/css/animate.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/css/flexslider.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/css/plugin.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/css/prettyPhoto.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/css/owl.carousel.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/css/owl.theme.css')}}" type="text/css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,900">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic">
        <link rel="stylesheet" href="{{asset('v2/css/style.css')}}" type="text/css">

        <!-- color scheme -->
        <link rel="stylesheet" href="{{asset('v2/css/color.css')}}" type="text/css">

        <!-- revolution slider -->
        <link rel="stylesheet" href="{{asset('v2/rs-plugin/css/settings.css')}}" type="text/css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,500,600,700,900,400">
        <link rel="stylesheet" href="{{asset('v2/css/revsettings.css')}}" type="text/css">

        <!-- load fonts -->
        <link rel="stylesheet" href="{{asset('v2/fonts/font-awesome-4.5.0/css/font-awesome.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/fonts/elegant_font/HTML_CSS/style.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('v2/fonts/et-line-font/style.css')}}" type="text/css">

        <!-- custom style css -->
        <link rel="stylesheet" href="{{asset('v2/css/custom-style.css')}}" type="text/css">
    @endif
</head>

<body id="homepage">

<!-- This section is for Splash Screen -->
<div id="jSplash">
    <section class="selected">
        {{trans('landingpagev2.splash1')}}
    </section>
    <section>
        {{trans('landingpagev2.splash2')}}
    </section>
    <section>
        {{trans('landingpagev2.splash3')}}
    </section>
</div>
<!-- End of Splash Screen -->


<div id="wrapper">
    <div class="page-overlay">
    </div>


    <!-- header begin -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- logo begin -->
                    <h1 id="logo">
                        <a href="{{url('/')}}">
                            @if(env('APP_ENV')=='production')
                                <img class="logo-1" src="{{secure_asset('v2/images/logo1.png')}}" alt="">
                                <img class="logo-2" src="{{secure_asset('v2/images/logo2.png')}}" alt="">
                            @else
                                <img class="logo-1" src="{{asset('v2/images/logo1.png')}}" alt="">
                                <img class="logo-2" src="{{asset('v2/images/logo2.png')}}" alt="">
                            @endif
                        </a>
                    </h1>
                    <!-- logo close -->

                    <!-- small button begin -->
                    <span id="menu-btn"></span>
                    <!-- small button close -->

                    <!-- mainmenu begin -->
                    <nav>
                        <ul id="mainmenu">
                            <li><a class="active" href="#wrapper">{{trans('landingpagev2.home')}}</a></li>
                            <li><a href="#section-services">{{trans('landingpagev2.aboutus')}}</a></li>
                            <li><a href="#section-team">{{trans('landingpagev2.team')}}</a></li>
                            <!--<li><a href="#section-testimonial">{{trans('landingpagev2.testimonial')}}</a></li>-->
                            <!--<li><a href="#section-blog">{{trans('landingpagev2.blog')}}</a></li>-->
                            <li><a href="#section-contact">{{trans('landingpagev2.contact')}}</a></li>
                            @if(env('APP_ENV')=='production')
                            <li><a href="{{secure_url('/')}}"><img src="{{secure_asset('v2/images/pt-BR.png')}}" alt="br"/></a></li>
                            <li><a href="{{LaravelLocalization::getLocalizedURL('en')}}"><img src="{{secure_asset('v2/images/en.png')}}" alt="en"/></a></li>
                            @else
                                <li><a href="{{url('/')}}"><img src="{{asset('v2/images/pt-BR.png')}}" alt="br"/></a></li>
                                <li><a href="{{LaravelLocalization::getLocalizedURL('en')}}"><img src="{{asset('v2/images/en.png')}}" alt="en"/></a></li>
                            @endif
                        </ul>
                    </nav>

                </div>
                <!-- mainmenu close -->

            </div>
        </div>
    </header>
    <!-- header close -->


    <!-- content begin -->
    <div id="content" class="no-bottom no-top">


        <!-- section begin -->
        <section class="full-height dark no-padding" data-speed="5" data-type="background">
            <div class="de-video-container">
                <div class="de-video-content">
                    <div class="text-center">
                        <div class="text-slider border-deco">
                            <div class="text-item">{!! trans('landingpagev2.welcome') !!}</div>
                            <div class="text-item">{!! trans('landingpagev2.slider1') !!}</div>
                            <div class="text-item">{!! trans('landingpagev2.slider2') !!}</div>
                        </div>
                        <div class="spacer-single"></div>
                        <div class="text-lg">{!! trans('landingpagev2.guimowel') !!}</div>
                    </div>
                </div>

                <div class="de-video-overlay-30"></div>

                <!-- load your video here -->
                @if(env('APP_ENV') == 'production')
                <video autoplay="" loop="" muted="" poster="{{secure_asset('v2/video/video-2.jpg')}}">
                    <source src="{{secure_asset('v2/video/video-2.webm')}}" type="video/webm" />
                    <source src="{{secure_asset('v2/video/video-2.mp4')}}" type="video/mp4" />
                    <source src="{{secure_asset('v2/video/video-2.ogg')}}" type="video/ogg" />
                </video>
                    @else
                    <video autoplay="" loop="" muted="" poster="{{asset('v2/video/video-2.jpg')}}">
                        <source src="{{asset('v2/video/video-2.webm')}}" type="video/webm" />
                        <source src="{{asset('v2/video/video-2.mp4')}}" type="video/mp4" />
                        <source src="{{asset('v2/video/video-2.ogg')}}" type="video/ogg" />
                    </video>
                @endif
            </div>

        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="section-about" class="side-bg no-padding">

            <div class="col-md-12">
                <div class="spacer-single"></div>
                <div class="spacer-single"></div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h1 class="animated" data-animation="fadeInUp">{!! trans('landingpagev2.helloworld') !!}
                            <span class="small-border animated" data-animation="fadeInUp"></span>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="inner-padding">
                            @if(env('APP_ENV') == 'production')
                                <img src="{{secure_asset('/v2/images/guimocabeca1.png')}}" alt="Cabeça" class="animated" data-animation="fadeInRight" data-delay="0"/>
                            @else
                                <img src="{{asset('/v2/images/guimocabeca1.png')}}" alt="Cabeça" class="animated" data-animation="fadeInRight" data-delay="0"/>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-9 animated" data-animation="fadeInLeft">
                        <div class="inner-padding">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>{{trans('landingpagev2.guimohead')}}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="col-md-12">
                <div class="col-md-3 pull-right">
                    <div class="inner-padding">
                        @if(env('APP_ENV')== 'production')
                            <img src="{{secure_asset('/v2/images/guimocorpo1.png')}}" alt="Cabeça" class="animated" data-animation="fadeInRight" data-delay="0"/>
                        @else
                            <img src="{{asset('/v2/images/guimocorpo1.png')}}" alt="Cabeça" class="animated" data-animation="fadeInRight" data-delay="0"/>
                        @endif
                    </div>
                </div>
                <div class="col-md-9 animated" data-animation="fadeInLeft">
                    <div class="inner-padding">
                        <h2>{{trans('landingpagev2.guimobody')}}</h2>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="section-services" class="no-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h1 class="animated" data-animation="fadeInUp">{!! trans('landingpagev2.whatmore') !!}
                            <span class="small-border animated" data-animation="fadeInUp"></span>
                        </h1>
                        <div class="spacer-single"></div>
                    </div>
                </div>

                <div class="row">

                    <!-- feature box begin -->
                    <div class="feature-box-small-icon col-md-4 animated" data-animation="fadeInUp" data-delay="600">
                        <div class="inner">
                            <i class="fa fa-mobile"></i>
                            <div class="text">
                                <h3>{{trans('landingpagev2.app')}}</h3>
                                {{trans('landingpagev2.apptxt')}}
                            </div>
                        </div>
                    </div>
                    <!-- feature box close -->

                    <!-- feature box begin -->
                    <div class="feature-box-small-icon col-md-4 animated" data-animation="fadeInUp" data-delay="0">
                        <div class="inner">
                            <i class="fa fa-gamepad"></i>
                            <div class="text">
                                <h3>{{trans('landingpagev2.games')}}</h3>
                                {{trans('landingpagev2.gamestxt')}}
                            </div>
                        </div>
                    </div>
                    <!-- feature box close -->

                    <!-- feature box begin -->
                    <div class="feature-box-small-icon col-md-4 animated" data-animation="fadeInUp" data-delay="200">
                        <div class="inner">
                            <i class="fa fa-graduation-cap"></i>
                            <div class="text">
                                <h3>{{trans('landingpagev2.edu')}}</h3>
                                {{trans('landingpagev2.edutxt')}}
                            </div>
                        </div>
                    </div>
                    <!-- feature box close -->

                    <!-- feature box begin -->
                    <div class="feature-box-small-icon col-md-4 animated" data-animation="fadeInUp" data-delay="400">
                        <div class="inner">
                            <i class="fa fa-puzzle-piece"></i>
                            <div class="text">
                                <h3>{{trans('landingpagev2.modular')}}</h3>
                                    {{trans('landingpagev2.modulartxt')}}
                            </div>
                        </div>
                    </div>
                    <!-- feature box close -->

                    <!-- feature box begin -->
                    <div class="feature-box-small-icon col-md-4 animated" data-animation="fadeInUp" data-delay="800">
                        <div class="inner">
                            <i class="fa fa-bluetooth"></i>
                            <div class="text">
                                <h3>{{trans('landingpagev2.bluetooth')}}</h3>
                                {{trans('landingpagev2.bluetoothtxt')}}
                            </div>
                        </div>
                    </div>
                    <!-- feature box close -->

                    <!-- feature box begin -->
                    <div class="feature-box-small-icon col-md-4 animated" data-animation="fadeInUp" data-delay="1000">
                        <div class="inner">
                            <i class="fa fa-cog"></i>
                            <div class="text">
                                <h3>{{trans('landingpagev2.custom')}}</h3>
                                {{trans('landingpagev2.customtxt')}}
                            </div>
                        </div>
                    </div>
                    <!-- feature box close -->

                    <div class="spacer-single"></div>

                    <!--<div class="col-md-12 text-center animated" data-animation="fadeInUp" data-delay="400">
                        <img src="{{secure_asset('v2/images/cubic_ss.png')}}" class="img-responsive" alt="">
                    </div>-->
                </div>
            </div>
        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="logo-full">
            <div class="container">
                <!-- logo carousel -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        @if(env('APP_ENV') == 'production')
                            <a href="http://www.techtudo.com.br/noticias/noticia/2016/01/techtudo-recebe-campuseiros-criadores-de-apps-na-campus-party-2016.html"><img src="{{secure_asset('v2/images/TechTudo.png')}}" alt="" height="45"></a>
                        @else
                            <a href="http://www.techtudo.com.br/noticias/noticia/2016/01/techtudo-recebe-campuseiros-criadores-de-apps-na-campus-party-2016.html"><img src="{{asset('v2/images/TechTudo.png')}}" alt="" height="45"></a>
                        @endif
                    </div>
                    <!-- logo carousel close -->
                </div>

            </div>
        </section>
        <!-- section close -->

        <!-- team section begin -->
        <section id="section-team">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h1 class="animated" data-animation="fadeInUp">{!! trans('landingpagev2.secteam') !!}
                            <span class="small-border animated" data-animation="fadeInUp"></span>
                        </h1>
                        <div class="spacer-single"></div>
                    </div>
                </div>


                <div class="row">
                    <!-- team member -->
                    <div class="de-team-list col-md-2 col-md-offset-1 animated" data-animation="fadeInUp" data-delay="0">
                        <div class="team-pic">
                            @if(env('APP_ENV')=='production')
                            <img src="{{secure_asset('v2/images/team/caio.jpg')}}" class="img-responsive" alt="Caio"/>
                                @else
                                <img src="{{asset('v2/images/team/caio.jpg')}}" class="img-responsive" alt="Caio"/>
                            @endif
                        </div>
                        <div class="team-desc col-md-12">
                            <h3>Caio</h3>
                            <p class="lead">Business</p>
                            <div class="small-border"></div>

                            <div class="social">
                                <a href="https://br.linkedin.com/in/rochacaiov/pt" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a>
                                <a href="skype:caio1905?call"><i class="fa fa-skype fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- team close -->

                    <!-- team member -->
                    <div class="de-team-list col-md-2 animated" data-animation="fadeInUp" data-delay="0">
                        <div class="team-pic">
                            @if(env('APP_ENV')=='production')
                            <img src="{{secure_asset('v2/images/team/buiu.jpg')}}" class="img-responsive" alt="William" />
                                @else
                                <img src="{{asset('v2/images/team/buiu.jpg')}}" class="img-responsive" alt="William" />
                            @endif
                        </div>
                        <div class="team-desc col-md-12">
                            <h3>Willian</h3>
                            <p class="lead">Hardware</p>
                            <div class="small-border"></div>

                            <div class="social">
                                <a href="https://br.linkedin.com/in/willianuehara" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a>
                                <a href="skype:willianuehara?call"><i class="fa fa-skype fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- team close -->

                    <!-- team member -->
                    <div class="de-team-list col-md-2 animated" data-animation="fadeInUp" data-delay="0">
                        <div class="team-pic">
                            @if(env('APP_ENV') == 'production')
                            <img src="{{secure_asset('v2/images/team/thihds.jpg')}}" class="img-responsive" alt="Thiago" />
                                @else
                                <img src="{{asset('v2/images/team/thihds.jpg')}}" class="img-responsive" alt="Thiago" />
                            @endif
                        </div>
                        <div class="team-desc col-md-12">
                            <h3>Thiago</h3>
                            <p class="lead">Hardware</p>
                            <div class="small-border"></div>
                            <div class="social">
                                <a href="https://br.linkedin.com/in/thiago-henrique-de-souza-4b6736106" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a>
                                <a href="skype:thihds?call"><i class="fa fa-skype fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- team close -->

                    <!-- team member -->
                    <div class="de-team-list col-md-2 animated" data-animation="fadeInUp" data-delay="0">
                        <div class="team-pic">
                            @if(env('APP_ENV') == 'production')
                                <img src="{{secure_asset('v2/images/team/lucas.jpg')}}" class="img-responsive" alt="Douglas" />
                                @else
                                <img src="{{asset('v2/images/team/lucas.jpg')}}" class="img-responsive" alt="Douglas" />
                            @endif
                        </div>
                        <div class="team-desc col-md-12">
                            <h3>Lucas</h3>
                            <p class="lead">Dev</p>
                            <div class="small-border"></div>
                            <div class="social">
                                <a href="https://br.linkedin.com/in/lucasbrigida/pt" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a>

                            </div>
                        </div>
                    </div>
                    <!-- team close -->


                    <!-- team member -->
                    <div class="de-team-list col-md-2 animated" data-animation="fadeInUp" data-delay="0">
                        <div class="team-pic">
                            @if(env('APP_ENV')=='production')
                                <img src="{{secure_asset('v2/images/team/jorge.jpg')}}" class="img-responsive" alt="Jorge" />
                                @else
                                <img src="{{asset('v2/images/team/jorge.jpg')}}" class="img-responsive" alt="Jorge" />
                                @endif
                        </div>
                        <div class="team-desc col-md-12">
                            <h3>Jorge</h3>
                            <p class="lead">Development</p>
                            <div class="small-border"></div>
                            <div class="social">
                                <a href="https://br.linkedin.com/in/jorgejd21" target="_blankk"><i class="fa fa-linkedin fa-lg"></i></a>
                                <a href="skype:facebook:jorgedjr21?call"><i class="fa fa-skype fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- team close -->

                    <div class="clearfix"></div>

                </div>
            </div>
        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="logo-full">
            <div class="container">
                <div class="row" style="margin: -50px;">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h2 class="animated" data-animation="fadeInUp">{{trans('landingpagev2.partners')}}
                            <span class="small-border animated" data-animation="fadeInUp"></span>
                        </h2>
                    </div>
                </div>
                <!-- logo carousel -->
                <div class="row animated" data-animation="fadeInUp">
                    <div class="col-md-12 text-center" style="margin:-25px 0px;">
                        @if(env('APP_ENV')=='production')
                            <a href="http://www.ceu.unifei.edu.br/" ><img src="{{secure_asset('assets/images/logo_ceu_branco.png')}}" alt="CEU" height="100" style="margin: -5px 10px;"></a>
                            <a href="http://www.ceu.unifei.edu.br/" ><img src="{{secure_asset('v2/images/lab001.png')}}" alt="CEU" height="100" style="margin:-5px 10px;"></a>
                            <a href="http://baita.ac" ><img src="{{secure_asset('v2/images/baita.png')}}" alt="Baita Aceleradora" height="100" style="margin:-5px 10px;"></a>
                        @else
                            <a href="http://www.ceu.unifei.edu.br/" ><img src="{{asset('assets/images/logo_ceu_branco.png')}}" alt="CEU" height="100" style="margin:-5px 10px;"></a>
                            <a href="http://www.ceu.unifei.edu.br/" ><img src="{{asset('v2/images/lab001.png')}}" alt="CEU" height="100" style="margin:-5px 10px;"></a>
                            <a href="http://baita.ac" ><img src="{{asset('v2/images/baita.png')}}" alt="Baita Aceleradora" height="100" style="margin:-5px 10px;"></a>
                        @endif
                    </div>
                    <!-- logo carousel close -->
                </div>

            </div>
        </section>
        <!-- section close -->



        <!-- section begin -->
        <section id="section-contact" class="dark whiteform" data-speed="5" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="animated" data-animation="fadeInUp">{!! trans('landingpagev2.contactus') !!}
                            <span class="small-border animated" data-animation="fadeInUp"></span>
                        </h1>
                        <div class="spacer-single"></div>
                    </div>

                    <div class="col-md-8 animated" data-delay="200" data-animation="fadeInUp" data-delay="200" data-speed="5">

                        <div name="contactForm" id='contact_form'>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id='email_error' class='error'></div>
                                    <div>
                                        <input name="email" id="email" type="email" class="form-control white" placeholder="{{trans('landingpagev2.youremail')}}">
                                    </div>
                                </div>
                                <div id='mail_success' class='success'></div>
                                <div id='mail_fail' class='error'></div>
                                <div class="col-md-12">
                                    <p id='submit'>
                                        <button type="button" id='send_email' class="btn btn-border">{{trans('landingpagev2.submit')}}</button>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <address>
                            <span><i class="fa fa-map-marker fa-lg"></i>Universidade Federal de Itajubá</span>
                            <span><i class="fa fa-envelope-o fa-lg"></i><a href="mailto:contato@guimo.toys">contato@guimo.toys</a></span>
                            <span><i class="fa fa-globe fa-lg"></i><a href="http://guimo.toys">http://guimo.toys</a></span>
                        </address>
                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->

        <!-- footer begin -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="social-icons">
                            <a href="https://www.facebook.com/guimotoy" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
                            <a href="mailto:contato@guimo.toys"><i class="fa fa-envelope-o fa-lg"></i></a>
                            <a href="https://plus.google.com/114992593692098714096" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a>
                        </div>
                        <div class="spacer-single"></div>
                        © Copyright 2015 - {{date('Y')}} Guimo
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer close -->
    </div>
</div>


<!-- Javascript Files
================================================== -->
@if(env('APP_ENV') == 'production')
<script src="{{secure_asset('v2/js/jquery.min.js')}}"></script>
<script src="{{secure_asset('v2/js/jpreLoader.js')}}"></script>
<script src="{{secure_asset('v2/js/bootstrap.min.js')}}"></script>
<script src="{{secure_asset('v2/js/jquery.isotope.min.js')}}"></script>
<script src="{{secure_asset('v2/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{secure_asset('v2/js/easing.js')}}"></script>
<script src="{{secure_asset('v2/js/jquery.ui.totop.js')}}"></script>
<script src="{{secure_asset('v2/js/jquery.flexslider-min.js')}}"></script>
<script src="{{secure_asset('v2/js/jquery.scrollto.js')}}"></script>
<script src="{{secure_asset('v2/js/owl.carousel.js')}}"></script>
<script src="{{secure_asset('v2/js/jquery.countTo.js')}}"></script>
<script src="{{secure_asset('v2/js/classie.js')}}"></script>
<script src="{{secure_asset('v2/js/designesia.js')}}"></script>
<script src="{{secure_asset('v2/js/validation.js')}}"></script>

<!-- SLIDER REVOLUTION SCRIPTS  -->
<script type="text/javascript" src="{{secure_asset('v2/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
<script type="text/javascript" src="{{secure_asset('v2/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
@else
    <script src="{{asset('v2/js/jquery.min.js')}}"></script>
    <script src="{{asset('v2/js/jpreLoader.js')}}"></script>
    <script src="{{asset('v2/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('v2/js/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('v2/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('v2/js/easing.js')}}"></script>
    <script src="{{asset('v2/js/jquery.ui.totop.js')}}"></script>
    <script src="{{asset('v2/js/jquery.flexslider-min.js')}}"></script>
    <script src="{{asset('v2/js/jquery.scrollto.js')}}"></script>
    <script src="{{asset('v2/js/owl.carousel.js')}}"></script>
    <script src="{{asset('v2/js/jquery.countTo.js')}}"></script>
    <script src="{{asset('v2/js/classie.js')}}"></script>
    <script src="{{asset('v2/js/designesia.js')}}"></script>
    <script src="{{asset('v2/js/validation.js')}}"></script>

    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="{{asset('v2/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('v2/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

    @endif
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-70734600-1', 'auto');
    ga('send', 'pageview');

</script>
<script>
    $(document).ready(function(){

        $("#send_email").click(function(){
            if($("#email").val() != '') {
                $("#send_email").attr("disabled",true);
                $("#send_email").html("{!! trans('landingpagev2.submiting') !!}").button("refresh");

                var form_data = {
                    email: $("#email").val(),
                    status: 1
                };
                $.post(
                        @if(env('APP_ENV') == 'development')
                            '{{url('/api_v1/emails')}}',
                        @else
                             '{{secure_url('/api_v1/emails')}}',
                        @endif
                    form_data,
                    function (response) {
                        if (response.success) {
                            $("#mail_success").html("{{trans('landingpagev2.successEmail')}}");
                            $("#mail_success").fadeIn();
                            $("#mail_success").delay(2200).fadeOut();

                        }
                        if (response.error) {
                            $("#mail_fail").html('{{trans('landingpagev2.wrongEmail')}}');
                            $("#mail_fail").fadeIn();
                            $("#mail_fail").delay(2200).fadeOut();
                        }
                        $("#send_email").attr("disabled",false);
                        $("#email").val('');
                        $("#send_email").html("Enviar").button("refresh");
                    });

            }else{
                $("#email_error").html("{{trans('landingpagev2.emptyEmail')}}");
                $("#email_error").fadeIn();
                $("#email_error").delay(2200).fadeOut();
            }
        });
    });
</script>
</body>
</html>
