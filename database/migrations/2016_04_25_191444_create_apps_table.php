<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('apps', function(Blueprint $table) {
            $table->increments('id');
			$table->string('name',60);
			$table->string('filename',60);
			$table->string('version',15);
			$table->string('description',100)->nullable();
			$table->text('changelog');
			$table->string('target_os',25)->index('so')->default('android');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('apps');
	}

}
