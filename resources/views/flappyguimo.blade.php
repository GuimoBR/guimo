<!DOCTYPE html>
<html>
<head>
    <title>Flappy Guimo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type='image/x-icon' href="{{secure_asset('assets/images/ico/favicon.ico')}}">


    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/css/bootstrap-material-design.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/css/ripples.min.css">
    <link href="{{secure_asset('assets/css/styles.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{secure_asset('assets/font-awesome/css/font-awesome.min.css')}}">
</head>
<body>

<div class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="javascript:void(0)"><img src="{{secure_asset('assets/images/logo_guimo_50px.png')}}" alt="Guimo" style="margin-top:-13px;"></a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{url("/")}}">Landing page</a></li>
                <li class="active"><a href="{{url('/flappyguimo')}}">Flappy Guimo</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{route('login')}}">Login <i class="fa fa-arrow-right"></i></a></li>

            </ul>
        </div>
    </div>
</div>

<div class="container">
    <h1 class="text-center text-info">Flappy Guimo <i class="fa fa-gamepad"></i> </h1>
    <p class="text-center"> Jogue agora mesmo o flappy guimo e tente ser o melhor de todos!</p>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    Flappy Guimo
                </div>
                <div class="panel-body">
                    <!--<div class="form-group orm-group-lg">
                        <label for="playerName" class="control-label">Nome do Jogador</label>
                        <input type="text" class="form-control" id="playerName">
                    </div>-->
                    <div class="form-group form-group-lg label-floating">
                        <label for="playerName" class="control-label">Nome do Jogador</label>
                        <input type="email" class="form-control" id="playerName">
                        <span class="help-block">Digite o nome do jogador</span>
                    </div>
                    <br>
                    <div id="phaser-game">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Ranking - Os 15 melhores</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-hover table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center">Posição</th>
                            <th class="text-center">Nome do Jogador</th>
                            <th class="text-center">Pontos</th>
                            <th class="text-center">Data</th>
                        </tr>
                        </thead>
                        <tbody id="ranking"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-70734600-1', 'auto');
    ga('send', 'pageview');

</script>

<script src="{{secure_asset('assets/js/jquery.min.js')}}"></script>
<script src="{{secure_asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{secure_asset('assets/js/jquery.min.js')}}"></script>
<script src="{{secure_asset('assets/js/bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js"></script>
<script>
    $.material.init();
</script>
<script src="{{secure_asset('assets/js/phaser.min.js')}}"></script>
<script src="{{secure_asset('assets/js/game.js')}}"></script>

<script>

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    };

    $.ajax({
        method: 'GET',
        url: '{{url('/')}}/api_v1/flappyguimo/points',
    }).success(function(response){
        var tbody = $("#ranking");
        var trclass = '';
        var trophy = '';
        for(var i =0; i < response.length;i++){
            trclass = '';
            trophy  = '';
            var d = new Date(response[i].created_at);
                if(i == 0){
                    trclass= 'success text-bold text-success';
                    trophy = '<i class="fa fa-trophy text-success"></i>';
                }

                if(i > 0 && i <= 2){
                    trclass= 'info text-bold text-info';
                }

                if( (i+1) == response.length){
                    trclass= 'danger text-bold text-danger';
                    trophy = '<i class="fa fa-frown-o text-danger"></i>';
                }

            tbody.append('<tr class="text-center '+trclass+'"><td>'+(i+1)+'º '+trophy+'</td><td>'+response[i].playerName+'</td><td>'+response[i].points+'</td><td>'+addZero(d.getDate())+'/'+ addZero(d.getMonth() + 1)+'/'+ d.getFullYear()+' '+ addZero(d.getHours())+':'+ addZero(d.getMinutes())+':'+addZero(d.getSeconds())+'</td></tr>');
        }
    }).error(function(response){
        console.log(response);
    });
</script>



</body>
</html>