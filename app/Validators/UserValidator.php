<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 23:33
 */

namespace Guimo\Validators;


use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator
{
    protected $rules = [
        'name'      => 'required|max:255',
        'email'     => 'required|email',
        'password'  => 'required',
    ];

    protected $messages = [
        'required'  => 'O campo :attribute é obrigatório',
        'email'     => 'O campo :attribute deve ser preenchido com um email válido',
        'max'       => 'O campo :attribute deve ter no maximo :max caracteres',
    ];

    protected $attributes = [
        'name'     => 'nome',
        'email'  => 'e-mail',
        'password'      => 'senha'
    ];

}