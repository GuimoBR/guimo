<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_code', function (Blueprint $table) {
            $table->increments('id');
            $table->string("token",255);
            $table->boolean('is_accessible')->default(true);
            $table->timestamp("created_at")->nullable();
            $table->timestamp("accessed_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('register_code');
    }
}
