@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/TableTools.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')}}">
@endsection

@section('content')
    <section class="style-default-bright">
        <div class="section-header">
            <h2 class="text-primary">Lista de Emails Cadastrados</h2>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table class="table order-column table-striped hover" data-swftools="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf')}}" id="emails">
                        <thead>
                        <tr class="headings">

                            <th class="text-center">#</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Data de Cadastro</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($emails as $email)
                            <tr class="even pointer text-center">
                                <td>
                                    {{$email->id}}
                                </td>
                                <td>
                                    {{$email->email}}
                                </td>
                                <td>
                                    {{$email->created_at->format('d/m/Y H:i:s')}}
                                </td>

                                <td>
                                    @if($email->status)
                                        <input type="checkbox" name="remove" id="remove" data-size="small" data-on-text="<i class='fa fa-circle-o'></i>" data-off-text="<i class='fa fa-power-off'></i>" data-on-color="success" data-off-color="danger" email="{{$email->id}}" checked>
                                    @else
                                        <input type="checkbox" name="remove" id="remove" data-size="small" data-on-text="<i class='fa fa-circle-o'></i>" data-off-text="<i class='fa fa-power-off'></i>" data-on-color="success" data-off-color="danger" email="{{$email->id}}">
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{asset('assets/bootstrap-switch/dist/js/bootstrap-switch.min.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            var checkbox = $("input[type='checkbox']");
            checkbox.bootstrapSwitch();
            checkbox.on('switchChange.bootstrapSwitch',function(event,state){
                var email_status = null;
                if(state){
                    email_status = 1;
                }else{
                    email_status = 0
                }
                var status_data = {
                    status: email_status,
                    id:this.getAttribute('email')
                };

                $.ajax({
                    method: 'PUT',
                    url: '{{url('/')}}/api_v1/emails/status',
                    data:status_data})
                        .success(function(response){
                            if(response.success) {
                                new PNotify({
                                    title: 'Alteração de Status',
                                    text: response.message,
                                    type: 'success'
                                });
                            }else{
                                new PNotify({
                                    title: 'Alteração de Status',
                                    text: response.message,
                                    type: 'error'
                                });
                            }
                        })
                        .error(function(response){
                            new PNotify({
                                title: 'Alteração de Status',
                                text: response.message,
                                type: 'error'
                            });
                        })
            });

            var oTable = $('#emails').DataTable({
                "dom": 'T<"clear">lfrtip',
                "order": [[1, 'asc']],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "tableTools": {
                    "sSwfPath": $('#emails').data('swftools')
                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
        });
    </script>
@endsection