(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

//global variables
window.onload = function () {
  var game = new Phaser.Game(282, 505, Phaser.AUTO, 'phaser-game');

  // Game States
  game.state.add('boot', require('./states/boot'));
  game.state.add('gameover', require('./states/gameover'));
  game.state.add('menu', require('./states/menu'));
  game.state.add('play', require('./states/play'));
  game.state.add('preload', require('./states/preload'));
  

  game.state.start('boot');
};
},{"./states/boot":7,"./states/gameover":8,"./states/menu":9,"./states/play":10,"./states/preload":11}],2:[function(require,module,exports){
'use strict';

var Bird = function(game, x, y, frame) {
  Phaser.Sprite.call(this, game, x, y, 'bird', frame);

  // initialize your prefab here

  //set bird anchor to the center
    this.anchor.setTo(0.5,0.5);
    this.animations.add('flap');
    this.animations.play('flap',12,true);
    this.flapSound = this.game.add.audio('flap');

    this.name = 'bird';
    this.alive = false;
    this.onGround = false;

    this.game.physics.arcade.enableBody(this);
    this.body.allowGravity = false;
    this.body.collideWorldBounds = true;

    this.events.onKilled.add(this.onKilled, this);

};

Bird.prototype = Object.create(Phaser.Sprite.prototype);
Bird.prototype.constructor = Bird;

Bird.prototype.update = function() {

  if(this.angle < 90 && this.alive) {
    this.angle += 2.5;
  }
  // write your prefab's specific update code here

};

Bird.prototype.flap = function(){
    if(!!this.alive) {
        this.flapSound.play();
        //cause our bird to "jump" upward
        this.body.velocity.y = -400;
        // rotate the bird to -40 degrees
        this.game.add.tween(this).to({angle: -40}, 100).start();
    }
};

Bird.prototype.revived = function() {
};

Bird.prototype.onKilled = function() {
    this.exists = true;
    this.visible = true;
    this.animations.stop();
    var duration = 90 / this.y * 300;
    this.game.add.tween(this).to({angle: 90}, duration).start();
    console.log('killed');
    console.log('alive:', this.alive);
};

module.exports = Bird;

},{}],3:[function(require,module,exports){
'use strict';

var Ground = function(game, x, y, width,height) {
  Phaser.TileSprite.call(this, game, x, y, width,height,'ground');

  this.autoScroll(-200,0);

  // initialize your prefab here

  this.game.physics.arcade.enableBody(this);

  this.body.allowGravity = false;
  this.body.immovable = true;
};

Ground.prototype = Object.create(Phaser.TileSprite.prototype);
Ground.prototype.constructor = Ground;

Ground.prototype.update = function() {
  
  // write your prefab's specific update code here
  
};

module.exports = Ground;

},{}],4:[function(require,module,exports){
'use strict';

var Pipe = function(game, x, y, frame) {
  Phaser.Sprite.call(this, game, x, y, 'pipe', frame);

  this.anchor.setTo(0.5,0.5);
  this.game.physics.arcade.enableBody(this);

  this.body.allowGravity = false;
  this.body.immovable = true;
  // initialize your prefab here
  
};

Pipe.prototype = Object.create(Phaser.Sprite.prototype);
Pipe.prototype.constructor = Pipe;

Pipe.prototype.update = function() {
  
  // write your prefab's specific update code here
  
};

module.exports = Pipe;

},{}],5:[function(require,module,exports){
'use strict';
var Pipe = require('./pipe');

var PipeGroup = function(game, parent) {
  Phaser.Group.call(this, game, parent);

  this.topPipe = new Pipe(this.game,0,0,0);
  this.add(this.topPipe);

  this.bottomPipe = new Pipe(this.game,0,440,1);
  this.add(this.bottomPipe);

  this.hasScored = false;

  this.setAll('body.velocity.x',-200);
  // initialize your prefab here
  
};

PipeGroup.prototype = Object.create(Phaser.Group.prototype);
PipeGroup.prototype.constructor = PipeGroup;

PipeGroup.prototype.update = function() {
  
  // write your prefab's specific update code here
  this.checkWorldBounds();
  
};

PipeGroup.prototype.reset = function(x,y){
  this.topPipe.reset(0,0);
  this.bottomPipe.reset(0,440);

  this.x = x;
  this.y = y;

  this.setAll('body.velocity.x',-200);

  this.hasScored = false;
  this.exists = true;
};

PipeGroup.prototype.checkWorldBounds = function(){
  if(!this.topPipe.inWorld){
    this.exists = false;
  }
}
module.exports = PipeGroup;

},{"./pipe":4}],6:[function(require,module,exports){
'use strict';

var Scoreboard = function(game,score,playerName) {
    var gameover;
    this.score = score;
    this.playerName = playerName;

    Phaser.Group.call(this,game);
    gameover = this.create(this.game.width/2,100,'gameover');
    gameover.anchor.setTo(0.5,0.5);

    this.scoreboard = this.create(this.game.width/2,200,'scoreboard');
    this.scoreboard.anchor.setTo(0.5,0.5);

    this.scoreText = this.game.add.bitmapText(this.scoreboard.width,180,'flappyfont','',18);
    this.add(this.scoreText);

    this.bestScoreText = this.game.add.bitmapText(this.scoreboard.width,230,'flappyfont','',18);
    this.add(this.bestScoreText);

    this.startButton = this.game.add.button(this.game.width/2,300,'startButton',this.startClick,this);
    this.startButton.anchor.setTo(0.5,0.5);

    this.sendButton = this.game.add.button(this.game.width/2,400,'sendButton',this.sendPoints,this);
    this.sendButton.anchor.setTo(0.5,0.5);

    this.add(this.startButton);
    this.add(this.sendButton);

    this.y=this.game.height;
    this.x=0;


  // initialize your prefab here
  
};

Scoreboard.prototype = Object.create(Phaser.Group.prototype);
Scoreboard.prototype.constructor = Scoreboard;

Scoreboard.prototype.update = function() {
  
  // write your prefab's specific update code here
  
};

Scoreboard.prototype.show = function(score){
    var medal, bestScore;

    this.scoreText.setText(score.toString());

    $.ajax({
        method: 'GET',
        url: 'http://guimo.dev/api_v1/flappyguimo/points',
        async:false
    }).success(function(response){
        if(response.length > 0) {
            bestScore = response[0].points;
        }else{
            bestScore = 0;
        }
    }).error(function(response){
        console.log(response);
    });
    /*if(!!localStorage){
        bestScore = localStorage.getItem('bestScore');
        if(!bestScore || bestScore < score){
            bestScore = score;
            localStorage.setItem('bestScore',bestScore);
        }
    }else{
        bestScore = 'N/A';
    }*/

    this.bestScoreText.setText(bestScore.toString());

    if(score>= 10 && score <20){
        medal = this.game.add.sprite(-65,7,'medals',1);
        medal.anchor.setTo(0.5,0.5);
        this.scoreboard.addChild(medal);
    }else if(score >=20){
        medal = this.game.add.sprite(-65,7,'medals',0);
        medal.anchor.setTo(0.5,0.5);
        this.scoreboard.addChild(medal);
    }

    if(medal){
        var emitter = this.game.add.emitter(medal.x,medal.y,400);
        this.scoreboard.addChild(emitter);
        emitter.width = medal.width;
        emitter.height = medal.height;

        emitter.makeParticles('particle');

        emitter.setRotation(-100,100);
        emitter.setXSpeed(0,0);
        emitter.setYSpeed(0.0);
        emitter.minParticleScale=0.25;
        emitter.maxParticleScale=0.5;
        emitter.setAll('body.allowGravity',false);
        emitter.start(false,1000,1000);
    }

    this.game.add.tween(this).to({y:0},1000,Phaser.Easing.Bounce.Out,true);
};

Scoreboard.prototype.startClick = function(){
    this.game.state.start('play');
};

    Scoreboard.prototype.sendPoints = function(){
        var form_data = {
            playerName: this.playerName,
            points: this.score,
            status:1
        };

        $.ajax({
            method: 'POST',
            url: 'http://guimo.dev/api_v1/flappyguimo/points',
            data:form_data
        }).success(function(response){
            $("#ranking").html('<tr class="text-center"><td colspan="4"><i class="fa fa-spin fa-spinner"></i></td></tr>');

            $.ajax({
                method: 'GET',
                url: 'http://guimo.dev/api_v1/flappyguimo/points'
            }).success(function(response){
                var tbody = $("#ranking");
                var trclass = '';
                var trophy = '';
                tbody.html('');
                for(var i =0; i < response.length;i++){
                    trclass = '';
                    trophy  = '';
                    var d = new Date(response[i].created_at);
                    if(i == 0){
                        trclass= 'success text-bold text-success';
                        trophy = '<i class="fa fa-trophy text-success"></i>';
                    }

                    if(i > 0 && i <= 2){
                        trclass= 'info text-bold text-info'
                    }

                    if( (i+1) == response.length){
                        trclass= 'danger text-bold text-danger';
                        trophy = '<i class="fa fa-frown-o text-danger"></i>';
                    }
                    tbody.append('<tr class="text-center '+trclass+'"><td>'+(i+1)+'º '+trophy+'</td><td>'+response[i].playerName+'</td><td>'+response[i].points+'</td><td>'+addZero(d.getDate())+'/'+ addZero(d.getMonth())+'/'+ d.getFullYear()+' '+ addZero(d.getHours())+':'+ addZero(d.getMinutes())+':'+addZero(d.getSeconds())+'</td></tr>');
                }
            }).error(function(response){
                console.log(response);
            });
        }).error(function(response){
            console.log(response);
        });
    };

module.exports = Scoreboard;

},{}],7:[function(require,module,exports){

'use strict';

function Boot() {
}

Boot.prototype = {
  preload: function() {
    this.load.image('preloader', 'assets/images/preloader.gif');
  },
  create: function() {
    this.game.input.maxPointers = 1;
    this.game.state.start('preload');
  }
};

module.exports = Boot;

},{}],8:[function(require,module,exports){

'use strict';
function GameOver() {}

GameOver.prototype = {
  preload: function () {

  },
  create: function () {
    var style = { font: '65px Arial', fill: '#ffffff', align: 'center'};
    this.titleText = this.game.add.text(this.game.world.centerX,100, 'Game Over!', style);
    this.titleText.anchor.setTo(0.5, 0.5);

    this.congratsText = this.game.add.text(this.game.world.centerX, 200, 'You Win!', { font: '32px Arial', fill: '#ffffff', align: 'center'});
    this.congratsText.anchor.setTo(0.5, 0.5);

    this.instructionText = this.game.add.text(this.game.world.centerX, 300, 'Click To Play Again', { font: '16px Arial', fill: '#ffffff', align: 'center'});
    this.instructionText.anchor.setTo(0.5, 0.5);
  },
  update: function () {
    if(this.game.input.activePointer.justPressed()) {
      this.game.state.start('play');
    }
  }
};
module.exports = GameOver;

},{}],9:[function(require,module,exports){

'use strict';
function Menu() {}

Menu.prototype = {
  preload: function() {

  },
  create: function() {
      this.playerName = document.getElementById('playerName');
    this.background = this.game.add.sprite(0,0,'background');
    this.ground  = this.game.add.tileSprite(0,400,335,112,'ground');
    this.ground.autoScroll(-200,0);

    /**CREATE A TITLE GROUP TO PUT THE TITLE IN **/
    this.titleGroup = this.game.add.group();

    /**INSTANTIATE TITLE AND PUT IN A TITLE GROUP **/
    this.title =this.game.add.sprite(0,0,'title');
    this.titleGroup.add(this.title);

    /**CREATE BIRD AND PUT IN A TITLE GROUP **/
    this.bird = this.game.add.sprite(200,5,'bird');
    this.titleGroup.add(this.bird);

    /**CREATE BIRD ANIMATION **/
    this.bird.animations.add('flap');
    this.bird.animations.play('flap',12,true);

    /**SET LOCALE TO TITLE GROUP**/
    this.titleGroup.x=30;
    this.titleGroup.y=100;

    this.game.add.tween(this.titleGroup).to({y:115},300,Phaser.Easing.Linear.None,true,0,1000,true);

    this.startButton = this.game.add.button(this.game.width/2,300,'startButton',this.startClick,this);
    this.startButton.anchor.setTo(0.5,0.5);

      this.game.add.bitmapText(10,20,'flappyfont','Bem Vindo ',18);
      this.playerNameString = this.game.add.bitmapText(110,21,'flappyfont',this.playerName.value.toString(),16);


  },
  startClick:function(){
      $("#playerName").attr('disabled',true);
    this.game.state.start('play');
  },
  update: function() {
        this.playerNameString.setText(this.playerName.value.toString());
  }
};

module.exports = Menu;

},{}],10:[function(require,module,exports){

'use strict';
var Bird = require('../prefabs/bird');
var Ground = require('../prefabs/ground');
var Pipe = require('../prefabs/pipe');
var PipeGroup = require('../prefabs/pipeGroup');
var Scoreboard = require('../prefabs/scoreboard');

function Play() {
}
Play.prototype = {
    create: function() {
        this.playerName = $("#playerName").val();
        // start the phaser arcade physics engine
        this.game.physics.startSystem(Phaser.Physics.ARCADE);


        // give our world an initial gravity of 1200
        this.game.physics.arcade.gravity.y = 1200;

        // add the background sprite
        this.background = this.game.add.sprite(0,0,'background');

        // create and add a group to hold our pipeGroup prefabs
        this.pipes = this.game.add.group();

        // create and add a new Bird object
        this.bird = new Bird(this.game, 100, this.game.height/2);
        this.game.add.existing(this.bird);



        // create and add a new Ground object
        this.ground = new Ground(this.game, 0, 400, 335, 112);
        this.game.add.existing(this.ground);


        // add keyboard controls
        this.flapKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.flapKey.onDown.addOnce(this.startGame, this);
        this.flapKey.onDown.add(this.bird.flap, this.bird);


        // add mouse/touch controls
        this.game.input.onDown.addOnce(this.startGame, this);
        this.game.input.onDown.add(this.bird.flap, this.bird);


        // keep the spacebar from propogating up to the browser
        this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);



        this.score = 0;
        this.scoreText = this.game.add.bitmapText(this.game.width/2, 10, 'flappyfont',this.score.toString(), 24);

        this.instructionGroup = this.game.add.group();
        this.instructionGroup.add(this.game.add.sprite(this.game.width/2, 100,'getReady'));
        this.playerNameString = this.game.add.bitmapText((this.game.width/2)+25,125,'flappyfont',this.playerName,16);
        this.instructionGroup.add(this.game.add.sprite(this.game.width/2, 325,'instructions'));
        this.instructionGroup.setAll('anchor.x', 0.5);
        this.instructionGroup.setAll('anchor.y', 0.5);

        this.pipeGenerator = null;

        this.gameover = false;

        this.pipeHitSound = this.game.add.audio('pipeHit');
        this.groundHitSound = this.game.add.audio('groundHit');
        this.scoreSound = this.game.add.audio('score');

    },
    update: function() {
        // enable collisions between the bird and the ground
        this.game.physics.arcade.collide(this.bird, this.ground, this.deathHandler, null, this);

        if(!this.gameover) {
            // enable collisions between the bird and each group in the pipes group
            this.pipes.forEach(function(pipeGroup) {
                this.checkScore(pipeGroup);
                this.game.physics.arcade.collide(this.bird, pipeGroup, this.deathHandler, null, this);
            }, this);
        }



    },
    shutdown: function() {
        this.game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);
        this.bird.destroy();
        this.pipes.destroy();
        this.scoreboard.destroy();
    },
    startGame: function() {
        if(!this.bird.alive && !this.gameover) {
            this.playerNameString.setText('');
            this.bird.body.allowGravity = true;
            this.bird.alive = true;
            // add a timer
            this.pipeGenerator = this.game.time.events.loop(Phaser.Timer.SECOND * 1.25, this.generatePipes, this);
            this.pipeGenerator.timer.start();

            this.instructionGroup.destroy();
        }
    },
    checkScore: function(pipeGroup) {
        if(pipeGroup.exists && !pipeGroup.hasScored && pipeGroup.topPipe.world.x <= this.bird.world.x) {
            pipeGroup.hasScored = true;
            this.score++;
            this.scoreText.setText(this.score.toString());
            this.scoreSound.play();
        }
    },
    deathHandler: function(bird, enemy) {
        if(enemy instanceof Ground && !this.bird.onGround) {
            this.groundHitSound.play();
            this.scoreboard = new Scoreboard(this.game,this.score,this.playerName);
            this.game.add.existing(this.scoreboard);
            this.scoreboard.show(this.score);
            this.bird.onGround = true;
        } else if (enemy instanceof Pipe){
            this.pipeHitSound.play();
        }

        if(!this.gameover) {
            this.gameover = true;
            this.bird.kill();
            this.pipes.callAll('stop');
            this.pipeGenerator.timer.stop();
            this.ground.stopScroll();
        }

    },
    generatePipes: function() {
        var pipeY = this.game.rnd.integerInRange(-100, 100);
        var pipeGroup = this.pipes.getFirstExists(false);
        if(!pipeGroup) {
            pipeGroup = new PipeGroup(this.game, this.pipes);
        }
        pipeGroup.reset(this.game.width, pipeY);


    }
};

module.exports = Play;

},{"../prefabs/bird":2,"../prefabs/ground":3,"../prefabs/pipe":4,"../prefabs/pipeGroup":5,"../prefabs/scoreboard":6}],11:[function(require,module,exports){

'use strict';
function Preload() {
  this.asset = null;
  this.ready = false;
}

Preload.prototype = {
    preload: function() {
        this.load.onLoadComplete.addOnce(this.onLoadComplete,this);
        this.asset = this.add.sprite(this.width/2,this.height/2,'preloader');
        this.asset.anchor.setTo(0.5,0.5);
        this.load.setPreloadSprite(this.asset);

        this.load.bitmapFont('flappyfont','assets/fonts/flappyfont/flappyfont.png','assets/fonts/flappyfont/flappyfont.fnt');

        this.load.image('background','assets/images/background.png');
        this.load.image('ground','assets/images/ground.png');
        this.load.image('title','assets/images/title.png');
        this.load.image('startButton','assets/images/start-button.png');
        this.load.image('sendButton','assets/images/send-button.png');
        this.load.image('instructions','assets/images/instructions.png');
        this.load.image('getReady','assets/images/get-ready.png');
        this.load.image('scoreboard','assets/images/scoreboard.png');
        this.load.image('gameover','assets/images/gameover.png');
        this.load.image('particle','assets/images/particle.png');

        this.load.audio('score','assets/audio/score.wav');
        this.load.audio('flap','assets/audio/flap.wav');
        this.load.audio('pipeHit','assets/audio/pipe-hit.wav');
        this.load.audio('groundHit','assets/audio/ground-hit.wav');

        this.load.spritesheet('bird','assets/images/bird.png',34,24,3);
        this.load.spritesheet('pipe','assets/images/pipes.png',54,320,2);
        this.load.spritesheet('medals','assets/images/medals.png',44,46,2);
    },
    create: function() {
        this.asset.cropEnabled = false;
    },
    update: function() {
        if(!!this.ready) {
            this.game.state.start('menu');
        }
    },
    onLoadComplete: function() {
        this.ready = true;
    }
};

module.exports = Preload;

},{}]},{},[1])