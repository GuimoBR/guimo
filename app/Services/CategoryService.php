<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 22:34
 */

namespace Guimo\Services;


use Guimo\Entities\Category;
use Guimo\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;
use DateTime;

class CategoryService
{

    /**
     * @var CategoryRepository
     */
    private $repository;
    private $rules;
    private $messages;
    private $customAttributes;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
        $this->rules =[
            'name' =>  'required|max:60',
            'description' =>'required',
            'slug' => 'required|max:50',
        ];

        $this->messages = [
            'required'  => 'O campo :attribute é obrigatório',
            'max'       => 'O campo :attribute deve ter no máximo :max caractéres',
        ];
        $this->customAttributes = [
            'name'=>"Nome",
            'slug'=>"URL",
            'description'=>'Descrição',
        ];
    }

    public function getAllCategories()
    {
        return $this->repository->all();
    }

    public function getProductsFromCategorySlug($slug){
        return $this->repository->with(['products'])->findWhere(['slug'=>$slug])->first();
    }

    public function getCategoryNameFromSlug($slug){
        $category = $this->repository->findWhere(['slug'=>$slug])->first();
        return $category->name;
    }

    public function getAllCategoriesWithProducts(){
        $categories = $this->repository->with(['products'])->all();

        return $categories;
    }

    public function insert(array $data){
        unset($data['_token']);
        $validator = Validator::make($data,$this->rules,$this->messages,$this->customAttributes);
        if($validator->fails()){
            return $validator;
        }else{
            $this->repository->create($data);
            return true;
        }
    }

    public function update($slug,$data){
        $category = $this->repository->findWhere(['slug'=>$slug])->first();
        $validator = Validator::make($data,$this->rules,$this->messages,$this->customAttributes);
        if($validator->fails()){
            return $validator;
        }else{
            unset($data['_token']);
            $this->repository->update($data,$category->id);
            return true;
        }
    }
}