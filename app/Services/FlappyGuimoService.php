<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 24/12/15
 * Time: 01:47
 */

namespace Guimo\Services;


use Exception;
use Guimo\Entities\FlappyGuimo;
use Guimo\Repositories\FlappyGuimoRepository;
use Illuminate\Database\QueryException;

class FlappyGuimoService
{

    /**
     * @var FlappyGuimoRepository
     */
    protected $repository;

    /**
     * FlappyGuimoService constructor.
     * @param FlappyGuimoRepository $repository
     */
    public function __construct(FlappyGuimoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function create(array $data){
        try{
            $this->repository->create($data);
            return response()->json(['success'=>true,'message'=>'Pontos enviados com sucesso!']);
        }catch(QueryException $e){
            return ['error'=>true, 'message'=>$e->getMessage()];
        }

    }

    public function findWhere(array $where,$columns = ['*']){
        return $this->repository->findWhere($where,$columns);
    }

    public function getRanking($limit = 15){
        $ranking = FlappyGuimo::where('status',1)
            ->orderBy('points','desc')
            ->orderBy('created_at','desc')
            ->take($limit)
            ->get();

        return $ranking;
    }

    public function all(){
        return FlappyGuimo::where('status',1)
            ->orderBy('points','desc')
            ->orderBy('created_at','desc')
            ->get();
    }

    public function resetRanking(){
        try{
            FlappyGuimo::where('status',1)->where('created_at','<',new \DateTime('now'))->update(['status'=>0]);
            return ['success'=>true,'message'=>'Ranking resetado com sucesso'];
        }catch(Exception $e){
            return ['error'=>true, 'message'=>$e->getMessage()];
        }
    }
}