@extends('layouts.store')

@section('content')
    <div class="row carousel-holder">

        <div class="col-md-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <a href="#"><img class="slide-image" src="https://placehold.it/800x300" alt="" style="height: 300px;"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img class="slide-image" src="https://placehold.it/800x300" alt="" style="height: 300px;"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img class="slide-image" src="https://placehold.it/800x300" alt="" style="height: 300px;"></a>
                    </div>
                    <div class="item">
                        <a href="#"><img class="slide-image" src="https://placehold.it/800x300" alt="" style="height: 300px;"></a>
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        @if(!is_null($products))
        @forelse($products as $product)
            <div class="col-sm-6 col-lg-4 col-md-4">
                <div class="thumbnail">
                    @if(isset($slug))
                        <a href="{{route('store.product',['cslug'=>$slug,'pslug'=>$product->slug])}}"><img src="{{url('image',['slug'=>$product->slug,'image'=>$product->thumb_image])}}" alt="{{$product->name}}" style="height: 200px;"></a>
                        @else
                        <a href="{{route('store.product',['cslug'=>$product->categories[0]->slug,'pslug'=>$product->slug])}}"><img src="{{url('image',['slug'=>$product->slug,'image'=>$product->thumb_image])}}" alt="{{$product->name}}" style="height: 200px;"></a>
                        @endif

                    <div class="caption" style="height: 200px;">
                        @if(isset($slug))
                            <h4><a href="{{route('store.product',['cslug'=>$slug,'pslug'=>$product->slug])}}">{{$product->name}}</a></h4>
                        @else
                            <h4><a href="{{route('store.product',['cslug'=>$product->categories[0]->slug,'pslug'=>$product->slug])}}">{{$product->name}}</a></h4>
                        @endif

                            @if($product->discount_percent > 0)
                                <h4>R$ <s class="text-danger">{{$product->price}}</s> <i class="fa fa-long-arrow-right"></i> {{$product->final_price}} <small class="text-success">(-{{$product->discount_percent}}%)</small></h4>
                            @else
                                <h4>R$ {{$product->final_price}}</h4>
                            @endif

                            @if(isset($slug))
                                <p style="height: 90px;">{!! substr(nl2br($product->description),0,150)!!}...</p><hr>
                            @else
                                <p style="height: 90px;">{!! substr(nl2br($product->description),0,150)!!}...<p><hr>
                            @endif
                    </div>
                    <div class="col-md-12">
                        @if(isset($slug))
                            <p><a href="{{route('store.product',['cslug'=>$slug,'pslug'=>$product->slug])}}" class="btn btn-success btn-block btn-raised">Mais detalhes</a></p>
                            @else
                            <p><a href="{{route('store.product',['cslug'=>$product->categories[0]->slug,'pslug'=>$product->slug])}}" class="btn btn-success btn-block btn-raised">Mais detalhes</a></p>
                        @endif
                    </div>
                    <div class="ratings">
                        <p class="pull-right">15 reviews</p>
                        <p>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                        </p>
                    </div>
                </div>
            </div>
        @empty
            <p class="lead text-center text-warning">Nenhum produto cadastrado até o momento, por favor, volte mais tarde.</p>
        @endforelse
        <div class="col-md-12 col-sm-12 text-center">
            {!!$products->render() !!}
        </div>
        @else
        <div class="col-md-12">
            <p class="lead text-center text-warning">Nenhum produto cadastrado até o momento, por favor, volte mais tarde.</p>
        </div>
        @endif
    </div>
@endsection