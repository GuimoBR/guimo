<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 14/03/2016
 * Time: 18:56
 */

return [
    'splash1'   => 'Please wait while loading',
    'splash2'   => 'You will be surprised...',
    'splash3'   => '...with the Guimo can do!',
    'home'      => 'Home',
    'aboutus'   => 'About Us',
    'team'      => 'Team',
    'services'  => 'Services',
    'portfolio' => 'Portfolio',
    'testimonial' => 'Testimonial',
    'blog'      => 'Blog',
    'contact'   => 'Contact',
    'guimowel'     => 'Guimo - Entertainment and Education from another world!',
    'welcome'   => "Welcome to <span class=\"id-color\">Guimo</span>",
    'slider1'   => "<span class=\"id-color\">Educational</span> + Fun",
    'slider2'   => "Toy + <span class=\"id-color\">Technology</span>",
    'hiimguimo'    => "Hi, I am <span class=\"id-color\">Guimo!</span>",
    'helloworld' => "Hello <span class=\"id-color\" >World!</span>",
    'guimohistory' => 'Oi, me chamo guimo, sou um robo viajante do planeta Guimundo. Enquanto explorávamos o sistema solar,
    algo muito estranho aconteceu: um raio muito poderoso nos atingiu e caimos em um planeta não identificado em
    nossos Arquivos de Planetas. Eu fiquei perdido, mas você me encontrou! Preciso muito de sua ajuda para buscar meus
    outros amigos Guimo e descobrir o mistério de quem derrubou nossa nave',
    'guimohead' => "In Guimundo we can exchange our parts and gain new skills.
    For this I need your help to recover my pieces and assembling as you want!",
    'guimobody' => "But I need your help to remember how to perform my duties. Just connect your Smartphone, assemble my pieces and arrange my schedule blocks.",
    'whatmore'  => "What can I <span class=\"id-color\">do?</span>",
    'app'       => "App",
    "apptxt"    => "Always with new updates, the application allows you to have your always Guimo hand.",
    'games'     => "Games",
    "gamestxt"  => "Inside and outside the app you can have fun with various games using Guimo, your smartphone, or both together.",
    'edu'       => 'Educational',
    'edutxt'    => 'More than just fun, in my app you can learn about programming in a relaxed and fun way. What awesome!',
    'modular'   => 'Modulate',
    'modulartxt' => 'Some parts of my body can be treated. So, you can ride your Guimo with different shapes and features.',
    'bluetooth' => 'Wireless',
        'bluetoothtxt' => 'With Bluetooth 4.0, I spend little energy and the fun can go even further. This is so awesome!!!',
    'custom'    => "Customizable",
    'customtxt' => 'Besides exchanging my pieces with any other Guimo, you can use a 3D printer to invent new parts.',
    'partners'  => "Partners",
    'secteam'      => "Meet my <span class='id-color'>Earth Friends</span>",
    'submit'    => "Submit",
    'submiting' => "Submiting... <i class='fa fa-spinner fa-spin'></i>",
    "contactus" => "<span class='id-color'>Are you interested?</span> Leave your email to us!",
    "youremail" => "Your Email",
    'wrongEmail' => 'The email you entered is not in the correct format, please try again!',
    'emptyEmail' => "The email field can't be empty...",
    'successEmail' => "Email registered successfully!"
];