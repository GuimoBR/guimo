@extends('layouts.store')
@section('content')

    <div class="col-md-12">

        <div class="well">
            <h3><i class="fa fa-shopping-basket"></i> Meu cesto de compras  </h3>
            <div class="row" style="height: 500px; max-height: 500px; overflow-y: auto;">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <!--<th></th>-->
                        <th  class="text-center">Quantidade</th>
                        <th  class="text-center">Produto</th>
                        <th  class="text-center">Preço (R$)</th>
                        <th  class="text-center">Desconto</th>
                        <th class="text-center">Quantidade</th>
                        <th class="text-center">Preço final(R$)</th>
                        <th  class="text-center">Total (R$)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!is_null($session) && isset($session['cart']))
                        @foreach($session['cart'] as $product)
                            <tr class="text-center">
                                <!--<td>Imagem</td>-->
                                <td style="width: 5%">{{$product->quantity}}</td>
                                <td style="width: 15%">{{$product->name}}</td>
                                <td style="width: 15%">{{$product->price}}</td>
                                <td style="width: 15%">{{$product->discount_percent}}%</td>
                                <td style="width: 10%">{{$product->quantity}}</td>
                                <td style="width: 15%">{{$product->quantity}} x {{number_format($product->final_price,2,',','.')}}</td>
                                <td style="width: 20%" class="text-info">{{number_format($product->total_price,2,',','.')}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7" class="text-center"><strong class="text-info">Nenhum produto encontrado em seu carrinho!</strong></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5">
                    @if(!is_null($session) && isset($session['totalProducts']) && isset($session['totalPrice']))
                        <p class="lead">Você tem {{$session['totalProducts']}} produto(s) no seu cesto.
                            <br><small>Frete: {{$session['shipment'][0]['method']}} {{$session['shipment'][0]['prazo']}} dia(s) uteis - <strong>R$</strong> {{number_format($session['shipment'][0]['price'],2,',','.')}}</small>
                            <br> Total: R$ {{number_format($session['totalPrice'],2,',','.')}}</p>
                    @endif
                </div>
                <div class="col-md-7 text-right">
                    <a href="{{route('store.index')}}" class="btn btn-info btn-lg btn-raised">Continhar Comprando <i class="fa fa-shopping-cart"></i></a>
                    @if(!is_null($session) && isset($session['cart']))
                        <a href="{{route('cart.clear')}}" class="btn btn-danger btn-lg btn-raised ">Esvaziar Cesto <i class="fa fa-trash"></i></a>
                        <button class="btn btn-primary btn-lg btn-raised" type="button">Finalizar Compra <i class="fa fa-arrow-right"></i> </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection