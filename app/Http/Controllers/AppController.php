<?php

namespace Guimo\Http\Controllers;

use Carbon\Carbon;
use Guimo\Repositories\AppRepository;
use Guimo\Repositories\CountryRepository;
use Guimo\Services\AppService;
use Guimo\Services\RegisterCodeService;
use Guimo\Services\UserService;
use Illuminate\Http\Request;

use Guimo\Http\Requests;
use Guimo\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{

    /**
     * @var User;
     */
    private $user;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var AppRepository
     */
    private $repository;
    /**
     * @var AppService
     */
    private $appService;

    public function __construct(AppRepository $repository, AppService $appService,  UserService $userService)
    {

        $this->user = Auth::user();
        if(!is_null($this->user->info)) {
            $this->user->info->birthdate = Carbon::parse($this->user->info->birthdate)->format('d/m/Y');
        }

        $this->userService = $userService;
        $this->repository = $repository;
        $this->appService = $appService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($token = null)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    public function getandroid(){

        $apps = $this->appService->getByOS('android');
        return view('dashboard.app_getandroid',['user'=>$this->user,'apps'=>$apps]);
    }

    public function uploadAndroid(Request $request){
        $upload = $this->appService->uploadApp($request);

        return $upload;

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $destroy = $this->appService->delete($id);
        $apps = $this->appService->getByOS('android');
        return redirect('/admin/apps/android')->with('destroy',$destroy['error'])->with('message',$destroy['message']);

    }
}
