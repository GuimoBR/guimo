<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CountryRepository
 * @package namespace Guimo\Repositories;
 */
interface CountryRepository extends RepositoryInterface
{
    //
}
