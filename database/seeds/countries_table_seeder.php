<?php

use Illuminate\Database\Seeder;

class countries_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('countries')->delete();

        $countries = array(
            ['name'=>"Brasil",'is_avaible'=>true],
        );

        foreach($countries as $country){
            DB::table('countries')->insert($country);
        }
    }
}
