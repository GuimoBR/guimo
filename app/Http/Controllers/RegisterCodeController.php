<?php

namespace Guimo\Http\Controllers;

use Guimo\Repositories\CountryRepository;
use Guimo\Services\EmailService;
use Guimo\Services\RegisterCodeService;
use Guimo\Services\UserService;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Guimo\Http\Requests;
use Guimo\Http\Controllers\Controller;

class RegisterCodeController extends Controller
{
    /**
     * @var CountryRepository
     */
    private $countryRepository;
    /**
     * @var RegisterCodeService
     */
    private $registerCodeService;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var EmailService
     */
    private $emailService;

    public function __construct(CountryRepository $countryRepository,UserService $userService, RegisterCodeService $registerCodeService, EmailService $emailService)
    {
        $this->countryRepository = $countryRepository;
        $this->registerCodeService = $registerCodeService;
        $this->emailService = $emailService;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($token = null)
    {
        //
        $countries = null;
        if(!is_null($token)){
            $countries = $this->countryRepository->findWhere(['is_avaible'=>true]);
        }
        $registerCode = $this->registerCodeService->searchByToken($token,true);


        if(count($registerCode) < 1){
            return view('dashboard.newuser',['token'=>$token,'is_valid'=>false, 'countries'=>$countries]);
        }else{
            $registerCode = $registerCode[0];
            $data['is_accessible'] = false;
            $data['accessed_at'] = Carbon::now()->toDateTimeString();
            if($this->registerCodeService->updateAccessCode($registerCode->id,$data)){
                return view('dashboard.newuser',['token'=>$token,'is_valid'=>true,  'countries'=>$countries]);
            }else{
                return redirect('/');
            }
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data['token'] = str_random(40);
        $data['is_accessible'] = true;
        $data['created_at']    =   Carbon::now()->toDateTimeString();
        if($this->registerCodeService->insert($data)){
            return redirect()->route('user.adminCode');
        }
        return null;
    }

    public function sendCodeEmail(Request $request){
        $data = $request->all();
        $sendEmail = $this->emailService->sendPage($data);
        if(is_bool($sendEmail)){
            return redirect()->route('user.adminCode')->with('message','Email enviado com sucesso!');
        }else{
            return redirect()->route('user.adminCode')->withErrors($sendEmail);
        }
    }

    public function storeWithToken($token = null,Request $request){
        $countries = null;
        $validation = null;

        if(!is_null($token)){
            $countries = $this->countryRepository->findWhere(['is_avaible'=>true]);
        }
        //  $registerCode = $this->registerCodeService->searchByToken($token,true);

        $createuser = $this->userService->create($request);
        if(is_bool($createuser)){

            //return redirect("/admin/new/$token")->with(['is_valid'=>true,'token'=>$token,'message'=>'Parabéns, você já está cadastrado no Banco de dados do Guimo. Faça o login para continuar seu cadastro!']);
            return view('dashboard.newuser',['is_valid'=>true,'token'=>$token,'message'=>"Parabéns, você já está cadastrado no Banco de dados do Guimo. Faça o <strong><a href=".route('login.index').">login</a></strong> para continuar seu cadastro!"]);
        }else{
            return redirect("/admin/new/$token")->with(['countries'=>$countries,'token'=>$token,'is_valid'=>true])->withInput($request->all())->withErrors($createuser);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
