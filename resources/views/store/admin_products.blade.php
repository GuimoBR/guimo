@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/TableTools.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Lista de todas as categorias cadastradas</h2>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @if($errors->has())

                        <div class="alert alert-callout alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p><strong>Atenção, alguns erros foram detectados:</strong></p>
                            <ul class="list-unestyled">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                            <div class="alert alert-callout alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <p>{{Session::get('message')}}</p>
                            </div>
                        @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Produtos</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <button class="btn btn-default-dark btn-flat btn-block ink-reaction" data-toggle="modal" data-target=".bs-example-modal-lg">Novo Produto <i class="fa fa-plus-circle"></i> </button></div>
                            </div>
                        </div>
                        <div class="card-body style-default-bright">
                            <p class="text-danger">Atenção, alguns produtos podem estar cadastrados em <strong>mais de uma categoria.</strong></p>

                                <table class="table table-stripped table order-column table-striped hover" id="products" data-swftools="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf')}}">
                                    <thead>
                                    <tr class="headings">

                                        <th class="text-center">#</th>
                                        <th class="text-center">Nome</th>
                                        <th class="text-center">Descrição</th>
                                        <th class="text-center">Categorias</th>
                                        <th class="text-center">Preço (R$)</th>
                                        <th class="text-center">Desconto</th>
                                        <th class="text-center">Estoque</th>
                                        <th class="text-center">Disponivel?</th>
                                        <th class="text-center">Adicionado Em</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr class="text-center">
                                            <td>{{$product->id}}</td>
                                            <td><a href="{{route('admin.store.product',['slug'=>$product->slug])}}">{{$product->name}} <i class="fa fa-edit"></i> </a></td>
                                            <td style="width: 30%">{!! substr(nl2br($product->description),0,70) !!}...</td>
                                            <td style="width: 10%">
                                                @foreach($product->categories as $category)
                                                    <span class="label label-primary">{{$category->name}}</span>
                                                @endforeach
                                            </td>
                                            <td>{{$product->price}}</td>
                                            <td>{{$product->discount_percent}}%</td>
                                            <td>{{$product->stock}}</td>
                                            @if($product->is_avaible)
                                                <td class="text-success">Sim</td>
                                            @else
                                                <td class="text-danger">Não</td>
                                            @endif
                                            <td>{{$product->created_at->format('d/m/Y H:i:s')}}</td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Novo Produto</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.store.new_product')}}" method="POST" id="create_product" class="form" enctype="multipart/form-data">
                                @include('store.form_new_product',['product'=>null])
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary" id="submit">Criar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('assets/js/string_to_slug.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script>
        $(document).ready(function(){

            $("#submit").click(function(){
                $("#create_product").submit();
            });

            $("#name").keyup(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            $("#name").change(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            @if(count($products) > 0)
            var oTable = $('#products').dataTable({
                "dom": 'T<"clear">lfrtip',
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "tableTools": {
                    "sSwfPath": $('#products').data('swftools')
                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
            @endif

            $("#price").keyup(function(){
                var discount = 1 - ($("#discount_percent").val()/100);
                var final_price = $(this).val() * discount;
                $("#final_price").val(final_price.toFixed(2));
            });

            $("#discount_percent").keyup(function(){
                var discount = 1 - ($("#discount_percent").val()/100);
                var final_price = $("#price").val() * discount;
                $("#final_price").val(final_price.toFixed(2));
            });

            $("#discount_percent").change(function(){
                var discount = 1 - ($("#discount_percent").val()/100);
                var final_price = $("#price").val() * discount;
                $("#final_price").val(final_price.toFixed(2));
            });




        });
    </script>
@endsection