<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Comment extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = "comments";
    protected $fillable = ['product_id','user_id','comment','is_readable'];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function owner(){
        return $this->belongsTo(User::class);
    }

}
