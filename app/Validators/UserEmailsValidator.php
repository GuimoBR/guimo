<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 22:38
 */

namespace Guimo\Validators;


use Prettus\Validator\LaravelValidator;

class UserEmailsValidator extends LaravelValidator
{

    protected $rules = [
        'email' =>  'required|email',
    ];

    protected $messages = [
        'email.required'  => 'O campo :attribute é obrigatório',
    ];

    protected $attributes = [
        'email' => 'e-mail'
    ];
}