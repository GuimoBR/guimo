@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/css/icheck/flat/blue.css')}}">
    <style>
        .hide-row{
            padding: 0 !important;
        }
    </style>
@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Emails padrão criados ate o momento</h2>
        </div>
        <div class="section-body">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Status</header>
                            <div class="clearfix"></div>
                        </div>

                        <div class="card-body style-default-bright">
                            <table class="table table-stripped responsive-utilities" id="emails_table">
                                <thead>
                                <tr class="headings">

                                    <th class="text-center">Email</th>
                                    <th class="text-center">Data</th>
                                    <th class="text-center">Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_array($emailList))
                                    <tr>
                                        <td colspan="5" class="warning text-center" style="border-collapse: collapse;">Status disponivel para apenas emails enviados nos ultimos <strong>dois dias</strong></td>
                                    </tr>
                                @else
                                    @foreach($emailList as $key => $status)
                                        <?php
                                        $class= '';
                                        if($status[count($status)-1]->event == 'failed'){
                                            $class = "danger";
                                        }
                                        if($status[count($status)-1]->event == 'accepted'){
                                            $class='info';
                                        }
                                        if($status[count($status)-1]->event == 'opened'){
                                            $class='success';
                                        }
                                        ?>
                                        <tr class="text-center">
                                            <td>{{$key}}</td>
                                            <td>{{date('d/m/Y H:i:s',$status[count($status)-1]->timestamp)}}</td>
                                            <td class="{{$class}}"><strong>{{$status[count($status)-1]->event}}</strong></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
        @section('script')
            <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
            <script>
                $(document).ready(function(){

                    @if(is_array($emailList))
                    var oTable = $('#emails_table').dataTable({
                        "dom": 'T<"clear">lfrtip',
                        //"order": [[1, 'asc']],
                        "colVis": {
                            "buttonText": "Columns",
                            "overlayFade": 0,
                            "align": "right"
                        },
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                            "lengthMenu": '_MENU_ entries per page',
                            "search": '<i class="fa fa-search"></i>',
                            "paginate": {
                                "previous": '<i class="fa fa-angle-left"></i>',
                                "next": '<i class="fa fa-angle-right"></i>'
                            }
                        }
                    });
                    @endif

        });
        $('.collapse').on('show.bs.collapse', function () {
            $('.collapse.in').collapse('hide');
        });
    </script>
    @endsection