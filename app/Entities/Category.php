<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'categories';
    protected $fillable = ['name','description','slug'];


    public function products(){
        return $this->belongsToMany(Product::class,'categories_products','category_id','product_id');
    }
}
