/**
 * Created by jorge on 20/04/2016.
 */
    $("#searchCep").click(function(){
        var cep = $("#CEP");
        toastr.options = {
            "closeButton":true,
            "progressBar":true,
            "positionClass":'toast-top-right',
            "showMethod":'fadeIn',
            "hideMethod":'fadeOut'
        };

        $("#searchCep").attr('disabled',true);
        $("#searchCep").html('<i class="fa fa-spin fa-spinner"></i>');
        if(cep.val() != ''){
            $.ajax({
                url:url+"/api_v1/correios/consultaCep/"+cep.val()+'',
                method:'GET',
            }).done(function(response){
                if(!$.isEmptyObject(response)){
                    $("#address").val(response.logradouro);
                    $("#neighborhood").val(response.bairro);
                    $("#city").val(response.cidade+' - '+response.uf)
                }else{
                    toastr.error("Atenção, nenhum endereço foi encontrado para este CEP, por favor, tente novamente!",'CEP');
                }
                $("#searchCep").attr('disabled',false);
                $("#searchCep").html('<i class="fa fa-search"></i>');
            })
        }
    });

    $("#CEP").focusout(function(){
        var cep = $("#CEP");
        toastr.options = {
            "closeButton":true,
            "progressBar":true,
            "positionClass":'toast-top-right',
            "showMethod":'fadeIn',
            "hideMethod":'fadeOut'
        };
        if(cep.val() != ''){
            $.ajax({
                url:url+"/api_v1/correios/consultaCep/"+cep.val()+'',
                method:'GET',
            }).done(function(response){
                if(!$.isEmptyObject(response)){
                    $("#address").val(response.logradouro);
                    $("#neighborhood").val(response.bairro);
                    $("#city").val(response.cidade+' - '+response.uf)
                }else{
                    toastr.error("Atenção, nenhum endereço foi encontrado para este CEP, por favor, tente novamente!",'CEP');
                }
            })
        }
    });
