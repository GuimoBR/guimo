@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Emails padrão criados ate o momento</h2>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @if($errors->has())

                        <div class="alert alert-callout alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p><strong>Atenção, alguns erros foram detectados:</strong></p>
                            <ul class="list-unestyled">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-callout alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p>{{Session::get('message')}}</p>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Emails</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <a class="btn btn-default-dark btn-flat btn-block ink-reaction" data-toggle="modal" data-target="#form-modal">Criar Email <i class="fa fa-plus-square-o"></i></a>
                                </div>
                            </div>
                        </div><!--end .card-head -->
                        <div class="card-body style-default-bright">
                        <table class="table table-stripped table order-column table-striped hover" id="emails">
                            <thead>
                            <tr class="headings">

                                <th>#</th>
                                <th>Nome</th>
                                <th>De</th>
                                <th>Tipo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($emails as $email)
                                <tr class="even pointer">
                                    <td>
                                        {{$email->id}}
                                    </td>
                                    <td>
                                        <a href="{{route('emails.show',['id'=>$email->id])}}">{{$email->name}} <i class="fa fa-link"></i></a>
                                    </td>
                                    <td>
                                        {{$email->from}}
                                    </td>
                                    <td>
                                        {{$email->type}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Criar Email padrão</h4>
                </div>
                <form action="{{route('emails.createTemplate')}}" method="POST" id="create_email" class="form" role="form">
                <div class="modal-body">
                    @include("dashboard.email_template")
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary" id="submit">Salvar Email</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {

            CKEDITOR.replace("body_email");
            CKEDITOR.config.image_previewText = "Visualizar imagem";


            var oTable = $('#emails').dataTable({
                "dom": 'T<"clear">lfrtip',
                "order": [[1, 'asc']],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });

            $("#submit").click(function(){
                $("#create_email").submit();
            })
        })
    </script>


@endsection