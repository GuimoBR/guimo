<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Country extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'countries';
    protected $fillable = [
        'name',
        'is_avaible',
        'obs'
    ];

    public function users(){
        return $this->hasMany(UserInfo::class);
    }

}
