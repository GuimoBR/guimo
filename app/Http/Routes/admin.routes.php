<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 15/04/2016
 * Time: 18:48
 */

/**ADMINISTRATIVE ROUTES**/

Route::group(['prefix'=>'/admin'],function(){

    Route::get('/search',['as'=>'admin.search','uses'=>'DashboardController@searchAdmin','middleware'=>'auth']);

    Route::get('/user_emails',[
        'as'=>'user_emails.index',
        'uses'=>'UserEmailsController@index',
        'middleware'=>'auth'
    ]);

    Route::group(['prefix'=>'/user_emails'],function(){
        Route::get('/status',[
            'as'=>'user_emails.status',
            'uses'=>'UserEmailsController@remove',
            'middleware'=>'auth'
        ]);
    });

    Route::group(['prefix'=>'/apps'],function(){
        Route::get('/android',[
            'as'   => 'apps.android',
            'uses' => 'AppController@getAndroid',
            'middleware' => 'auth'
        ]);

        Route::post('/android',[
            'as' => 'apps.androidupload',
            'uses' => "AppController@uploadAndroid",
            'middleware' => 'auth'
        ]);

        Route::delete('/android/{id}',[
           'as' => 'apps.deleteandroid',
            'uses'=>'AppController@destroy',
            'middleware'=>'auth'
        ]);
    });

    Route::get('/register/code',[
        'as'=>'user.adminCode',
        'uses'=>"UserController@getAllCodes",
        'middleware'=>'auth',
    ]);

    Route::get('/register/code/generate',[
        'as'=>'user.adminCodeGenerate',
        'uses'=>"RegisterCodeController@store",
        'middleware'=>'auth',
    ]);

    Route::post('/register/code/send',[
        'as'=>'user.sendCodeEmail',
        'uses' => 'RegisterCodeController@sendCodeEmail',
        'middleware' => 'auth'
    ]);

    Route::get('/new/{token}',[
        'as' => 'user.newWithToken',
        'uses' => 'RegisterCodeController@create',
    ]);

    Route::post('/new/{token}',[
        'as'=>'user.storeWithToken',
        'uses'=>'RegisterCodeController@storeWithToken'
    ]);

    Route::get('/new',[
        'as' => 'user.newNoToken',
        'uses' =>'RegisterCodeController@create'
    ]);

    Route::get('emails',[
        'as'=>'emails.index',
        'uses'=>'EmailController@index',
        'middleware'=>'auth',
    ]);

    Route::group(['prefix'=>"emails",'middleware'=>'auth'],function(){
        Route::get('/send',[
            'as'=>'emails.send',
            'uses'=>'EmailController@sendForm',
            'middleware'=>'auth',
        ]);

        Route::post('/send',[
            'as'=>'emails.postSend',
            'uses'=>"EmailController@sendEmail",
            'middleware'=>'auth',
        ]);

        Route::get("/status",[
            'as'=>'emails.status',
            'uses'=>"EmailController@status",
            'middleware'=>'auth',
        ]);
        Route::get('/{id}',[
            'as'=>'emails.show',
            'uses'=>"EmailController@show",
            'middleware'=>'auth',
        ]);
        Route::get('/create',[
            'as'=>'emails.create',
            'uses'=>'EmailController@create',
            'middleware'=>'auth',
        ]);

        Route::post('/create',[
            'as'=>'emails.createTemplate',
            'uses'=>'EmailController@store',
            'middleware'=>'auth',
        ]);

    });

    Route::get('/flappyguimo/ranking',[
        'as'=>'flappyguimo.ranking',
        'uses'=>'FlappyGuimoController@index',
        'middleware'=>'auth'
    ]);

    Route::get('/store',[
        'as'=>'admin.store.index',
        'uses'=>'StoreController@adminStoreIndex',
        'middleware'=>'auth',
    ]);

    Route::get('/store/categories',[
        'as'=>'admin.store.categories',
        'uses'=>'StoreController@adminStoreCategories',
        'middleware'=>'auth',
    ]);

    Route::get('/store/categories/{slug}',[
        'as'=>'admin.store.category',
        'uses'=>'StoreController@adminStoreCategory',
        'middleware'=>'auth',
    ]);

    Route::put('/stote/categories/{slug}',[
        'as'=>'admin.store.update_category',
        'uses'=>'StoreController@adminStoreUpdateCategory',
        'middleware'=>'auth',
    ]);

    Route::post('store/categories',[
        'as'=>'admin.store.new_category',
        'uses'=>'StoreController@adminStoreNewCategory',
        'middleware'=>'auth',
    ]);

    Route::get('/store/products',[
        'as'=>'admin.store.products',
        'uses'=>'StoreController@adminStoreProducts',
        'middleware'=>'auth',
    ]);

    Route::post('/store/products',[
        'as'=>'admin.store.new_product',
        'uses'=>'StoreController@adminStoreNewProduct',
        'middleware'=>'auth',
    ]);

    Route::get('/store/products/{slug}',[
        'as'=>'admin.store.product',
        'uses'=>'StoreController@adminStoreProduct',
        'middleware'=>'auth'
    ]);

    Route::put('/store/products/{slug}',[
        'as'=>'admin.store.update_product',
        'uses'=>'StoreController@adminStoreUpdateProduct',
        'middleware'=>'auth',
    ]);

});