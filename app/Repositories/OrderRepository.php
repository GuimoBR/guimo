<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository
 * @package namespace Guimo\Repositories;
 */
interface OrderRepository extends RepositoryInterface
{
    //
}
