<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository
 * @package namespace Guimo\Repositories;
 */
interface AppRepository extends RepositoryInterface
{
    //
}
