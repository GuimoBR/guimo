<?php

namespace Guimo\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Guimo\Repositories\AppRepository;
use Guimo\Entities\App;
use Guimo\Validators\AppValidator;

/**
 * Class AppRepositoryEloquent
 * @package namespace Guimo\Repositories;
 */
class AppRepositoryEloquent extends BaseRepository implements AppRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return App::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
