<?php

namespace Guimo\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Guimo\Repositories\UserEmailRepository;
use Guimo\Entities\UserEmail;

/**
 * Class UserEmailRepositoryEloquent
 * @package namespace Guimo\Repositories;
 */
class UserEmailsRepositoryEloquent extends BaseRepository implements UserEmailsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserEmail::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    /*public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }*/
}
