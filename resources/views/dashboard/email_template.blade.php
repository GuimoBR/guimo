{{csrf_field()}}
<div class="form-group floating-label @if($errors->has('name')) has-error @endif">
        <input type="text" class="form-control" name="name">
        <label for="name">Nome do email </label>
        <p class="help-block">Nome que este template de email terá.</p>
</div>
<div class="form-group floating-label @if($errors->has('from')) has-error @endif">

    <input type="email" class="form-control" name="from">
    <label for="from">De: <small>exemplo@exemplo.com</small></label>

</div>
<div class="form-group floating-label @if($errors->has('subject')) has-error @endif">
    <input type="text" class="form-control" name="subject">
    <label for="subject">Título: <small>Email de Boas Vindas</small></label>
</div>
<div class="form-group floating-label">
        <div class="radio radio-styled">
            <label>
                <input type="radio" class="flat" name="type" value="" checked="checked"> Texto puro
            </label>
        </div>
        <div class="radio radio-styled">
            <label>
                <input type="radio" class="flat" name="type" value="text/html" checked="checked"> Texto com HTML
            </label>
        </div>
    <label for="type">Tipo de Email</label>
</div>

<div class="form-group @if($errors->has('body')) has-error @endif">
    <label for="body" class="control-label">Mensagem <small> ou texto.</small></label>
    <textarea name="body" class=" form-control input-sm" id="body_email" cols="30" rows="10"></textarea>
</div>



