@extends('layouts.dashboard2')

@section('content')
    <section>
        <div class="section-body">
            <div class="row">

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-info no-margin">
                                <strong class="pull-right text-success text-lg"><i class="fa fa-users fa-2x"></i></strong>
                                <strong class="text-xl" id="count-emails">{{count($clients)}}</strong> <br/>
                                <span class="text-primary-dark text-lg"><a href="#">Clientes</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                <strong class="pull-right text-info text-lg"><i class="fa fa-dollar fa-2x"></i></strong>
                                <strong class="text-xl" id="count-emails">R$ 0,00</strong> <br/>
                                <span class="text-accent text-lg"><a href="#">Vendas</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-warning no-margin">
                                <strong class="pull-right text-accent text-lg"><i class="fa fa-list fa-2x"></i></strong>
                                <strong class="text-xl" id="count-emails">{{count($categories)}}</strong> <br/>
                                <span class="text-dark text-lg"><a href="{{route('admin.store.categories')}}">Categorias</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-danger no-margin">
                                <strong class="pull-right text-dark text-lg"><i class="fa fa-shopping-cart fa-2x"></i></strong>
                                <strong class="text-xl" id="count-emails">{{$products->total()}}</strong> <br/>
                                <span class="text-danger text-lg"><a href="{{route('admin.store.products')}}">Produtos</a></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection