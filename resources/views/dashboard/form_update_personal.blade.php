{{csrf_field()}}
{{method_field('PUT')}}
<div class="row">

        <div class="form-group">
            <div id="documentDiv">
                <label for="CPF" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">CPF:</label>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 floating-label">
                    <input type="text" name="CPF" id="CPF" class="form-control col-md-3" value="@if(!is_null($profile->info)){{$profile->info->CPF}}@else{{old('CPF')}}@endif" data-inputmask="'mask': '999.999.999-99'">
                    <span id="helpBlock2" class="help-block"></span>
                </div>
            </div>

                <div @if($errors->has('RG')) class="has-error" @endif>
                    @if($user->privileges >= 10)
                        <label for="RG" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">RG:</label>
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 floating-label" id="documentDiv">
                            <input type="text" name="RG" id="RG" class="form-control col-md-3" value="@if(!is_null($profile->info)){{$profile->info->RG}}@else{{old('RG')}}@endif" >
                            @if($errors->has('RG')) <span id="helpBlock2" class="help-block">{{$errors->first('RG')}}</span>@endif
                        </div>
                </div>

            <label for="passport" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">Passaporte:</label>
            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 floating-label" id="documentDiv">
                <input type="text" name="passport" id="passport" class="form-control col-md-3" value="@if(!is_null($profile->info)){{$profile->info->passport}}@endif" >
                <span id="helpBlock2" class="help-block"></span>
            </div>
            @endif

            <div @if($errors->has('birthdate')) class="has-error" @endif>
                <label for="birthdate" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">Data de Nascimento:</label>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="birthdate" id="birthdate" class="form-control" placeholder="Data de Nascimento" value="@if(!is_null($profile->info)){{$profile->info->birthdate}}@else{{old('birthdate')}}@endif" data-inputmask="'mask': '99/99/9999'">
                    @if($errors->has('birthdate')) <span id="helpBlock2" class="help-block">{{$errors->first('birthdate')}}</span>@endif
                </div>
            </div>
        </div>
</div>


<div class="row">
    <div class="form-group" >

        @if($user->privileges >= 10)
            <div @if($errors->has('birthcity')) class="has-error" @endif>
                <label for="birthcity" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">Cidade onde Nasceu?</label>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="birthcity" id="birthcity" class="form-control" value="@if(!is_null($profile->info)){{$profile->info->birthcity}}@else{{old('birthcity')}}@endif">
                    @if($errors->has('birthcity')) <span id="helpBlock2" class="help-block">{{$errors->first('birthcity')}}</span>@endif
                </div>
            </div>
        @endif
        <div @if($errors->has('CEP')) class="has-error" @endif>
            <label for="CEP" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">CEP:</label>
            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                <div class="input-group">
                    <input type="text" name="CEP" id="CEP" class="form-control" placeholder="CEP" data-inputmask="'mask': '99999-999'" value="@if(!is_null($profile->info)){{$profile->info->CEP}}@else{{old('CEP')}}@endif">
                    <span id="helpBlock2" class="help-block">Cidade onde você mora</span>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" id="searchCep"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
        </div>



        <label for="country_id" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">País:</label>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <select name="country_id" id="contry_id" class="form-control">
                @foreach($countries as $country)
                    <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div @if($errors->has('address')) class="has-error" @endif>
            <label for="address" class="control-label col-lg-1 col-md-2 col-sm-3 col-xs-12">Endereço</label>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="address" id="address" class="form-control col-md-3" placeholder="Endereço" value="@if(!is_null($profile->info)){{$profile->info->address}}@else{{old('address')}}@endif">
                @if($errors->has('address')) <span id="helpBlock2" class="help-block">{{$errors->first('address')}}</span>@endif
            </div>
        </div>
        <div @if($errors->has('neighborhood')) class="has-error" @endif>
            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="neighborhood" id="neighborhood" class="form-control col-md-3" placeholder="Bairro" value="@if(!is_null($profile->info)){{$profile->info->neighborhood}}@else{{old('neighborhood')}}@endif">
                @if($errors->has('neighborhood')) <span id="helpBlock2" class="help-block">{{$errors->first('neighborhood')}}</span>@endif
            </div>
        </div>
        <div @if($errors->has('number')) class="has-error" @endif>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <input type="text" name="number" id="number" class="form-control col-md-3" placeholder="Número" value="@if(!is_null($profile->info)){{$profile->info->number}}@else{{old('number')}}@endif">
                @if($errors->has('number')) <span id="helpBlock2" class="help-block">{{$errors->first('number')}}</span>@endif
            </div>
        </div>
        <div class="col-lg-1 col-md-4 col-sm-6 col-xs-12">
            <input type="text" name="adjunct" id="adjunct" class="form-control col-md-3" placeholder="Complemento" value="@if(!is_null($profile->info)){{$profile->info->adjunct}}@else{{old('adjunct')}}@endif">
        </div>
        <div @if($errors->has('city')) class="has-error" @endif>
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                <input type="text" name="city" id="city" class="form-control col-md-3" placeholder="Cidade" value="@if(!is_null($profile->info)){{$profile->info->city}}@else{{old('city')}}@endif">
                @if($errors->has('city')) <span id="helpBlock2" class="help-block">{{$errors->first('city')}}</span>@endif
            </div>
        </div>
    </div>
</div>
<hr>
<p> <span class="lead">Mini Bio</span> <small>(Coloque suas experiências relevantes ou oque você quiser)</small></p>
<div class="row">
    <div class="form-group">
        <div class="col-lg-12">
            <textarea name="bio" id="bio" class="form-control" rows="10" contenteditable="true">
                @if(!is_null($profile->info))
                    {{$profile->info->bio}}
                    @endif
            </textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-md-offset-5">
        <button id="submitPersonal" class="btn btn-success ink-reaction btn-lg" type="submit">Altualizar Dados Pessoais</button>
    </div>
</div>