@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Ranking Flappy Guimo</h2>
        </div>
        <div class="section-body">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Lista de Emails cadastrados na Landing Page</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <a class="btn btn-default-dark btn-flat btn-block ink-reaction" href="#" id="resetRanking"><i class="fa fa-refresh fa-spin"></i> &nbsp; Resetar Ranking</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body style-default-bright">
                            <table class="table table-stripped table order-column table-striped hover" id="ranking">
                                <thead>
                                <tr class="headings">

                                    <th class="text-center">Posição</th>
                                    <th class="text-center">Jogador</th>
                                    <th class="text-center">Pontuação</th>
                                    <th class="text-center">Data</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0 ?>
                                @foreach($players as $player)
                                    <?php $i++;?>
                                    <tr class="even pointer text-center ">
                                        <td>{{$i}}</td>
                                        <td>{{$player->playerName}}</td>
                                        <td>{{$player->points}}</td>
                                        <td>{{$player->created_at->format('d/m/Y H:i:s')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('script')
            <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            toastr.options = {
                "closeButton":true,
                "progressBar":true,
                "positionClass":'toast-top-right',
                "showMethod":'fadeIn',
                "hideMethod":'fadeOut'
            };

            $("#resetRanking").click(function(){
                $.ajax({
                    url:'{{url('/')}}/api_v1/flappyguimo/resetRanking',
                    method:'PUT'
                }).success(function(response){
                    if(response.success) {
                        toastr.success(response.message,'Resetar Ranking');
                    }else{

                        toastr.error(response.message,'Resetar Ranking');
                    }
                })
            });

            var oTable = $('#ranking').dataTable({
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json"
                },
                'iDisplayLength': 10,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip'
            });
        })
    </script>
@endsection