<?php

use Illuminate\Database\Seeder;

class ShoppingCartProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Guimo\Entities\ShoppingCartProducts::class,8)->create();
    }
}
