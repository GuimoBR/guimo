<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 22:34
 */

namespace Guimo\Services;


use Guimo\Entities\CategoriesProducts;
use Guimo\Repositories\CategoriesProductsRepository;
use Guimo\Repositories\ProductImageRepository;
use Guimo\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\Support\Facades\Validator;

class ProductService
{

    /**
     * @var ProductRepository
     */
    private $repository;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var CategoriesProductsRepository
     */
    private $categoriesProductsRepository;
    /**
     * @var ProductImageRepository
     */
    private $productImageRepository;

    private $rules;
    private $messages;
    private $customAttributes;



    public function __construct(ProductRepository $repository, ProductImageRepository $productImageRepository, CategoriesProductsRepository $categoriesProductsRepository, Filesystem $filesystem,Storage $storage)
    {
        $this->repository = $repository;
        $this->filesystem = $filesystem;
        $this->storage = $storage;
        $this->categoriesProductsRepository = $categoriesProductsRepository;
        $this->productImageRepository = $productImageRepository;

        $this->rules =[
            'name' =>  'required|max:60',
            'price'=>'required|numeric',
            'discount_percent'=>'required|numeric',
            'is_avaible'=>'required',
            'stock'=>'required|numeric',
            'thumb_image'=>'required|image',
            'categories'=>'required|array',
            'description'=>'required',
            'slug' => 'required|max:50',
            'images.*' => 'image|mimes:jpg,jpeg'
        ];

        $this->messages = [
            'required'  => 'O campo :attribute é obrigatório.',
            'max'       => 'O campo :attribute deve ter no máximo :max caractéres.',
            'numeric'   => 'O campo :attribure deve ser um número.',
            'image'     => 'O campo :attribute deve ser um arquivo de imagem!',
            'array'     => 'Você deve selectionar pelo menos uma opção',
            'mimes'     => 'As imagens devem ser nos formatos :values',
        ];
        $this->customAttributes = [
            'name'=>"Nome",
            'price' => 'Preço',
            'discount_percent'=>'Desconto',
            'is_avaible'=>'Disponivel',
            'stock' => 'Estoque',
            'thumb_image'=>'Imagem Destaque',
            'categories'=>'Categoria do Produto',
            'description'=>'Descrição',
            'slug'=>"URL do Produto",
            'images'=>"Imagens",


        ];
    }

    public function getAllProducts()
    {
        return $this->repository->with(['categories'])->paginate(8);
    }

    public function getProducts(){
        return $this->repository->with(['categories'])->all();
    }

    public function getProductFromSlug($pslug){
        return $this->repository->with(['categories','comments'])->findWhere(['slug'=>$pslug])->first();
    }

    public function createNewProduct(Request $request){
        $data = array();
        $data['name']  = $request->input('name');
        $data['price'] = $request->input('price');
        $data['discount_percent'] = $request->input('discount_percent');
        $data['is_avaible'] = $request->input('is_avaible');
        $data['stock'] = $request->input('stock');
        $data['thumb_image'] = $request->file('thumb_image');
        $data['categories'] = $request->input('categories');
        $data['description'] = $request->input('description');
        $data['slug'] = $request->input('slug');
        $data['images'] = $request->file('images');

        if(is_null($data['images'][0])){
            unset($this->rules['images']);
        }

        $validator = Validator::make($data,$this->rules,$this->messages,$this->customAttributes);
        if($validator->fails()){
            return $validator;
        }else{

            $images = $data['images'];
            $extension = $request->file('thumb_image')->getClientOriginalExtension();

            $data['thumb_image'] = 'thumb_image.' . $extension;
            $product = $this->repository->create($data);

            $product->discount_percent = $data['discount_percent'];
            $product->save();

            foreach ($request->input('categories') as $category) {
                $this->categoriesProductsRepository->create(['category_id' => $category, 'product_id' => $product->id]);
            }
            $this->storage->put('store/' . $product->slug . '/' . $product->thumb_image, $this->filesystem->get($request->file('thumb_image')));

            if(!is_null($images[0]) && count($images < 5)){

                $imagesSaved = $this->productImageRepository->findWhere(['product_id'=>$product->id]);

                if(count($imagesSaved) > 0){
                    foreach($imagesSaved as $img){
                        $this->storage->delete('store/'.$product->slug.'/'.$img->file_name);
                        $this->productImageRepository->delete($img->id);

                    }
                }

                $i = 0;
                foreach($images as $image){
                    $extension = $image->getClientOriginalExtension();
                    $file_name = "$i.$extension";

                    $this->storage->put('store/'.$product->slug.'/'.$file_name,$this->filesystem->get($image));
                    $pimg = ['product_id'=>$product->id,'file_name'=>$file_name];
                    $this->productImageRepository->create($pimg);
                    $i++;
                }

            }
            return true;



        }

    }

    public function updateProduct($slug,$data){
        $product = $this->getProductFromSlug($slug);
        unset($data['_token']);
        unset($this->rules['thumb_image']);
        $validator = Validator::make($data,$this->rules,$this->messages,$this->customAttributes);
        if($validator->fails()){
            return $validator;
        }else{
            $images = $data['images'];
            $updatedProduct = $this->repository->update($data,$product->id);
            $updatedProduct->discount_percent = $data['discount_percent'];
            $updatedProduct->save();
            foreach($product->categories as $pc){

                CategoriesProducts::where('category_id',$pc->id)->where('product_id',$product->id)->delete();
            }

            foreach($data['categories'] as $category){
                $this->categoriesProductsRepository->create(['category_id'=>$category,'product_id'=>$product->id]);
            }

            if(!is_null($images[0]) && count($images < 5)){

                $imagesSaved = $this->productImageRepository->findWhere(['product_id'=>$updatedProduct->id]);

                if(count($imagesSaved) > 0){
                    foreach($imagesSaved as $img){
                        $this->storage->delete('store/'.$updatedProduct->slug.'/'.$img->file_name);
                        $this->productImageRepository->delete($img->id);

                    }
                }

                $i = 0;
                foreach($images as $image){
                    $extension = $image->getClientOriginalExtension();
                    $file_name = "$i.$extension";

                    $this->storage->put('store/'.$updatedProduct->slug.'/'.$file_name,$this->filesystem->get($image));
                    $pimg = ['product_id'=>$updatedProduct->id,'file_name'=>$file_name];
                    $this->productImageRepository->create($pimg);
                    $i++;
                }

            }

            return true;
        }
    }

}