<?php

namespace Guimo\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Guimo\Repositories\ShoppingCartRepository;
use Guimo\Entities\ShoppingCart;

/**
 * Class ShoppingCartRepositoryEloquent
 * @package namespace Guimo\Repositories;
 */
class ShoppingCartRepositoryEloquent extends BaseRepository implements ShoppingCartRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ShoppingCart::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
