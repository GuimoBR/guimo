<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 14/03/2016
 * Time: 19:02
 */

return [
    'splash1'   => 'Aguarde enquando carregamos',
    'splash2'   => 'Você vai se surpreender...',
    'splash3'   => '...com o que o guimo pode fazer!',
    'home'      => 'Home',
    'aboutus'   => 'O Guimo',
    'team'      => 'Time',
    'services'  => 'Serviços',
    'portfolio' => 'Portifólio',
    'testimonial' => 'Depoimentos',
    'blog'      => 'Blog',
    'contact'   => 'Contato',
    'guimowel'     => 'Guimo - Diversão e Educação de outro mundo!',
    'welcome'   => "Bem vindo ao <span class=\"id-color\">Guimo</span>",
    'slider1'   => "<span class=\"id-color\">Educativo</span> + Divertido",
    'slider2'   => "Brinquedo + <span class=\"id-color\">Tecnologia</span>",
    'hiimguimo'    => "Oi, eu sou o <span class=\"id-color\">Guimo!</span>",
    'helloworld' => "Olá <span class=\"id-color\" >mundo!</span>",
    'guimohistory' => 'Oi, me chamo guimo, sou um robo viajante do planeta Guimundo. Enquanto explorávamos o sistema solar,
    algo muito estranho aconteceu: um raio muito poderoso nos atingiu e caimos em um planeta não identificado em
    nossos Arquivos de Planetas. Eu fiquei perdido, mas você me encontrou! Preciso muito de sua ajuda para buscar meus
    outros amigos Guimo e descobrir o mistério de quem derrubou nossa nave',
    'guimohead' => "No Guimundo podemos trocar nossas peças e ganhar novas habilidades.
     Para isso preciso da sua ajuda para recuperar minhas peças e montando como quiser!",
    'guimobody' => "Mas preciso de sua ajuda para lembrar como executar as minhas funções. Basta me conectar ao seu Smartphone, montar minhas peças e organizar meus blocos de programação. ",
    'whatmore'  => "O que eu posso <span class=\"id-color\">fazer?</span>",
    'app'       => "Aplicativo",
    "apptxt"    => "Sempre com novas atualizações, o aplicativo permite que você tenha seu Guimo sempre a mão.",
    'games'     => "Jogos",
    "gamestxt"  => "Dentro e fora do aplicativo você pode se divertir com vários jogos usando o Guimo, seu smartphone, ou os dois juntos.",
    'edu'       => 'Educativo',
    'edutxt'    => 'Mais do que apenas divertir, no meu app você pode aprender sobre programação de uma forma descontraída e divertida.',
    'modular'   => 'Modular',
    'modulartxt' => 'Algumas peças do meu corpo podem ser trocadas. Assim, você pode montar seu guimo com formas e funcionalidades diferentes.',
    'bluetooth' => 'Sem Fios',
    'bluetoothtxt' => 'Com o bluetooh 4.0, eu gasto pouca energia e a diversão pode ir ainda mais longe.',
    'custom'    => "Customizável",
    'customtxt' => 'Além de trocar minhas peças com qualquer outro Guimo, você pode utilizar uma impressora 3D para inventar novas peças.',
    'partners'  => "Parceiros",
    'secteam'      => "Conheça meus amigos <span class='id-color'>da Terra</span>",
    'submit'    => 'Enviar',
    'submiting' => "Enviando... <i class='fa fa-spinner fa-spin'></i>",
    "contactus" => "<span class='id-color'>Ficou interessado?</span> Deixe seu email conosco!",
    "youremail" => "Seu Email",
    'wrongEmail' => 'O email informado não está no formato correto, por favor, tente novamente!',
    'emptyEmail' => 'O email não pode ficar em branco...',
    'successEmail' => "Email cadastrado com sucesso!",
];