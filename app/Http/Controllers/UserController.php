<?php

namespace Guimo\Http\Controllers;

use Carbon\Carbon;
use Guimo\Repositories\CountryRepository;
use Guimo\Services\RegisterCodeService;
use Guimo\Services\UserService;
use Illuminate\Http\Request;

use Guimo\Http\Requests;
use Guimo\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{


    /**
     * @var CountryRepository
     */
    private $countryRepository;

    /**
     * @var RegisterCodeService
     */
    private $registerCodeService;
    /**
     * @var User;
     */
    private $user;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(CountryRepository $countryRepository, UserService $userService,RegisterCodeService $registerCodeService)
    {
        $this->countryRepository = $countryRepository;
        $this->registerCodeService = $registerCodeService;
        $this->user = Auth::user();
        if(!is_null($this->user->info)) {
            $this->user->info->birthdate = Carbon::parse($this->user->info->birthdate)->format('d/m/Y');
        }

        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($token = null)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $countries = $this->countryRepository->findWhere(['is_avaible'=>true]);
        $profile = $this->userService->getFromId($id);
        if(!is_null($profile->info)){
            $profile->info->birthdate = Carbon::parse($profile->info->birthdate)->format('d/m/Y');
            if(!is_null($profile->info->uploads)){
                $profile->info->uploads = explode(',',$profile->info->uploads);
            }
        }
        return view('dashboard.profile',['user'=>$this->user,'profile'=>$profile,'profileid'=>$id,'countries'=>$countries]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function getAllCodes()
    {
        $codes = $this->registerCodeService->all();
        foreach ($codes as $code) {
            $code['created_at'] = Carbon::parse($code['created_at']);
            if (!is_null($code['accessed_at'])) {
                $code['accessed_at'] = Carbon::parse($code['accessed_at']);
            }
        }
        return view('dashboard.user_registercodes', ['user' => $this->user, 'codes' => $codes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGeneral(Request $request, $id)
    {
        //
        $data = $request->all();

        $updateUser = $this->userService->update($data,$id);
        if(is_bool($updateUser)){
            return redirect()->route('profile',['id'=>$id])->with('message','Dados gerais atualizados com sucesso!');
        }else{
            return redirect()->route('profile',['id'=>$id])->withErrors($updateUser);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePersonal(Request $request, $id)
    {
        //
        $data = $request->all();

        $updateUser = $this->userService->update($data,$id,'personal');

        if(is_bool($updateUser)){
            return redirect()->route('profile',['id'=>$id])->with('message','Dados pessoais atualizados com sucesso!');
        }else{
            return redirect()->route('profile',['id'=>$id])->withErrors($updateUser)->withInput($request->all());
        }
    }

    public function uploadFiles(Request $request, $id){
        $uploadUser = $this->userService->uploadFiles($request,$id);
        if(is_bool($uploadUser)){
            return redirect()->route('profile',['id'=>$id])->with('message','Uploads efetuados com sucesso');
        }else{
            return redirect()->route('profile',['id'=>$id])->withErrors($uploadUser['message']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
