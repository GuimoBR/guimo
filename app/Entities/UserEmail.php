<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserEmail extends Model implements Transformable
{
    use TransformableTrait;

    protected  $table = 'user_emails';

    protected $fillable = [
        'email',
        'status'
    ];

}
