@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Lista de Códigos Gerados</h2>
        </div>
        <div class="section-body">
            @if($errors->has())
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="alert alert-callout alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p><strong>Atenção, alguns errros foram detectados:</strong></p>
                            <ul class="list-unestyled">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            @if(Session::has('message'))
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="alert alert-callout alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p>{{Session::get('message')}}</p>
                        </div>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Lista de Códigos para cadastro gerados</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <a class="btn btn-default-dark btn-flat btn-block ink-reaction" href="{{route('user.adminCodeGenerate')}}">Gerar Código <i class="fa fa-plus-square-o"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body style-default-bright">
                            <table id="emails" class="table order-column table-striped hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Situação</th>
                                    <th>Data de Cadastro</th>
                                    <th>Data de Acesso</th>
                                    <th>Link</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($codes as $code)
                                    <tr class="gradeA">
                                        <td>
                                            {{$code->id}}
                                        </td>
                                        <td>
                                            {{$code->token}}
                                        </td>
                                        <td class="@if($code->is_accessible) text-success @else text-warning @endif">
                                            @if($code->is_accessible) Ainda acessível @else Já acessado @endif
                                        </td>
                                        <td>
                                            {{$code->created_at->format('d/m/Y H:i:s')}}
                                        </td>
                                        <td>
                                            @if(is_null($code->accessed_at))
                                                Não acessado
                                            @else
                                                {{$code->accessed_at->format('d/m/Y H:i:s')}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($code->is_accessible)
                                            <a data-token="{{$code->token}}" class="text-primary" href="#" id="showLink" class="btn btn-flat">Enviar link <i class="fa fa-arrow-right"></i> </a>
                                                @else
                                                <a href="#" class="text-danger">Indisponível</a>
                                                @endif
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- BEGIN LARGE TEXT MODAL MARKUP -->
    <div class="modal fade" id="textModal" tabindex="-1" role="dialog" aria-labelledby="textModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="textModalLabel">License terms</h4>
                </div>
                <form action="{{route('user.sendCodeEmail')}}" method="post">
                    {{csrf_field()}}
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <p class="lead">Link para cadastro: </p>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" name="linkurl" id="linkurl" readonly class="form-control">
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-info btn-flat copy" type="button" data-clipboard-target="#linkurl" data-toggle="tooltip" data-placement="top" title="" data-original-title="Copiar Link"><i class="fa fa-copy"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="col-md-12">
                            <p class="lead"><small class="text-xs">ou</small> Envie por email:</p>
                        </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Nome:</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Email Destino:</label>
                                    <input type="text" name="to" class="form-control">
                                </div>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
                    <button type="submit" class="btn btn-success">Enviar <i class="md md-send"></i> </button>
                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- END LARGE TEXT MODAL MARKUP -->



@endsection

@section('script')
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>
    <script>
        $(document).ready(function() {
            new Clipboard('.copy');

            var oTable = $('#emails').DataTable({
                "dom": 'T<"clear">lfrtip',
                "order": [[0, 'asc']],
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });

            $("a#showLink").click(function(){
                $("#linkurl").val('{{url('/admin/new/')}}'+'/'+$(this).data('token'));
                $('.modal').modal('show');
            });
        })
    </script>
@endsection