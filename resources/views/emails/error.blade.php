
<body>
<div>
    <img src="http://guimo.toys/v2/images/logo2.png" alt="Guimo" style="width: 200px;float:left">
    <h1 style="text-align: center;color:#9c3328">Um erro ocorreu na aplicação {{env('APP_NAME')}}</h1>
    <br><br><br>
    @if($code > 0)
    <p style="font-size: 2em; color:#333"><strong>Código: </strong> {{$code}}</p>
    @endif
    <p style="font-size:1.5em;color:#0a6ebd">Acesso: {{$url}} em {{date('d/m/Y H:i:s')}}</p>

    <p style="font-size:1.5em; color:#333"><strong>Mensagem: </strong> {{$errmsg}} na linha ({{$line}}) do arquivo ({{$errfile}})</p>

    <p style="font-size:1.1em;color:#333"><strong>Strack Trace</strong><br>
        {!!  nl2br($errTrace) !!}</p>
</div>
</body>