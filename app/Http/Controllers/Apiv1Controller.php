<?php

namespace Guimo\Http\Controllers;

use Guimo\Repositories\EmailRepository;
use Guimo\Repositories\FlappyGuimoRepository;
use Guimo\Repositories\UserEmailsRepository;
use Guimo\Repositories\UserRepository;
use Guimo\Services\EmailService;
use Guimo\Services\FlappyGuimoService;
use Guimo\Services\UserEmailsService;
use Guimo\Services\UserService;
use Illuminate\Http\Request;

use Guimo\Http\Requests;
use Guimo\Http\Controllers\Controller;
use Guimo\Email;
use Guimo\User;
use Guimo\FlappyGuimo;
use Auth;
use Cagartner\CorreiosConsulta\Facade as Correios;


class Apiv1Controller extends Controller
{

    /**
     * @var EmailRepository
     */
    private $emailRepository;
    /**
     * @var EmailService
     */
    private $emailService;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var FlappyGuimoRepository
     */
    private $flappyGuimoRepository;
    /**
     * @var FlappyGuimoService
     */
    private $flappyGuimoService;
    /**
     * @var UserEmailsRepository
     */
    private $userEmailsRepository;
    /**
     * @var UserEmailsService
     */
    private $userEmailsService;

    /**
     * Apiv1Controller constructor.
     * @param EmailRepository $emailRepository
     * @param EmailService $emailService
     */
    public function __construct(EmailRepository $emailRepository,
                                EmailService $emailService,
                                UserEmailsRepository $userEmailsRepository,
                                UserEmailsService $userEmailsService,
                                UserRepository $userRepository,
                                UserService $userService,
                                FlappyGuimoRepository $flappyGuimoRepository,
                                FlappyGuimoService $flappyGuimoService)
    {
        $this->middleware('cors');
        $this->emailRepository = $emailRepository;
        $this->emailService = $emailService;
        $this->userRepository = $userRepository;
        $this->userService = $userService;
        $this->flappyGuimoRepository = $flappyGuimoRepository;
        $this->flappyGuimoService = $flappyGuimoService;
        $this->userEmailsRepository = $userEmailsRepository;
        $this->userEmailsService = $userEmailsService;
    }


    public function getEmailsCreated(){
        return $this->emailRepository->all();
    }
    public function getEmails()
    {
        return $this->userEmailsService->findWhereIfAuthenticated(['status'=>1]);
    }


    /**
     * Save email from landingpage
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function saveEmail(Request $request){
       return $this->userEmailsService->create($request->all());
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function updateUserInfo(Request $request, $id){
       return $this->userService->update($request->all(),$id);
    }


    /**
     * Get flappyGuimo ranking points
     * @return mixed
     */
    public function flappyGuimoGetPoints(){
        return $this->flappyGuimoService->getRanking();
    }


    /**
     * Send points to ranking
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function flappyGuimoSendPoints(Request $request){
        return $this->flappyGuimoService->create($request->all());
    }

    public function flappyGuimoResetRanking(){
        return $this->flappyGuimoService->resetRanking();
    }


    public function updateStatus(Request $request){
        return $this->userEmailsService->updateStatus($request->all());
    }

    public function validateCPF(Request $request){

        $cpf = $request->input('cpf');
        // Verifica se um número foi informado
        if(empty($cpf)) {
            return ['is_valid'=>false];
        }

        $cpf = preg_replace( '#[^0-9]#', '', $cpf);

        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return ['is_valid'=>false];
        }

        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            return ['is_valid'=>false];
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return ['is_valid'=>false];
                }
            }
            return ['is_valid'=>true];
        }
    }

    public function correiosConsultaCep(Request $request, $cep){
        //$cep = str_replace('-','',$cep);
        return Correios::cep($cep);
    }

    public function correiosConsultaPreco(Request $request,$cep){
        $pac = ['tipo'=>'pac','formato'=>'caixa','cep_destino' => $cep,'cep_origem'=>'37500-174','peso'=>1,'comprimento'=>16,'altura'=>11,'largura'=>11,'diametro'=>0];
        $sedex = ['tipo'=>'sedex','formato'=>'caixa','cep_destino' => $cep,'cep_origem'=>'37500-174','peso'=>1,'comprimento'=>16,'altura'=>11,'largura'=>11,'diametro'=>0];
        $frete = [
            'pac'=>Correios::frete($pac),
            'sedex'=>Correios::frete($sedex),
        ];

        return $frete;
    }
}
