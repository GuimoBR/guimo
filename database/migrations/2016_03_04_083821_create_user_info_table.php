<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_info',function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('profile_image',60)->nullable();
            $table->date('birthdate')->nullable();
            $table->string('birthcity',45)->nullable();
            $table->string('passport',40)->nullable();
            $table->string('RG',20)->nullable();
            $table->string('CPF',20)->nullable();
            $table->string('CEP',15)->nullable();
            $table->string('address',255)->nullable();
            $table->integer('number')->nullable();
            $table->string('adjunct',15)->nullable();
            $table->string('neighborhood',45)->nullable();
            $table->string('city',45)->nullable();
            $table->text('bio')->nullable();
            $table->text('uploads')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users_info');
    }
}
