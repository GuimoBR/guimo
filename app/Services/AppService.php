<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 25/04/2016
 * Time: 19:25
 */

namespace Guimo\Services;


use Guimo\Repositories\AppRepository;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\Support\Facades\Validator;

class AppService
{

    private $repository;
    private $rules;
    private $messages;
    private $customAttributes;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Storage
     */
    private $storage;


    public function __construct(AppRepository $repository,Filesystem $filesystem,Storage $storage)
    {
        $this->repository = $repository;
        $this->rules =[
            'name' =>  'required',
            'filename' =>'required',
            'version'   => 'required',
            'changelog'=> 'required',
            'uploadfile'      => 'required',
            'target_os'   => 'required',
        ];

        $this->messages = [
            'required'  => 'O campo :attribute é obrigatório',
        ];
        $this->customAttributes = [
            'name'      => "Nome",
            'filename'  => "Nome do Arquivo",
            'version'   => 'Versão',
            'target_os' => 'Sistema Operacional',
            'changelog' => 'Descrição detalhada',
            'uploadfile'      => 'Arquivo',
        ];
        $this->filesystem = $filesystem;
        $this->storage = $storage;
    }


    public function getByOS($so){
        return $this->repository->findWhere(['target_os'=>$so]);
    }

    public function uploadApp(Request $request){
        $data = $request->all();
        $validator = Validator::make($data,$this->rules,$this->messages,$this->customAttributes);
        if($validator->fails()){
            return ['error'=>true,'message'=>"Não foi possivel fazer o upload. Por favor, verifique se todos os campos foram preenchidos!"];
        }else{

            if($this->storage->put('apps/'.$data['version']."/".$data['filename'],$this->filesystem->get($request->file('uploadfile')))){
                unset($data['_token']);
                unset($data['file']);
                $this->repository->create($data);
                return ['error'=>false,'message'=>"Upload realizado com sucesso!"];
            }else{
                return ['error'=>true,'message'=>"Não foi possivel fazer upload dos arquivos!"];
            }

        }
    }

    public function delete($id){
        $app = $this->repository->find($id);
        if($this->storage->delete('/apps/'.$app->version.'/'.$app->filename)){
            $this->repository->delete($id);
            return ['error'=>false,'message'=>"App $app->name removido com sucesso!"];
        }else{
            return ['error'=>true,'message'=>"Erro ao remover app $app->name. Por favor,tente novamente"];
        }
    }

}