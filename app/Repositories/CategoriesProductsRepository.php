<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoriesProductsRepository
 * @package namespace Guimo\Repositories;
 */
interface CategoriesProductsRepository extends RepositoryInterface
{
    //
}
