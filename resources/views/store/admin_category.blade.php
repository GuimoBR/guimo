@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/TableTools.css')}}" rel="stylesheet" type="text/css" />
    @endsection
@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Lista de todas as categorias cadastradas</h2>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @if($errors->has())

                        <div class="alert alert-callout alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p><strong>Atenção, alguns erros foram detectados:</strong></p>
                            <ul class="list-unestyled">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-callout alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p>{{Session::get('message')}}</p>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>{{$category->name}}</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#edit_category"><i class="fa fa-edit"></i> Atualizar Categoria</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body style-default-bright">
                            <div class="col-md-12">
                                <h3 class="no-margin">Produtos nesta Categoria: {{count($category->products)}} <span class="pull-right">Adicionada em: {{$category->created_at->format('d/m/Y')}}</span></h3>
                                <br>
                                <p><strong>Descrição:</strong> <br>
                                    {!! $category->description !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-dark">
                        <div class="card-head">
                            <header>Produtos de {{$category->name}}</header>

                        </div>
                        <div class="card-body style-default-bright">
                            <p class="text-danger">Atenção, alguns produtos podem estar cadastrados em <strong>mais de uma categoria</strong>.</p>
                            <table class="table table-stripped table order-column table-striped hover" id="categories" data-swftools="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf')}}" id="categories">
                                <thead>
                                <tr class="headings">

                                    <th class="text-center">#</th>
                                    <th class="text-center">Nome</th>
                                    <th class="text-center">Descrição</th>
                                    <th class="text-center">Categorias</th>
                                    <th class="text-center">Preço (R$)</th>
                                    <th class="text-center">Desconto</th>
                                    <th class="text-center">Estoque</th>
                                    <th class="text-center">Disponivel?</th>
                                    <th class="text-center">Adicionado Em</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($category->products as $product)
                                    <tr class="text-center">
                                        <td>{{$product->id}}</td>
                                        <td><a href="{{route('admin.store.product',['slug'=>$product->slug])}}">{{$product->name}} <i class="fa fa-edit"></i> </a></td>
                                        <td style="width: 30%">{!! substr(nl2br($product->description),0,70) !!}...</td>
                                        <td style="width: 10%">
                                            @foreach($product->categories as $ct)
                                                <span class="label label-primary">{{$ct->name}}</span>
                                            @endforeach
                                        </td>
                                        <td>{{$product->price}}</td>
                                        <td>{{$product->discount_percent}}%</td>
                                        <td>{{$product->stock}}</td>
                                        @if($product->is_avaible)
                                            <td>Sim</td>
                                        @else
                                            <td>Não</td>
                                        @endif
                                        <td>{{$product->created_at->format('d/m/Y H:i:s')}}</td>
                                    </tr>

                                @empty
                                    <tr class="text-center"><td colspan="9"><strong class="text-warning">Nenhum Produto nesta Categoria</strong></td></tr>
                                @endforelse
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="edit_category">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">Atualizar Categoria</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.store.update_category',['slug'=>$category->slug])}}" method="POST" id="create_category">
                                {{method_field('PUT')}}
                                @include('store.form_new_category',['category'=>$category])
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary" id="submit">Atualizar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{asset('assets/js/string_to_slug.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('materialadmin/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script>
        $(document).ready(function(){

            $("#submit").click(function(){
                $("#create_category").submit();
            });

            $("#slug").val(url_slug($("#name").val()));

            $("#name").keyup(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            $("#name").change(function(){
                $("#slug").val(url_slug($(this).val()));
            });

            @if(count($category->products)>0)
            var oTable = $('#categories').dataTable({
                "dom": 'T<"clear">lfrtip',
                "colVis": {
                    "buttonText": "Columns",
                    "overlayFade": 0,
                    "align": "right"
                },
                "tableTools": {
                    "sSwfPath": $('#categories').data('swftools')
                },
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                    "lengthMenu": '_MENU_ entries per page',
                    "search": '<i class="fa fa-search"></i>',
                    "paginate": {
                        "previous": '<i class="fa fa-angle-left"></i>',
                        "next": '<i class="fa fa-angle-right"></i>'
                    }
                }
            });
            @endif

        });
    </script>
@endsection