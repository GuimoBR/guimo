<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Guimo | Dashboard </title>

    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" type='image/x-icon' href="{{asset('assets/images/ico/favicon.ico')}}">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>

    <link href="{{asset('materialadmin/assets/css/theme-1/bootstrap.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('materialadmin/assets/css/theme-1/materialadmin.css')}}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset('materialadmin/assets/css/theme-1/material-design-iconic-font.min.css')}}">
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/toastr/toastr.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/typeahead/typeahead.css')}}" rel="stylesheet" type="text/css" />
    @yield('style')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="menubar-hoverable header-fixed menubar-pin menubar-first">
<!--BEGIN HEADER -->
<header id="header" class="">
    <div class="headerbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="headerbar-left">


            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand" >
                    <div class="brand-holder">
                        <a href="{{route('dashboard')}}">
                            <span class="text-lg text-bold text-primary"><img src="{{asset('assets/images/logo_guimo_100px.png')}}" alt="Guimo"> GUIMO</span>
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                {{--@if(App::environment('development'))
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li>
                            <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                {{$localeCode }}
                            </a>
                        </li>
                    @endforeach
                @endif--}}
                @if(App::environment('development'))
                <li class="dropdown">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                        <img class="pull-right img-circle" src="{{asset('v2/images/'.LaravelLocalization::getCurrentLocale().'.png')}}" alt="{{LaravelLocalization::getCurrentLocale()}}" style="width: 36px; height: 26px;"/>
                    </a>
                    <ul class="dropdown-menu animation-expand">
                        <li class="dropdown-header">Idiomas</li>
                        <li>
                            <a class="alert alert-callout alert-success" hreflang="{{LaravelLocalization::getCurrentLocale()}}" href="{{LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale())}}">
                                <img class="pull-right img-circle" src="{{asset('v2/images/'.LaravelLocalization::getCurrentLocale().'.png')}}" alt="{{LaravelLocalization::getCurrentLocale()}}" style="width: 36px; height: 26px;" />
                                <strong>{{ LaravelLocalization::getCurrentLocale() }}</strong>
                            </a>
                        </li>
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            @if($localeCode != LaravelLocalization::getCurrentLocale())
                                <li>
                                    <a class="alert alert-callout alert-info" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode)}}">
                                        <img class="pull-right img-circle" src="{{asset('v2/images/'.$localeCode.'.png')}}" alt="{{$localeCode}}" style="width: 36px; height: 26px;"/>
                                        <strong>{{ $localeCode }}</strong>
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul><!--end .dropdown-menu -->
                </li>
                    @endif
            </ul>

        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="headerbar-right">
            <ul class="header-nav header-nav-options">
                @if($user->privileges >= 10)
                <li>
                    <!-- Search form -->
                    <form class="navbar-search" role="search" action="{{secure_url(route('admin.search'))}}">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Busca de Admins.">
                        </div>
                        <button type="submit" class="btn btn-icon-toggle ink-reaction" ><i class="fa fa-search"></i></button>
                    </form>
                </li>
                @endif
                {{--<li class="dropdown hidden-xs">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                        <i class="fa fa-bell"></i><sup class="badge style-danger">4</sup>
                    </a>
                    <ul class="dropdown-menu animation-expand">
                        <li class="dropdown-header">Today's messages</li>
                        <li>
                            <a class="alert alert-callout alert-warning" href="javascript:void(0);">
                                <img class="pull-right img-circle dropdown-avatar" src="#" alt="" />
                                <strong>Alex Anistor</strong><br/>
                                <small>Testing functionality...</small>
                            </a>
                        </li>
                        <li>
                            <a class="alert alert-callout alert-info" href="javascript:void(0);">
                                <img class="pull-right img-circle dropdown-avatar" src="#" alt="" />
                                <strong>Alicia Adell</strong><br/>
                                <small>Reviewing last changes...</small>
                            </a>
                        </li>
                        <li class="dropdown-header">Options</li>
                        <li><a href="#">View all messages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                        <li><a href="#">Mark as read <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
                <li class="dropdown hidden-xs">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                        <i class="fa fa-area-chart"></i>
                    </a>
                    <ul class="dropdown-menu animation-expand">
                        <li class="dropdown-header">Server load</li>
                        <li class="dropdown-progress">
                            <a href="javascript:void(0);">
                                <div class="dropdown-label">
                                    <span class="text-light">Server load <strong>Today</strong></span>
                                    <strong class="pull-right">93%</strong>
                                </div>
                                <div class="progress"><div class="progress-bar progress-bar-danger" style="width: 93%"></div></div>
                            </a>
                        </li><!--end .dropdown-progress -->
                        <li class="dropdown-progress">
                            <a href="javascript:void(0);">
                                <div class="dropdown-label">
                                    <span class="text-light">Server load <strong>Yesterday</strong></span>
                                    <strong class="pull-right">30%</strong>
                                </div>
                                <div class="progress"><div class="progress-bar progress-bar-success" style="width: 30%"></div></div>
                            </a>
                        </li><!--end .dropdown-progress -->
                        <li class="dropdown-progress">
                            <a href="javascript:void(0);">
                                <div class="dropdown-label">
                                    <span class="text-light">Server load <strong>Lastweek</strong></span>
                                    <strong class="pull-right">74%</strong>
                                </div>
                                <div class="progress"><div class="progress-bar progress-bar-warning" style="width: 74%"></div></div>
                            </a>
                        </li><!--end .dropdown-progress -->
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->--}}
            </ul><!--end .header-nav-options -->
            <ul class="header-nav header-nav-profile">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                        @if(!is_null($user->info->profile_image))
                        <img src="{{url('/')}}/files/{{$user->id}}/{{$user->info->profile_image}}"  alt="Perfil" />
                        @else
                            <img src="{{url('/')}}/files/{{$user->id}}/default.png"  alt="Perfil" />
                            @endif
					<span class="profile-info">
						@if($user){{substr($user->name,0,11)}} @endif
						@if($user->privileges >= 10)<small>Administrador</small>@endif
					</span>
                    </a>
                    <ul class="dropdown-menu animation-dock">
                        <li class="dropdown-header">Links</li>
                        <li><a href="{{url('/')}}">Landing Page</a></li>
                        <li><a href="{{route('store.index')}}">Store</a></li>
                        <li class="dropdown-header">Config</li>
                        <li><a href="{{route('profile',['id'=>$user->id])}}">Meu Perfil</a></li>
                        {{--<li><a href="#"><span class="badge style-danger pull-right">16</span>My blog</a></li>
                        <li><a href="#">My appointments</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-fw fa-lock"></i> Lock</a></li>--}}
                        <li><a href="{{route('logout')}}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
            </ul><!--end .header-nav-profile -->
            <!--<ul class="header-nav header-nav-toggle">
                <li>
                    <a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                </li>
            </ul><!--end .header-nav-toggle -->
        </div><!--end #header-navbar-collapse -->
    </div>
</header>
<!--END HEADER-->

<div id="base">
    <!-- BEGIN OFFCANVAS LEFT -->
    {{--<div class="offcanvas"></div>--}}
    <!--end .offcanvas-->
    <!-- BEGIN CONTENT-->
    <div id="content">
        @yield('content')
    </div>
    <!--END CONTENT-->
    <!-- BEGIN MENUBAR-->
    <div id="menubar" class="animate">
        <div class="menubar-fixed-panel">
            <div>
                <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <div class="expanded">
                <a href="{{route('dashboard')}}">
                    <span class="text-lg text-bold text-primary "><img src="{{asset('assets/images/logo_guimo_100px.png')}}" alt="Guimo"> GUIMO</span>
                </a>
            </div>
        </div>
        <div class="menubar-scroll-panel">
            <!-- BEGIN MAIN MENU -->




            <ul id="main-menu" class="gui-controls">
                <!-- BEGIN DASHBOARD -->
                <li>
                    <a href="{{route('profile',['id'=>$user->id])}}">
                        <div class="gui-icon"><i class="fa fa-user"></i></div>
                        <span class="title">Meu Perfil</span>
                    </a>
                </li><!--end /menu-li -->
                <li>
                    <a href="{{route('dashboard')}}">
                        <div class="gui-icon"><i class="fa fa-info-circle"></i></div>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <!-- END DASHBOARD -->

                @if($user->privileges >= 10)
                    <li class="gui-folder">
                        <a>
                            <div class="gui-icon"><i class="fa fa-users"></i></div>
                            <span class="title">Novos Membros</span>
                        </a>
                        <ul>
                            <li><a href="{{route('user.adminCode')}}"><span class="title">Link para cadastro</span></a></li>
                        </ul>

                    </li>

                    <li class="gui-folder">
                        <a>
                            <div class="gui-icon"><i class="md md-apps"></i></div>
                            <span class="title">Apps</span>
                        </a>
                        <ul>
                            <li><a href="{{route('apps.android')}}"><span class="title">Android</span></a></li>
                            <li><a href="#"><span class="title">iOS</span></a></li>
                        </ul>

                    </li>

                <!-- BEGIN EMAIL -->
                <li class="gui-folder" >
                    <a>
                        <div class="gui-icon"><i class="md md-email"></i></div>
                        <span class="title">Emails</span>
                    </a>
                    <!--start submenu -->
                    <ul>
                        {{--<li><a href="{{route('dashboard')}}"><span class="title">Informações</span></a>
                        </li>--}}
                        <li><a href="{{route('user_emails.index')}}" ><span class="title">Emails Cadastrados</span></a></li>

                        <li><a href="{{route('user_emails.status')}}" ><span class="title">Alterar Status</span></a></li>

                        <li><a href="{{route('emails.index')}}" ><span class="title">Email Padrão</span></a></li>

                        <li><a href="{{route('emails.send')}}" ><span class="title">Enviar Emails</span></a></li>

                    </ul><!--end /submenu -->
                </li><!--end /menu-li -->
                <!-- BEGIN EMAIL -->
                <li class="gui-folder">
                    <a>
                        <div class="gui-icon"><i class="md md-shopping-cart"></i></div>
                        <span class="title">Guimo Store</span>
                    </a>
                    <!--start submenu -->
                    <ul>
                        <li><a href="{{route('admin.store.index')}}" ><span class="title">Informações</span></a></li>

                        <li><a href="{{route('admin.store.categories')}}" ><span class="title">Categorias</span></a></li>

                        <li><a href="{{route('admin.store.products')}}" ><span class="title">Produtos</span></a></li>

                        <li><a href="#" ><span class="title">Breve...</span></a></li>

                    </ul><!--end /submenu -->
                </li><!--end /menu-li -->
                @endif

                <!-- BEGIN LEVELS -->
                <li class="gui-folder">
                    <a>
                        <div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
                        <span class="title">Menu levels demo</span>
                    </a>
                    <!--start submenu -->
                    <ul>
                        <li><a href="#"><span class="title">Item 1</span></a></li>
                        <li><a href="#"><span class="title">Item 1</span></a></li>
                        <li class="gui-folder">
                            <a href="javascript:void(0);">
                                <span class="title">Open level 2</span>
                            </a>
                            <!--start submenu -->
                            <ul>
                                <li><a href="#"><span class="title">Item 2</span></a></li>
                                <li class="gui-folder">
                                    <a href="javascript:void(0);">
                                        <span class="title">Open level 3</span>
                                    </a>
                                    <!--start submenu -->
                                    <ul>
                                        <li><a href="#"><span class="title">Item 3</span></a></li>
                                        <li><a href="#"><span class="title">Item 3</span></a></li>
                                        <li class="gui-folder">
                                            <a href="javascript:void(0);">
                                                <span class="title">Open level 4</span>
                                            </a>
                                            <!--start submenu -->
                                            <ul>
                                                <li><a href="#"><span class="title">Item 4</span></a></li>
                                                <li class="gui-folder">
                                                    <a href="javascript:void(0);">
                                                        <span class="title">Open level 5</span>
                                                    </a>
                                                    <!--start submenu -->
                                                    <ul>
                                                        <li><a href="#"><span class="title">Item 5</span></a></li>
                                                        <li><a href="#"><span class="title">Item 5</span></a></li>
                                                    </ul><!--end /submenu -->
                                                </li><!--end /submenu-li -->
                                            </ul><!--end /submenu -->
                                        </li><!--end /submenu-li -->
                                    </ul><!--end /submenu -->
                                </li><!--end /submenu-li -->
                            </ul><!--end /submenu -->
                        </li><!--end /submenu-li -->
                    </ul><!--end /submenu -->
                </li><!--end /menu-li -->
                <!-- END LEVELS -->

            </ul><!--end .main-menu -->
            <!-- END MAIN MENU -->
            <div class="menubar-foot-panel">
                <small class="no-linebreak hidden-folded">
                    <span class="opacity-75">Copyright &copy; {{date('Y')}}</span> <strong>Guimo</strong>
                </small>
            </div>
        </div><!--end .menubar-scroll-panel-->
    </div><!--end #menubar-->
    <!-- END MENUBAR -->

</div>

<script src="{{asset('materialadmin/assets/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/libs/autosize/jquery.autosize.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/libs/toastr/toastr.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/libs/typeahead/typeahead.bundle.min.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/core/source/App.min.js')}}"></script>

{{--<script src="{{asset('materialadmin/assets/js/core/demo/Demo.js')}}"></script>
<script src="{{asset('materialadmin/assets/js/core/demo/DemoDashboard.js')}}"></script>--}}


{{--<!-- bootstrap progress js -->
<script src="{{asset('assets/js/progressbar/bootstrap-progressbar.min.js')}}"></script>
<script src="{{asset('assets/js/nicescroll/jquery.nicescroll.min.js')}}"></script>
<!-- icheck -->
<script src="{{asset('assets/js/icheck/icheck.min.js')}}"></script>

<script src="{{asset('assets/js/notify/pnotify.core.js')}}"></script>
<script src="{{asset('assets/js/notify/pnotify.buttons.js')}}"></script>
<script src="{{asset('assets/js/notify/pnotify.nonblock.js')}}"></script>

<script src="{{asset('assets/js/custom.js')}}"></script>--}}

@yield('script')


</body>

</html>