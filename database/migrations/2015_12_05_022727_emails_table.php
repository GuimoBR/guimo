<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('emails',function(Blueprint $table){
            $table->increments('id');
            $table->string('name',80);
            $table->string('from',120);
            $table->string('subject',80);
            $table->string('type',30)->default("text");
            $table->tinyInteger('status')->default(0);
            $table->text('body');
            $table->string('obs',120)->nullable();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('emails');
    }
}
