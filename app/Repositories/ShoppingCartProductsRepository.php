<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ShoppingCartProductsRepository
 * @package namespace Guimo\Repositories;
 */
interface ShoppingCartProductsRepository extends RepositoryInterface
{
    //
}
