@extends('layouts.dashboard2')
@section('style')

@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">{{$email->name}}</h2>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>{{$email->name}}</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <a href="{{route('emails.index')}}" class="btn btn-default-dark btn-flat btn-block ink-reaction"><i class="fa fa-arrow-left"></i> Voltar</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body style-default-bright"><!--end .card-head -->



                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <p><strong>Nome do email: </strong>{{$email->name}}</p>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <p><strong>Email de: </strong>{{$email->from}}</p>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <p><strong>Tipo: </strong>{{$email->type}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <p><strong>Título do email:</strong> {{$email->subject}}</p>
                                </div>
                                <div class="col-md-12 separator">
                                    <h3> <strong>Mensagem: </strong></h3>
                                    <div>@if($email->type == "text") {{$email->body}} @elseif($email->type == "text/html") {!! $email->body !!} @endif</div>
                                </div>
                                <div class="col-md-12 separator">
                                    <h3> <strong>Observações:</strong></h3>
                                    <p>{{$email->obs}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')
    <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/js/datatables/js/jquery.dataTables.js')}}"></script>
    <script>
        $(document).ready(function() {


        })
    </script>


@endsection