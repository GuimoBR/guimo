var dashboardApp = angular.module('dashboardApp',[
    'ui.router',
    'satellizer',
    'dashboardControllers',
    'ngToast',
    'ngCookies'
]);

dashboardApp.config(['$stateProvider','$urlRouterProvider','$authProvider','$locationProvider',function($stateProvider,$urlRouterProvider,$authProvider,$locationProvider){

    $authProvider.loginUrl = 'http://guimo.dev/authenticate';
    $urlRouterProvider.otherwise('/');



    $stateProvider
        .state('/',{
            url:'/',
            templateUrl:'../partials/login.html',
            controller: 'AuthController as auth'
        })
        .state('dashboard',{
            url:'/dashboard',
            templateUrl:'../partials/interessados.html',
            controller:'DashboardController as dashboard'
        })
}]);

dashboardApp.config(['ngToastProvider', function(ngToastProvider) {
    ngToastProvider.configure({
        animation: 'fade' // or 'fade'
    });
}]);
