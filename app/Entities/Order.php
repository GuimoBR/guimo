<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Order extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'orders';
    protected $fillable = ['user_id','shopping_cart_id','status','status_info','total_price','discount_percent','prom_code','order_date','obs'];

    public function shopping_carts(){
        return $this->hasMany(ShoppingCart::class);
    }

    public function owner(){
        return $this->belongsTo(User::class);
    }
}
