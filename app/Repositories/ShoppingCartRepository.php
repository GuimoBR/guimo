<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ShoppingCartRepository
 * @package namespace Guimo\Repositories;
 */
interface ShoppingCartRepository extends RepositoryInterface
{
    //
}
