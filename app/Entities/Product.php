<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'products';
    protected $fillable = ['slug','name','description','price','thumb_image','is_avaible','stock'];


    public function categories(){
        return $this->belongsToMany(Category::class,'categories_products','product_id','category_id');
    }

    public function shopping_cart(){
        return $this->belongsToMany(ShoppingCart::class,'shopping_cart_products','product_id','shopping_cart_id');
    }

    public function images(){
        return $this->hasMany(ProductImage::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

}
