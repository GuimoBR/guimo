
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Guimo | Erro </title>

    <!-- Bootstrap core CSS -->

    @if(env('APP_ENV') == 'development')
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/icheck/flat/blue.css')}}" rel="stylesheet">

    @else
        <link href="{{secure_asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

        <link href="{{secure_asset('assets/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{secure_asset('assets/css/animate.min.css')}}" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="{{secure_asset('assets/css/custom.css')}}" rel="stylesheet">
        <link href="{{secure_asset('assets/css/icheck/flat/blue.css')}}" rel="stylesheet">
            @endif

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md e404">

<div class="container body">

    <div class="main_container">

        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <p><a href="/">
                            @if(env('APP_ENV')=='development')
                            <img src="{{asset('assets/images/logo_guimo_black320.png')}}" alt="startup-logo"></a></p>
                            @else
                            <img src="{{secure_asset('assets/images/logo_guimo_black320.png')}}" alt="startup-logo"></a></p>
                            @endif
                    <h1 class="error-number">404</h1>
                    <h2>Desculpe-nos, mas não encontramos a página que você procura.</h2>
                    <p>Esta página não existe, clique <a href="/" class="link404">aqui</a> para voltar ao início ou tente novamente.
                    </p>
                </div>
            </div>
        </div>
        <!-- /page content -->

    </div>
    <!-- footer content -->
</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

@if(env('APP_ENV') == 'development')
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
@else
    <script src="{{secure_asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{secure_asset('assets/js/bootstrap.min.js')}}"></script>
        @endif
<!-- bootstrap progress js -->
<!--<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<!--<script src="js/icheck/icheck.min.js"></script>-->

<!--<script src="{{asset('assets/js/custom.js')}}"></script>

<!-- /footer content -->
</body>

</html>