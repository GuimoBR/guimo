<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 22:34
 */

namespace Guimo\Services;


use Guimo\Repositories\EmailRepository;
use Guimo\Repositories\UserEmailsRepository;
use Guimo\Validators\EmailValidator;
use Guimo\Validators\UserEmailsValidator;
use Illuminate\Database\QueryException;
use Illuminate\Mail\Mailer;
use Prettus\Validator\Exceptions\ValidatorException;
use Auth;

class UserEmailsService
{

    /**
     * @var EmailRepository
     */
    protected $repository;

    /**
     * @var EmailValidator
     */
    protected $validator;

    public function __construct(UserEmailsRepository $repository, UserEmailsValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function create(array $data){
        try{
            $this->validator->with($data)->passesOrFail();
            $this->repository->create($data);
            return [
                'success'=>true,
                'message'=>'Email cadastrado com sucesso'
            ];
        }catch(ValidatorException $e){
            return [
                'error'=>true,
                'message'=>$e->getMessageBag()
            ];
        }
    }

    /**
     * Change email status to 0 or 1
     * @param array $data
     */
    public function updateStatus(array $data){
        try{
            $this->repository->update($data,$data['id']);
            return ['success'=>true,'message'=>'Status alterado com sucesso'];
        }catch(QueryException $e){
            return ['error'=>true,'message'=>$e->getMessage()];
        }
    }


    public function findWhereIfAuthenticated(array $where, $columns = ['*']){
        if(Auth::check()){
            return $this->repository->findWhere($where,$columns);
        }else{
            return response()->json(['error'=>true,'message'=>'Você não está logado'],403);
        }

    }


}