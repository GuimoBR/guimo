/**
 * Created by jorge on 11/12/2015.
 */
function changeConfig(form_data, url_to,title_to){
    $.ajax({
        method: 'PUT',
        url: url_to,
        data:form_data
    }).success(function(response){
        var notify_type = null;
        var message = '';
        if(response.success){
            notify_type = 'success';
            message = response.message;
        }else{
            notify_type = 'error';
            message = response.message;
            if(response.message instanceof Object){
                message = '';
                $.each(response.message,function(index,msg){
                    message += msg+"\n";
                });
            }
        }

        new PNotify({
            title: title_to,
            text: message,
            type: notify_type
        });

    }).error(function(response){
        console.log(response);
        new PNotify({
            title: title_to,
            text: response.message,
            type: 'error'
        });
    });
}

