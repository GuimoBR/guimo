<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;

class FlappyGuimo extends Model
{
    //

    protected  $table = 'flappy_ranking';

    protected $fillable = [
        'playerName',
        'points',
        'status'
    ];
}
