<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository
 * @package namespace Guimo\Repositories;
 */
interface CategoryRepository extends RepositoryInterface
{
    //
}
