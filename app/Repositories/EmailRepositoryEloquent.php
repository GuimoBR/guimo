<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 22:22
 */

namespace Guimo\Repositories;


use Guimo\Entities\Email;
use Prettus\Repository\Eloquent\BaseRepository;

class EmailRepositoryEloquent extends BaseRepository implements EmailRepository
{
    public function model(){
        return Email::class;
    }

}