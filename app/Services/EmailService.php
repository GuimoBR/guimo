<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 22:34
 */

namespace Guimo\Services;


use Bogardo\Mailgun\Facades\Mailgun;
use Guimo\Repositories\EmailRepository;
use Guimo\Repositories\UserEmailsRepository;
use Guimo\Validators\EmailValidator;
use Guimo\Validators\UserEmailsValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;
use Auth;
use DateTime;

class EmailService
{

    /**
     * @var EmailRepository
     */
    protected $repository;



    /**
     * @var Mailer
     */
    protected $mailer;

    public function __construct(EmailRepository $repository,Mailer $mailer)
    {
        $this->repository = $repository;
        $this->mailer = $mailer;
    }


    public function createEmailTemplate(array $data){

        $rules =[
            'name' =>  'required|max:80',
            'from' =>  'required|email|max:120',
            'subject' => 'required|max:80',
            'body'  => 'required'
        ];

        $messages = [
            'required'  => 'O campo :attribute é obrigatório',
            'max'       => 'O campo :attribute deve ter no máximo :max caractéres',
            'email'     => 'O campo :attribute deve ser preenchido com um email'
        ];
        $customAttributes = [
            'name'=>"Nome",
            'from'=>"De",
            'subject'=>'Título',
            'body'=>"Mensagem"
        ];

        unset($data['_token']);
        $validator = Validator::make($data,$rules,$messages,$customAttributes);

        if($validator->fails()){
            return $validator;
        }else{
            $this->repository->create($data);
            return true;
        }
    }

    public function getEmail($id){
        try{
            return $this->repository->find($id);
        }catch(ModelNotFoundException $e){
            return ['error'=>true,'message'=>$e->getMessage()];
        }
    }

    public function sendRaw(array $data){
        $rules = ['to'=>'required','body'=>'required','type'=>'required','from'=>'required'];
        $messages = ['required'=>'Você deve fornecer o(s) :attribute para enviar uma mensagem'];
        $cutomAttributes = ['to'=>'Destinatário(s)','body'=>'Mensagem','type'=>'tipo','from'=>'required'];

        $validator = Validator::make($data,$rules,$messages,$cutomAttributes);

        if($validator->fails()){
            return $validator;
        }else{

            $data['to'] = explode(',',$data['to']);
            $this->mailer->raw($data['body'],function($message) use ($data){
                foreach($data['to'] as $to){
                    $message->to($to);
                }
                $message->from($data['from']);
                $message->subject($data['subject']);
                $message->setBody($data['body'],$data['type']);

            });

            return true;
        }
    }

    public function sendPage(array $data){
        $rules = ['name'=>'required', 'to'=>'required'];
        $messages = ['required'=>'O campo :attribute é obrigatório!'];
        $cutomAttributes = ['name'=>'Nome', 'to'=>'Email Destino'];
        $validator = Validator::make($data,$rules,$messages,$cutomAttributes);
        if($validator->fails()){
            return $validator;
        }else{

            $this->mailer->send('emails.userlinkemail',['name'=>$data['name'],'link'=>$data['linkurl']],function($m) use($data){
                $m->from('contato@guimo.toys','Guimo');
                $m->to($data['to'],$data['name'])->subject('Bem vindo ao guimo!');
            });
            return true;
        }
    }

    public function getEmailsStatus()
    {
        $msgClient = new \Mailgun\Mailgun(env('MAILGUN_SECRET'));
        $domain = env('MAILGUN_DOMAIN');
        $queryString = ['begin' => date('D, 1 M Y H:i:s -0000'), 'ascending' => 'yes', 'pretty' => 'yes'];
        $emailList = $msgClient->get("$domain/events", $queryString);
        $emailFormat = "";
        if (count($emailList->http_response_body->items) > 0) {
            $data = new DateTime();
            $i = 0;
            foreach ($emailList->http_response_body->items as $key => $value) {
                $emailFormat[$value->recipient][$key] = $value;
                $i++;
            }
        }
       // dd($emailFormat);
        return $emailFormat;
    }


    public function getMessage($id){
        try{
            $email = $this->repository->find($id);
            return ['message'=>$email];
        }catch(ModelNotFoundException $e){
            return ['error'=>true,'message'=>$e->getMessage()];
        }
    }

    public function getAll(){
        return $this->repository->all();
    }

}