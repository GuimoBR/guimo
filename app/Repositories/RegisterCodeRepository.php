<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegisterCodeRepository
 * @package namespace Guimo\Repositories;
 */
interface RegisterCodeRepository extends RepositoryInterface
{
    //
}
