<?php

namespace Guimo\Providers;

use Illuminate\Support\ServiceProvider;

class GuimoRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind(\Guimo\Repositories\EmailRepository::class,\Guimo\Repositories\EmailRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\UserRepository::class,\Guimo\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\FlappyGuimoRepository::class,\Guimo\Repositories\FlappyGuimoRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\UserEmailsRepository::class,\Guimo\Repositories\UserEmailsRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\CountryRepository::class,\Guimo\Repositories\CountryRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\ProductRepository::class,\Guimo\Repositories\ProductRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\CategoryRepository::class,\Guimo\Repositories\CategoryRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\CategoriesProductsRepository::class,\Guimo\Repositories\CategoriesProductsRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\ProductImageRepository::class,\Guimo\Repositories\ProductImageRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\ShoppingCartRepository::class,\Guimo\Repositories\ShoppingCartRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\OrderRepository::class,\Guimo\Repositories\OrderRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\UserInfoRepository::class,\Guimo\Repositories\UserInfoRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\RegisterCodeRepository::class,\Guimo\Repositories\RegisterCodeRepositoryEloquent::class);
        $this->app->bind(\Guimo\Repositories\AppRepository::class,\Guimo\Repositories\AppRepositoryEloquent::class);
    }
}
