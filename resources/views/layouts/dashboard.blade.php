<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Guimo | Dashboard </title>

    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" type='image/x-icon' href="{{secure_asset('assets/images/ico/favicon.ico')}}">
    <link href="{{secure_asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{secure_asset('assets/css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{secure_asset('assets/css/custom.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{secure_asset('assets/css/maps/jquery-jvectormap-2.0.1.css')}}" />
    <link href="{{secure_asset('assets/css/icheck/flat/green.css')}}" rel="stylesheet" />
    <link href="{{secure_asset('assets/css/floatexamples.css')}}" rel="stylesheet" type="text/css" />

    @yield('style')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{route('dashboard')}}" class="site_title"><img src="{{secure_asset('assets/images/logo_guimo_50px.png')}}" alt="Guimo"><span>Guimo</span></a>
                </div>
                <div class="clearfix"></div>


                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="{{secure_asset('assets/images/user.png')}}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Bem vindo,</span>
                        <h2>@if($user) {{$user->name}}  @endif</h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>Menu</h3>
                        <ul class="nav side-menu">
                            <li><a href="{{route('profile')}}"><i class="fa fa-user"></i> Meus Dados</a>
                            </li>
                            <!--<li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><a href="tables.html">Tables</a>
                                    </li>
                                    <li><a href="tables_dynamic.html">Table Dynamic</a>
                                    </li>
                                </ul>
                            </li>-->
                        </ul>
                    </div>

                    @if($user->privileges >= 10)
                        <div class="menu_section">
                            <h3>Adm. Landing Page</h3>
                            <ul class="nav side-menu">
                                <li><a href="{{route('dashboard')}}"><i class="fa fa-info-circle"></i> Informações</a></li>
                                <li><a><i class="fa fa-envelope-o"></i> Emails <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="{{route('user_emails.index')}}">Emails Cadastrados</a></li>
                                        <li><a href="{{route('user_emails.status')}}">Alterar Status</a></li>
                                        <li><a href="{{route('emails.index')}}">Email Padrão</a></li>
                                        <li><a href="{{route('emails.send')}}">Enviar Emails</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-gamepad"></i> Flappy Guimo <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="{{route('flappyguimo.ranking')}}">Ranking</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-laptop"></i> Breve <span class="label label-success pull-right">Coming Soon</span></a>
                                </li>
                            </ul>
                        </div>

                        <div class="menu_section">
                            <h3>Adm. Guimo Store</h3>
                            <ul class="nav side-menu">
                                <li><a href="{{route('admin.store.index')}}"><i class="fa fa-info-circle"></i> Informações</a></li>
                                <li><a href="{{route('admin.store.categories')}}"><i class="fa fa-list"></i> Categorias</a></li>
                                <li><a href="{{route('admin.store.products')}}"><i class="fa fa-shopping-cart"></i> Produtos</a></li>
                                <li><a><i class="fa fa-laptop"></i> Breve <span class="label label-success pull-right">Coming Soon</span></a>
                                </li>
                            </ul>
                        </div>
                    @endif

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                   <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>-->
                    <span></span>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li class="">
                            <a href="{{route('profile')}}" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{secure_asset('assets/images/user.png')}}" alt="">@if($user) {{$user->name}} @endif
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="{{route('profile')}}"> <i class="fa fa-user pull-right"></i> Profile</a>
                                </li>
                                <li><a href="{{route('store.index')}}"> <i class="fa fa-shopping-cart pull-right"></i> Guimo Store</a>
                                </li>
                                <!--<li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">Help</a>
                                </li>-->
                                <li><a href="{{route('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="{{route('dashboard')}}">Adm Landing Page</a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.store.index')}}">Adm Guimo Store</a>
                        </li>


                        <!--<li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                <li>
                                    <a>
                                            <span class="image">
                                        <img src="{{secure_asset('assets/images/user.png')}}" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                            <span class="image">
                                        <img src="{{secure_asset('assets/images/user.png')}}" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                            <span class="image">
                                        <img src="{{secure_asset('assets/images/user.png')}}" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                            <span class="image">
                                        <img src="{{secure_asset('assets/images/user.png')}}" alt="Profile Image" />
                                    </span>
                                            <span>
                                        <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                        Film festivals used to be do-or-die moments for movie makers. They were where...
                                    </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>-->

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
            <div class="" style="height: auto;">

                @yield('content')


            </div>


        </div>
        <!-- /page content -->
    </div>


</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="{{secure_asset('assets/js/jquery.min.js')}}"></script>
<script src="{{secure_asset('assets/js/bootstrap.min.js')}}"></script>

<!-- bootstrap progress js -->
<script src="{{secure_asset('assets/js/progressbar/bootstrap-progressbar.min.js')}}"></script>
<script src="{{secure_asset('assets/js/nicescroll/jquery.nicescroll.min.js')}}"></script>
<!-- icheck -->
<script src="{{secure_asset('assets/js/icheck/icheck.min.js')}}"></script>

<script src="{{secure_asset('assets/js/notify/pnotify.core.js')}}"></script>
<script src="{{secure_asset('assets/js/notify/pnotify.buttons.js')}}"></script>
<script src="{{secure_asset('assets/js/notify/pnotify.nonblock.js')}}"></script>

<script src="{{secure_asset('assets/js/custom.js')}}"></script>

@yield('script')


</body>

</html>