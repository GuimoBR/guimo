<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class App extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name','filename','version','description','changelog','target_os'];

}
