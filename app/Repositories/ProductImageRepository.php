<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductImageRepository
 * @package namespace Guimo\Repositories;
 */
interface ProductImageRepository extends RepositoryInterface
{
    //
}
