<?php

namespace Guimo\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Guimo\Repositories\CategoriesProductsRepository;
use Guimo\Entities\CategoriesProducts;

/**
 * Class CategoriesProductsRepositoryEloquent
 * @package namespace Guimo\Repositories;
 */
class CategoriesProductsRepositoryEloquent extends BaseRepository implements CategoriesProductsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CategoriesProducts::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
