<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 15/04/2016
 * Time: 18:47
 */

Route::get('/dashboard',[
    'as'=>'dashboard',
    'uses'=>'DashboardController@index',
    'middleware'=>'auth',
]);

Route::get('profile/{id}',[
    'as'=>'profile',
    'uses'=>'UserController@show',
    'middleware'=>'auth'
]);

Route::put('profile/{id}/general',[
    'as'=>'profile.update_general',
    'uses'=>'UserController@updateGeneral',
    'middleware'=>'auth',
]);

Route::put('profile/{id}/personal',[
    'as'=>'profile.update_personal',
    'uses'=>'UserController@updatePersonal',
    'middleware'=>'auth',
]);

Route::post('profile/{id}/upload',[
    'as'=>'profile.upload_files',
    'uses'=>'UserController@uploadFiles',
    'middleware'=>'auth'
]);


Route::get('/flappyguimo',[
    'as'=>'flappyguimo.game',
    'uses'=>'FlappyGuimoController@game'
]);



Route::get('/files/{id}/{image}',function($id=null,$image = null){
    $path = storage_path('app').'/users/'.$id.'/'.$image;

    if(file_exists($path)){
        return response()->download($path);
    }else{
        return response()->download(storage_path('app').'/users/default/user.png');
    }
});

Route::get('/app/{version}/{name}',function($version=null,$name = null){
    $path = storage_path('app').'/apps/'.$version.'/'.$name;
    if(file_exists($path)){
        return response()->download($path);
    }else{
        return response()->view('errors.404');
    }
});

