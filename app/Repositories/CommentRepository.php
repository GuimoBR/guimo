<?php

namespace Guimo\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CommentRepository
 * @package namespace Guimo\Repositories;
 */
interface CommentRepository extends RepositoryInterface
{
    //
}
