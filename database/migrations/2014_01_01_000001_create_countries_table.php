<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            //
            $table->increments("id");
            $table->string('name',60);
            $table->boolean('is_avaible')->default(false);
            $table->text('obs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('countries', function (Blueprint $table) {
            //
        });*/
        Schema::drop("countries");
    }
}
