<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ShoppingCart extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'shopping_carts';
    protected $fillable = ['product_id','order_id','product_quantity'];


    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'shopping_cart_products','shopping_cart_id','product_id');
    }

}
