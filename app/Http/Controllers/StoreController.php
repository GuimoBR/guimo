<?php

namespace Guimo\Http\Controllers;

use Guimo\Repositories\CategoryRepository;
use Guimo\Repositories\ProductRepository;
use Guimo\Services\CategoryService;
use Guimo\Services\ProductService;
use Guimo\Services\UserService;
use Illuminate\Http\Request;

use Guimo\Http\Requests;
use Guimo\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\UrlGenerator as URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class StoreController extends Controller
{

    private $categories = null;
    private $products = null;
    private $user = null;
    private $scart = null;
    private $cartId = null;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductService
     */
    private $productService;
    /**
     * @var URL
     */
    private $url;
    /**
     * @var UserService
     */
    private $userService;


    public function __construct(CategoryRepository $categoryRepository,
                                CategoryService $categoryService,
                                ProductRepository $productRepository,
                                ProductService $productService,
                                UserService $userService,
                                URL $url,
                                Request $request)
    {


        $this->categoryRepository = $categoryRepository;
        $this->categoryService = $categoryService;
        $this->productRepository = $productRepository;
        $this->productRepository = $productRepository;
        $this->productService = $productService;
        $this->categories = $this->categoryService->getAllCategories();
        $this->user = Auth::user();
        $this->url = $url;
        $this->scart = $request->session()->all();
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        /*$request->session()->forget('cart');
        $request->session()->forget('id');
        $request->session()->forget('totalPrice');
        $request->session()->forget('totalProducts');*/

        $this->products   = $this->productService->getAllProducts();
        foreach($this->products as $product){
            if($product->discount_percent > 0){
                $product->final_price = number_format(round((1 - ($product->discount_percent/100))*$product->price,2),2,',','.');
            }else{
                $product->final_price = number_format(round($product->price,2),2,',','.');
            }
        }
        return view('store.index',['user'=>$this->user,'categories'=>$this->categories,'products'=>$this->products,'session'=>$this->scart,'breadcumbs'=>[
            0=>['link'=>'store.index','name'=>'Home','slug'=>null],
        ]]);
    }

    public function category($slug){

        $category = $this->categoryService->getProductsFromCategorySlug($slug);
        $name = null;
        $slug = null;


        $this->products = $category->products()->paginate(8);
        $name = $category->name;
        $slug = $category->slug;



        if(!is_null($this->products)) {
            foreach ($this->products as $product) {
                if ($product->discount_percent > 0) {
                    $product->final_price = number_format(round((1 - ($product->discount_percent / 100)) * $product->price, 2), 2, ',', '.');
                } else {
                    $product->final_price = number_format(round($product->price, 2), 2, ',', '.');
                }
            }
        }

        return view('store.index',['user'=>$this->user,
            'categories'=>$this->categories,
            'products'=>$this->products,
            'slug'=>$slug,
            'session'=>$this->scart,
        'breadcumbs'=>[
            0=>['link'=>'store.index','name'=>'Home','slug'=>null],
            1=>['link'=>'store.category','name'=>$name,'slug'=>$slug]
        ]]);
    }

    public function product($cslug,$pslug){
        $category = $this->categoryService->getCategoryNameFromSlug($cslug);

        $product = $this->productService->getProductFromSlug($pslug);
        if($product->discount_percent > 0){
            $product->final_price = number_format(round((1 - ($product->discount_percent/100))*$product->price,2),2);
        }

        return view('store.product',[
            'user'=>$this->user,
            'categories'=>$this->categories,
            'product'=>$product,
            'session'=>$this->scart,
            'breadcumbs'=>[
                0=>['link'=>'store.index','name'=>'Home','slug'=>null],
                1=>['link'=>'store.category','name'=>$category,'slug'=>$cslug],
                2=>['link'=>'store.product','name'=>$product->name,'slug'=>null]]]);
    }

    public function cartClear(Request $request){
        $request->session()->forget('id');
        $request->session()->forget('cart');
        $request->session()->forget('totalPrice');
        $request->session()->forget('totalProducts');
        $request->session()->forget('shipment');
        return redirect()->route('cart.show',['cartid'=>0]);
    }
    public function cartAdd(Request $request,$pslug){

        $product = $this->productService->getProductFromSlug($pslug);
        if(!$request->session()->has('id')){
            $request->session()->put('id',sha1($this->user->id));
        }

        //dd($request->all());
        
        if($request->session()->has('shipment')){
            $request->session()->forget('shipment');
        }

        $ship['method'] = ($request->input('shipmethod')) ? $request->input('shipmethod') : null;
        $ship['price'] = ($request->input('shipvalue')) ? $request->input('shipvalue') : null;
        $ship['prazo'] = ($request->input('shipprazo')) ? $request->input('shipprazo') : null;

        $request->session()->push('shipment',$ship);

        $product->quantity = $request->input('quantity');
        if($product->discount_percent > 0){
            $product->final_price = round((1 - ($product->discount_percent/100))*$product->price,2);
        }else{
            $product->final_price = $product->price;
        }
        $product->total_price = $product->final_price * $product->quantity;
        $request->session()->push('cart',$product);

        return redirect($this->url->previous());
    }

    public function cart($cartid,Request $request){
        $totalPrice = 0;
        $totalProducts = 0;
        $session = null;
        if(Session::has('id')){
            $session = $this->scart;
        }
        if(Session::has('cart')) {
            foreach ($session['cart'] as $product) {
                $totalPrice += $product->total_price;
                $totalProducts += $product->quantity;
            }
            $ship = $request->session()->get('shipment');
            $totalPrice += $ship[0]['price'];
            $request->session()->put('totalPrice',$totalPrice);
            $request->session()->put('totalProducts',$totalProducts);
        }

        $session = $request->session()->all();

        return view('store.cart',['user'=>$this->user,'categories'=>$this->categories,'session'=>$session,'breadcumbs'=>[
            0=>['link'=>'store.index','name'=>'Home','slug'=>null],
            1=>['link'=>'store.cart','name'=>'Carrinho de Compras','slug'=>null]
        ],'totalPrice'=>$totalPrice,'totalProducts'=>$totalProducts]);
    }

    public function adminStoreIndex(){
        $products = $this->productService->getAllProducts();
        $clients = $this->userService->getAllUserClients();
        return view('store.admin_info',['user'=>$this->user,
            'categories'=>$this->categories,
            'products'=>$products,
            'clients'=>$clients,
        ]);
    }

    public function adminStoreNewCategory(Request $request){

        $createCategory = $this->categoryService->insert($request->all());
        if(is_bool($createCategory)){
            return redirect()->route('admin.store.categories')->with('message','Categoria criada com sucesso');
        }else{
            return redirect()->route('admin.store.categories')->withErrors($createCategory);
        }

    }

    public function adminStoreCategories(){
        $categories = $this->categoryService->getAllCategoriesWithProducts();
        return view('store.admin_categories',['user'=>$this->user,'categories'=>$categories]);
    }

    public function adminStoreCategory($slug){
        $category = $this->categoryService->getProductsFromCategorySlug($slug);
        return view('store.admin_category',['user'=>$this->user,'category'=>$category]);
    }

    public function adminStoreUpdateCategory($slug,Request $request){
        $updateCategory = $this->categoryService->update($slug,$request->all());
        if(is_bool($updateCategory)){
            return redirect()->route('admin.store.category',['slug'=>$slug])->with('message','Categoria criada com sucesso');
        }else{
            return redirect()->route('admin.store.category',['slug'=>$slug])->withErrors($updateCategory);
        }
    }

    public function adminStoreProducts(){
        $products = $this->productService->getProducts();
        return view('store.admin_products',['user'=>$this->user,'products'=>$products,'categories'=>$this->categories]);
    }

    public function adminStoreProduct($slug){
        $product = $this->productService->getProductFromSlug($slug);
        if($product->discount_percent > 0){
            $product->final_price = round((1 - ($product->discount_percent/100))*$product->price,2);
        }else{
            $product->final_price = $product->price;
        }
        return view('store.admin_product',['user'=>$this->user,'product'=>$product,'categories'=>$this->categories]);
    }

    /**
     * Store new product in db
     */
    public function adminStoreNewProduct(Request $request){

        $files = $request->file('images');

        $createProduct = $this->productService->createNewProduct($request);
        if(is_bool($createProduct)){
            return redirect()->route('admin.store.products')->with('message','Produto inserido com sucesso!');
        }else{
            return redirect()->route('admin.store.products')->withErrors($createProduct);
        }

    }

    public function adminStoreUpdateProduct($slug,Request $request){
        $updateProduct= $this->productService->updateProduct($slug,$request->all());
        if(is_bool($updateProduct)){
            return redirect()->route('admin.store.product',['slug'=>$slug])->with('message','Produto atualizado com sucesso!');
        }else{
            return redirect()->route('admin.store.product',['slug'=>$slug])->withErrors($updateProduct);
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
