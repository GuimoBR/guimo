@extends('layouts.store')
@section('content')
    <div class="row">
        <div class="col-md-6 carousel-holder">
            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php $i = 0 ?>
                        @if(count($product->images) > 0)
                            @foreach($product->images as $image)
                                <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class="@if($i == 0) active @endif"></li>
                                <?php $i++ ?>
                            @endforeach
                        @else
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            @endif
                    </ol>
                    <div class="carousel-inner">
                        <?php $i = 1 ?>
                        @if(count($product->images) > 0)
                            @foreach($product->images as $image)

                            <div class="item @if($i == 1) active @endif">
                                <a href="#"><img src="{{url('http://store.guimo.dev/image/'.$product->slug.'/'.$image->file_name)}}" alt="{{$image->file_name}}" style="height: 300px; width: 750px;"></a>
                            </div>
                                <?php $i++ ?>
                            @endforeach
                        @else
                        <div class="item active">
                            <a href="#"><img class="slide-image" src="http://placehold.it/750x300" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img class="slide-image" src="http://placehold.it/750x300" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img class="slide-image" src="http://placehold.it/750x300" alt=""></a>
                        </div>
                        <div class="item">
                            <a href="#"><img class="slide-image" src="http://placehold.it/750x300" alt=""></a>
                        </div>
                            @endif
                    </div>

                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <form action="{{route('cart.add_product',['pslug'=>$product->slug])}}" method="POST">
                {{csrf_field()}}
                <div class="well well-sm">
                    <h3 class="pull-left text-info">{{$product->name}}</h3>
                    @if($product->discount_percent > 0 )
                    <p class="pull-right lead">De &nbsp; <s class="text-danger">R$ {{$product->price}}</s><br>Por R$ {{$product->final_price}} <small class="text-success"> - {{$product->discount_percent}}%</small></p>
                    @else
                        <h3 class="pull-right">R$ {{$product->price}}</h3>
                    @endif
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-left"><strong>Quantidade Disponivel:</strong> {{$product->stock}}</p>
                        </div>
                    </div>
                   <div class="row">
                       <div class="form-group" style="margin-top: -10px;" id="formship">
                           <div class="col-md-12">
                               <label for="CEP" class="control-label">Calcular valor do frete e prazo de entrega</label>
                               <div class="input-group">
                                   <input type="text" name="CEP" id="CEP" placeholder="CEP" class="form-control"/>
                                   <span class="input-group-btn">
                                       <button class="btn btn-primary btn-raised" type="button" id="shipprice">Calcular</button>
                                   </span>
                               </div>
                               <span class="text-center col-md-12" id="shipfeedback"></span>
                           </div>
                       </div>
                   </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group" style="margin-top: -5px;">
                                <label for="quantity" class="control-label">Quantidade</label>
                                <select name="quantity" id="quantity" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12" >
                            <p class="lead">Descrição do produto - <small>leia com atenção</small></p>
                            <p  style="height:100px; max-height: 100px; overflow-y: auto;">
                                {!! nl2br($product->description)!!}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success btn-raised btn-block btn-lg" type="submit"><i class="fa fa-plus"></i> &nbsp; Adicionar ao cesto  </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Comentários ({{count($product->comments)}})</h3>

                    </div>
                </div>
                <hr>
                <div class="row" style="height: 950px; max-height: 950px; overflow-y: auto;">
                    @for($i = 0;$i<6;$i++)
                    <div class="row" style="margin:10px 0;">
                        <div class="col-md-12">
                            <div class="col-md-2 col-lg-2 col-sm-12" style="margin-right: -20px;">
                                <img src="http://guimo.dev/files/1/profile.jpg" alt="profile" class="img-responsive img-circle" style="max-width: 100px;">
                            </div>
                            <div class="col-md-8 col-lg-9 col-sm-12" style="max-height: 250px; overflow-y: auto;">
                                    <div class="col-md-12">
                                        <strong>Nome da Pessoa</strong>
                                        <small class="pull-right">10/10/2015 15:05:02</small>
                                    </div>
                                    <div class="col-md-12" style="margin-top:10px;"><p>Comentário da pessoa</p></div>
                            </div>
                        </div>
                    </div>
                        <hr>
                        @endfor

                    <div class="text-center">
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    @if(!Auth::check())
                        <div class="text-center">
                            <a href="{{route('login')}}" class="btn btn-raised btn-lg btn-warning">Logue para comentar</a>
                        </div>
                        @else
                        <div class="text-center">
                            <p class="lead">Comentar</p>
                            <form action="#" method="POST">
                                {{csrf_field()}}
                                <textarea name="comment" id="comment" cols="30" rows="10" class="form-control" placeholder="Achei o guimo  muito legal!"></textarea>
                                <button type="submit" class="btn btn-block btn-info btn-raised btn-lg">Enviar Comentário</button>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            var shipbutton = $("#shipprice");
            var form_ship = $("#formship");
            var shipfeedback = $("#shipfeedback");
            var cep = $("#CEP");
            shipbutton.click(function(){
                $(this).addClass('disabled');
                $(this).prop('disabled',true);
                shipfeedback.html("<i class='fa fa-spinner fa-spin'>&nbsp;</i><small>Calculando</small>");

                $.ajax({
                    url:'{{url('/')}}/api_v1/correios/consultaPreco/'+cep.val(),
                    method:'GET',

                }).done(function(response){
                    console.log(response);
                    if(response.pac.prazo > 0 || response.sedex.prazo > 0) {
                        form_ship.html("<div class='col-md-12' style='margin-top:10px;'>" +
                                "<input type='radio' name='shipmethod' id='#shipmethod' value='pac' checked>Correios PAC - R$ " + response.pac.valor + " - " + response.pac.prazo + " dia(s) utei(s)" +
                                "</div>" +
                                "<div class='col-md-12'>" +
                                "<input type='radio' name='shipmethod' id='shipmethod' value='sedex'>Correios Sedex - R$ " + response.sedex.valor + " - " + response.sedex.prazo + " dia(s) utei(s)" +
                                "</div>" +
                                "<input type='hidden' name='shipvalue' id='shipvalue' value='" + response.pac.valor + "'> <input type='hidden' id='shipprazo' name='shipprazo' value='" + response.pac.prazo + "'>");
                        form_ship.fadeIn(1000);


                        var shipmethod = $("input[type='radio']");
                        var shipvalue = $("#shipvalue");
                        var shipprazo = $("#shipprazo");

                        shipmethod.click(function () {

                            if ($(this).is(':checked')) {
                                switch ($(this).val()) {
                                    case 'pac':
                                        shipvalue.val(response.pac.valor);
                                        shipprazo.val(response.pac.prazo);
                                        break;
                                    case 'sedex':
                                        shipvalue.val(response.sedex.valor);
                                        shipprazo.val(response.sedex.prazo);
                                        break;
                                    default:
                                        shipvalue.val(response.pac.valor);
                                        shipprazo.val(response.pac.prazo);
                                }
                            }
                        });
                    }else{
                        shipbutton.removeClass('disabled');
                        shipbutton.prop('disabled',false);
                        shipfeedback.addClass('text-danger')
                        shipfeedback.html("CEP Não encontrado ou incorreto!");

                    }
                });

            })
        });
    </script>
@endsection