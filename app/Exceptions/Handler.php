<?php

namespace Guimo\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Mail;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    protected $messages = [
        '400'=>'Requisição incorreta',
        '403'=>'Acesso não permitido',
        '404'=>'Página não encontrada ou inexistente',
        '500'=>'Erro interno no servidor'
    ];
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {


        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if ($e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
            return parent::render($request, $e);
        }

        if($request->is('api_v1/*')){
            return response()->json(['error'=>true,'message'=>$this->messages[$e->getStatusCode()], 'file'=>$e->getFile(),'line'=>$e->getLine(),'status'=>$e->getStatusCode()],$e->getStatusCode());
        }

        if(env('APP_DEBUG')) {
            return parent::render($request, $e);
        }else {

            $url = $request->fullUrl();
            $code = ($e instanceof NotFoundHttpException) ? '404' : $e->getCode();
            $line = $e->getLine();
            $file = $e->getFile();
            $message = ($e instanceof NotFoundHttpException) ? 'Página não encontrada' : $e->getMessage();
            $stackTrace = $e->getTraceAsString();

            Mail::send('emails.error', ['line' => $line, 'errmsg' => $message, 'errfile'=>$file,'url'=>$url, 'errTrace' => $stackTrace,'code'=>$code], function ($m) {
                $m->from('no-reply@guimo.toys','Guimo No-Reply');
                $m->to('jorgejunior@guimo.toys','Jorge Junior')->subject('ERRO - Um error correu na aplicação '.env('APP_NAME'));
                $m->to('douglaspuppio@guimo.toys','Douglas Puppio')->subject('ERRO - Um error correu na aplicação '.env('APP_NAME'));
            });

            return response(view("errors.generic", ['request' => $request, 'code' => $e->getCode(),'message'=>$e->getMessage()]));

        }
    }
}
