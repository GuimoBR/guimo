<?php
Route::group(['domain'=>'store.'.config('app.url')],function(){
    Route::get('/',[
        'as'=>'store.index',
        'uses'=>'StoreController@index',
    ]);

    Route::get('/cart/clear',[
        'as'=>'cart.clear',
        'uses'=>'StoreController@cartClear']);

    Route::get('/category/{slug}',[
        'as'=>'store.category',
        'uses'=>'StoreController@category'
    ]);

    Route::get('category/{cslug}/product/{pslug}',[
        'as'=>'store.product',
        'uses'=>'StoreController@product'
    ]);

    Route::post('/cart/add/{pslug}',[
        'as'=>'cart.add_product',
        'uses'=>'StoreController@cartAdd',
        'middleware'=>'auth',
    ]);

    Route::get('/cart/{cartid}',[
        'as'=>'cart.show',
        'uses'=>'StoreController@cart',
    ]);

    Route::get('/image/{slug}/{image}',function($slug=null,$image = null){
        $path = storage_path('app').'/store/'.$slug.'/'.$image;

        if(file_exists($path)){
            return response()->download($path);
        }else{
            return response()->download(storage_path('app').'/store/default/320150.png');
        }
    });

});