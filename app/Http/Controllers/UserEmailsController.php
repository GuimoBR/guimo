<?php

namespace Guimo\Http\Controllers;

use Guimo\Repositories\UserEmailRepository;
use Guimo\Repositories\UserEmailsRepository;
use Guimo\Services\UserEmailsService;
use Illuminate\Http\Request;

use Guimo\Http\Requests;
use Auth;

class UserEmailsController extends Controller
{
    /**
     * @var UserEmailsRepository
     */
    private $repository;
    /**
     * @var UserEmailsService
     */
    private $service;

    /**
     * EmailController constructor.
     * @param UserEmailsRepository $repository
     * @param UserEmailsService $service
     */
    public function __construct(UserEmailsRepository $repository, UserEmailsService $service)
    {

        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //


        $user = Auth::user();
        $emails = $this->repository->all();

        return view('dashboard.user_emails_index',compact('user','emails'));

    }

    public function remove(){
        $user = Auth::user();

        $emails = $this->repository->all();
        return view('dashboard.user_emails_remove',compact('user','emails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
