<body>
<div>
    <img src="{{asset('v2/images/logo2.png')}}" alt="">
    <h1>Bem Vindo ao Guimo!</h1>
    <div>
        <p>Olá {{$name}} Seja bem vindo à equipe Guimo!.Agora você poder</p>
        <p>Para você fazer seu cadastro, utilize o seguinte link abaixo. Este link só poderá ser acessado <strong class="text-danger">uma única vez.</strong>.
            Portanto, acesse-o somente quando tiver certeza que haverá tempo para preencher todo o formulário.   </p>
        <p>
            {{$link}}
        </p>
        <p>Estamos muito felizes por você fazer parte do nosso time</p>
    </div>
</div>
</body>