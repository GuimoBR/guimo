<?php

namespace Guimo\Http\Controllers;

use Guimo\Entities\User;
use Guimo\Repositories\CountryRepository;
use Guimo\Services\AppService;
use Guimo\Services\EmailService;
use Guimo\Services\UserEmailsService;
use Guimo\Services\UserService;
use Illuminate\Http\Request;

use Guimo\Http\Requests;
use Guimo\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{


    /**
     * @var CountryRepository
     */
    private $countryRepository;

    /**
     * @var User;
     */
    private $user;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var UserEmailsService
     */
    private $userEmailsService;
    /**
     * @var EmailService
     */
    private $emailService;
    /**
     * @var AppService
     */
    private $appService;

    public function __construct(CountryRepository $countryRepository,
                                UserService $userService,
                                UserEmailsService $userEmailsService,
                                EmailService $emailService,
                                AppService $appService)
    {
        $this->countryRepository = $countryRepository;
        $this->user = Auth::user();
        $this->userService = $userService;
        $this->userEmailsService = $userEmailsService;
        $this->emailService = $emailService;
        $this->appService = $appService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $totalEmails = $this->userEmailsService->findWhereIfAuthenticated(['status'=>1]);
        $createdEmails = $this->emailService->getAll();
        $androidApps = $this->appService->getByOS('android');
        $iosApps     = $this->appService->getByOS('ios');

       // dd($totalEmails);
        if($this->user->privileges >= 10){
            return view('dashboard.index',[
                'user'=>$this->user,
                'totalEmails'=>count($totalEmails),
                'createdEmails'=>count($createdEmails),
                'androidApps'=>count($androidApps),
                'iosApps'=>count($iosApps)]);
        }else{
            return redirect('/login')->withErrors(['login'=>'Área de acesso restrito!']);
        }

    }

    public function searchAdmin(Request $request){

        $name = '%'.$request->input('name').'%';
        $admins = $this->userService->searchLike($name);
        $general = null;
        return view('dashboard.search',['user'=>$this->user,'name'=>$request->input('name'),'admins'=>$admins,'general'=>$general]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
