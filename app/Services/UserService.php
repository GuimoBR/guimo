<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 23/12/15
 * Time: 23:32
 */

namespace Guimo\Services;


use Guimo\Entities\UserInfo;
use Guimo\Entities\User;
use Guimo\Repositories\UserRepository;
use Guimo\Repositories\UserInfoRepository;
use Guimo\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;

class UserService
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserValidator
     */
    protected $validator;
    private $rules;
    private $messages;
    private $customAttributes;
    /**
     * @var UserInfoRepository
     */
    private $usersInfoRepository;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Storage
     */
    private $storage;

    /**
     * UserService constructor.
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(UserRepository $repository, UserInfoRepository $usersInfoRepository, UserValidator $validator,Filesystem $filesystem,Storage $storage)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->usersInfoRepository = $usersInfoRepository;
        $this->filesystem = $filesystem;
        $this->storage = $storage;

        $this->rules =[
            'name' =>  'required',
            'email' =>'required',
            'password'   => 'required',
            'old_password'=> 'required',
            'CPF'   => 'required',
            'RG'    => 'required',
            'birthdate'=>'required',
            'birthcity'=>'required',
            'country_id' => 'required',
            'CEP'        => 'required',
            'address'    => 'required',
            'number'     => 'required|numeric',
            'neighborhood'=>'required',
            'city'=>'required',
            'where_know_guimo' => 'required',
        ];

        $this->messages = [
            'required'  => 'O campo :attribute é obrigatório',
            'max'       => 'O campo :attribute deve ter no máximo :max caractéres',
            'numeric'   => 'O campo :attribute deve ser um número',
        ];
        $this->customAttributes = [
            'name'=>"Nome",
            'email'=>"Email",
            'password'=>'Senha Nova',
            'RG' => 'RG',
            'CEP'        => 'CEP',
            'birthdate'=>'Data de Aniversário',
            'birthcity'=>'Cidade onde Nasceu',
            'old_password'=>'Senha Antiga',
            'country_id'=>'País',
            'address'=>'Endereço',
            'number'=>'Número',
            'neighborhood'=>'Bairro',
            'city'=>'Cidade',
            'where_know_guimo'=>'Onde conheceu o Guimo?',
        ];

    }
    public function create(Request $request){
        $this->rules['profile_image'] = 'required|image';
        $this->messages['image'] = 'O campo :attribute deve ser uma imagem!';
        $this->customAttributes['profile_image'] = 'Imagem de perfil';

        unset($this->rules['where_know_guimo']);
        unset($this->rules['old_password']);
        $data = $request->all();
        $validator = Validator::make($data,$this->rules,$this->messages,$this->customAttributes);
        if($validator->fails()){
            return $validator;
        }else{
            unset($data['_token']);
            $birthdate = Carbon::createFromFormat('d/m/Y',$request->input('birthdate'));

            $imagem = $request->file('profile_image');
            $extensao = $imagem->getClientOriginalExtension();
            $profile = "profile.".$extensao;
            $data['profile_image'] = $profile;


            $user = new User;
            $user->name             = $data['name'];
            $user->email            = $data['email'];
            $user->password         = Hash::make($data['password']);
            $user->privileges       = 10;
            $user->save();
            $this->storage->put('users/' . $user->id . '/' . $profile,$this->filesystem->get($request->file('profile_image')));

            $userinfo = new UserInfo;
            $userinfo->user_id    = $user->id;
            $userinfo->country_id = $data['country_id'];
            $userinfo->birthcity  = $data['birthcity'];
            $userinfo->passport   = ($data['passport']) ? $data['passport'] : null;
            $userinfo->RG         = $data['RG'];
            $userinfo->CPF        = $data['CPF'];
            $userinfo->CEP        = $data['CEP'];
            $userinfo->address    = $data['address'];
            $userinfo->number     = $data['number'];
            $userinfo->adjunct    = ($data['adjunct']) ? $data['adjunct'] : null;
            $userinfo->neighborhood = $data['neighborhood'];
            $userinfo->city       = $data['city'];
            $userinfo->profile_image = $profile;
            $userinfo->birthdate  = $birthdate;
            $user->info()->save($userinfo);



            return true;
        }


    }


    public function searchLike($like){
        return User::where('name','LIKE',$like)->paginate(10);
    }

    public function getFromId($id){
        return $this->repository->with(['info'])->find($id);
    }

    /*public function create(array $data){
        try{
            $this->validator->with($data)->passesOrFail();
            $this->repository->create($data);
            return [
                'success'=>true,
                'message'=>'Usuario cadastrado com sucesso'
            ];
        }catch(ValidatorException $e){
            return [
                'error' =>true,
                'message'=>$e->getMessageBag()
            ];
        }
    }*/

    public function update(array $data,$id,$type = 'general'){

        if(Auth::check()){

            $user = Auth::user();

           // if (Hash::check($data['old_pass'], $user->password)) {
            if($type == 'general'){


                unset($this->rules['country_id']);
                unset($this->rules['CEP']);
                unset($this->rules['address']);
                unset($this->rules['number']);
                unset($this->rules['neighborhood']);
                unset($this->rules['city']);
                unset($this->rules['where_know_guimo']);
                unset($this->rules['CPF']);
                unset($this->rules['RG']);
                unset($this->rules['birthdate']);
                unset($this->rules['birthcity']);
            }
            if($type == 'personal'){
                unset($this->rules['name']);
                unset($this->rules['email']);
                unset($this->rules['password']);
                unset($this->rules['old_password']);
                unset($this->rules['where_know_guimo']);
                if($user->privileges < 5) {
                    unset($this->rules['CPF']);
                    unset($this->rules['birthcity']);
                }
            }

            $validator = Validator::make($data,$this->rules,$this->messages,$this->customAttributes);
            if($validator->fails()){
                return $validator;
            }else{

                if($type == 'general'){
                    if(Hash::check($data['old_password'],$user->password)){
                        $data['password'] = Hash::make($data['password']);
                        $this->repository->update($data,$id);
                    }else{
                        return ['error'=>true,'message'=>'As senha antiga digitada não confere com a sua senha, por favor tente novamente'];
                    }
                }

                if($type == 'personal'){

                    $data['birthdate'] = Carbon::createFromFormat('d/m/Y',$data['birthdate']);
                    //$user = $this->usersInfoRepository->update($data,$id);
                    $user->info->birthdate   = $data['birthdate'];
                    $user->info->birthcity  = $data['birthcity'];
                    $user->info->passport   = $data['passport'];
                    $user->info->RG         = $data['RG'];
                    $user->info->CPF        = $data['CPF'];
                    $user->info->CEP        = $data['CEP'];
                    $user->info->address    = $data['address'];
                    $user->info->number     = $data['number'];
                    $user->info->adjunct    = $data['adjunct'];
                    $user->info->neighborhood = $data['neighborhood'];
                    $user->info->city       = $data['city'];
                    $user->info->bio        = $data['bio'];
                    $user->info->save();
                }
                return true;
            }

        }else{
            return ['error' => true,'message'=>'Você não está logado, por favor, tente novamente'];
        }
    }
    public function uploadFiles(Request $request,$id){
        if(Auth::check()) {
            $user = Auth::user();
            $birthdate = Carbon::createFromFormat('d/m/Y',$user->info->birthdate);
            $files = [];
            if (!is_null($request->file('profile_image'))) {
                $profile = 'profile.' . $request->file('profile_image')->getClientOriginalExtension();
                if(!is_null($user->info->profile_image)){
                    $this->storage->delete('/users/'.$id.'/'.$user->info->profile_image);
                }
                $this->storage->put('users/' . $id . '/' . $profile,$this->filesystem->get($request->file('profile_image')));
                $user->info->profile_image = $profile;
                $user->info->birthdate = $birthdate;
                $user->info->save();
            }

            if(!is_null($request->file('arquivo'))) {
                if(!is_null($user->info->uploads)){
                    $uploads = explode(',',$user->info->uploads);
                    foreach($uploads as $upload){
                        $this->storage->delete('/users/'.$id.'/'.$upload);
                    }
                }
                foreach ($request->file('arquivo') as $file) {
                    //$request->file('arquivo')->getClientOriginalName()
                    if (!is_null($file)) {
                        $files[] = $file->getClientOriginalName();
                        $fileName = $file->getClientOriginalName();

                        $this->storage->put('users/' . $id . '/' . $fileName,$this->filesystem->get($file));
                    }
                }
                $files = implode(',', $files);
                $user->info->birthdate = $birthdate;
                $user->info->uploads = $files;
                $user->info->save();
            }


            return true;
        }else{
            return ['error' => true,'message'=>'Você não está logado, por favor, tente novamente'];
        }
    }

    public function getAllUserClients(){
        return $this->repository->findWhere([['privileges','<=',1]]);
    }



}
