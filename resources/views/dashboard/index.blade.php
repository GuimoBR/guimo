@extends('layouts.dashboard2')

@section('content')
    <section>
        <div class="section-body">
            <div class="row">

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-info no-margin">
                                <strong class="pull-right text-success text-lg"><i class="fa fa-envelope fa-3x"></i></strong>
                                <strong class="text-xl" id="count-emails">{{$totalEmails}}</strong> <br/>
                                <span class="text-primary-dark"><a href="{{route('user_emails.index')}}">{{trans('dashboard.total_emails')}}</a></span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-danger no-margin">
                                <strong class="pull-right text-info text-lg"><i class="fa fa-at fa-3x"></i></strong>
                                <strong class="text-xl" id="count-emails-created">{{$createdEmails}}</strong> <br/>
                                <span class="text-primary-dark"><a href="{{route('emails.index')}}">Emails Criados</a></span>
                            </div>
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-warning no-margin">
                                <strong class="pull-right text-accent text-lg"><i class="fa fa-android fa-3x"></i></strong>
                                <strong class="text-xl">{{$androidApps}}</strong><br>
                                <span class="text-primary-dark"><a href="{{route('apps.android')}}">Apps Android</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-success no-margin">
                                <strong class="pull-right text-danger text-lg"><i class="fa fa-apple fa-3x"></i></strong>
                                <strong class="text-xl">{{$iosApps}}</strong><br>
                                <span class="text-primary-dark"><a href="#">Apps IOs</a></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $(document).ready(function(){

            $.ajax({
                url:'{{url('/')}}/api_v1/emails/created',
                method:'GET'
            }).success(function(response){
                //console.log(response);
                if(response != undefined) {
                    $("#count-emails-created").html(response.length);
                }
            });

        });
    </script>
@endsection