<?php

namespace Guimo\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Guimo\Repositories\ShoppingCartProductsRepository;
use Guimo\Entities\ShoppingCartProducts;

/**
 * Class ShoppingCartProductsRepositoryEloquent
 * @package namespace Guimo\Repositories;
 */
class ShoppingCartProductsRepositoryEloquent extends BaseRepository implements ShoppingCartProductsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ShoppingCartProducts::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
