<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CategoriesProducts extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'categories_products';
    protected $fillable = ['category_id','product_id'];

}
