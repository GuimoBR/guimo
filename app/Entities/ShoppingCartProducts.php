<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ShoppingCartProducts extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'shopping_cart_products';
    protected $fillable = ['shopping_cart_id','product_id'];

}
