{{csrf_field()}}

<div class="row">
    <div class="form-group @if($errors->has('name')) has-error @endif">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label for="name" class="control-label">Nome do App:</label>
            <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}">
            @if($errors->has('name')) <span id="helpBlock2" class="help-block">{{$errors->first('name')}}</span>@endif
        </div>
    </div>
</div>


<div class="row">
    <div class="form-group">
        <div @if($errors->has('version'))class="has-error" @endif>
            <div class="col-md-6">
                <label for="version" class="contro-label">Versão</label>
                <input type="text" name="version" id="version" class="form-control" value="{{old('version')}}">
                @if($errors->has('name')) <span id="helpBlock2" class="help-block">{{$errors->first('version')}}</span>@endif
            </div>
        </div>

        <div @if($errors->has('filename'))class="has-error" @endif>
            <div class="col-md-6">
                <label for="filename" class="contro-label">Nome do Arquivo</label>
                <input type="text" name="filename" id="filename" class="form-control col-md-3" value="{{old('filename')}}" readonly>
                @if($errors->has('name')) <span id="helpBlock2" class="help-block">{{$errors->first('filename')}}</span>@endif
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">

        <div class="col-md-6">
            <label for="description" class="contro-label">Descrição resumida do app</label>
            <input type="text" name="description" id="description" class="form-control" value="{{old('version')}}">
        </div>

        <div>
            <div class="col-md-6">
                <label for="target_os" class="contro-label">Sistema Operacional</label>
                <select name="target_os" id="target_os" class="form-control">
                    <option value="android">Android</option>
                    <option value="ios">iOS</option>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-md-12">
            <label for="changelog" class="control-label">Descrição detalhada da mudança</label>
            <textarea name="changelog" id="changelog" class="form-control" rows="5"></textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-12">
            <div class="row">
                <div class="text-center" id="txtfeedback">
                    Fazendo upload...
                </div>
            </div>
            <div class="row">
                <div class="progress" id="pbardiv">
                    <div class="progress-bar progress-bar-accent progress-bar-striped active" id="pbar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%">
                        40%
                    </div>
                </div>
            </div>
            <label for="uploadfile">Arquivo</label>
            <input type="file" name="uploadfile" id="uploadfile" class="form-control">
        </div>
    </div>
</div>

