<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProductImage extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'product_images';
    protected $fillable = ['product_id','file_name'];
    public $timestamps = false;

    public function product(){
        return $this->belongsTo(Product::class);
    }

}
