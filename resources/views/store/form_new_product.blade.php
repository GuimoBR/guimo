{{csrf_field()}}
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="name">Nome do produto</label>
            <input type="text" class="form-control" name="name"  id="name" value="@if(!is_null($product)){{$product->name}}@endif">

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="price">Preço do Produto</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-usd fa-lg"></i></span>
                <div class="input-group-content">

                    <input type="text" class="form-control" name="price" id="price" value="@if(!is_null($product)){{$product->price}}@endif">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="discount_percent">Desconto</label>
            <div class="input-group">
                <div class="input-group-content">

                    <input type="number" class="form-control" name="discount_percent" min="0" step="1" id="discount_percent" value="@if(!is_null($product)){{$product->discount_percent}}@endif">
                </div>
                <span class="input-group-addon"><i class="fa fa-percent fa-lg"></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="final_price">Preço Final</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-usd fa-lg"></i></span>
                <div class="input-group-content">
                    <input type="text" class="form-control" name="final_price" id="final_price">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label class="control-label">Produto Disponível?</label>
            <div class="col-sm-12">
                <label class="radio-inline radio-styled">
                    <input type="radio" name="is_avaible" id="is_avaible" value="1" @if(!is_null($product)) @if($product->is_avaible) checked="" @endif @endif required><span>Sim</span>
                </label>
                <label class="radio-inline radio-styled">
                    <input type="radio" name="is_avaible" id="is_avaible" value="0" @if(!is_null($product)) @if(!$product->is_avaible) checked="" @endif @endif><span>Não</span>
                </label>
            </div>
        </div>
    </div>


    <div class="col-md-3">
        <div class="form-group">
            <label for="stock">Estoque</label>
            <input type="number" class="form-control" name="stock"  min="0" step="1" id="stock"  value="@if(!is_null($product)){{$product->stock}}@endif">

        </div>
    </div>
    @if(is_null($product))
    <div class="col-md-6">
        <div class="form-group">
            <label for="stock">Imagem destaque (320x150)</label>
            <input type="file" class="form-control" name="thumb_image" required>

        </div>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="category" class="control-label">Categoria do Produto:</label>
            <div class="col-sm-12">
                @foreach($categories as $category)
                    <label class="checkbox-inline checkbox-styled">
                        <input type="checkbox" name="categories[]" value="{{$category->id}}" class="flat" @if(!is_null($product)) @foreach($product->categories as $pcategory) @if($pcategory->id == $category->id) checked @endif @endforeach @endif> {{$category->name}} &nbsp;
                    </label>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <br>
        <div class="form-group">
            <label for="description">Descrição </label>
            <textarea name="description" class=" form-control input-sm" id="description" cols="30" rows="5">@if(!is_null($product)){!! $product->description !!}@endif</textarea>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="slug">URL do Produto</label>
            <input type="text" class="form-control input-sm" name="slug" id="slug" placeholder="novo-produto-x" value="@if(!is_null($product)){{$product->slug}}@endif" readonly>

        </div>
    </div>
</div>
<hr>
<p class="text-black"><span class="text-xl">Imagens</span> <span class="text-sm">máximo 5 com formato jpg/jpeg</span></p>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <input type="file" class="form-control input-sm" name="images[]" id="images" multiple="multiple">
        </div>
    </div>
</div>