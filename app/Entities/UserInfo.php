<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserInfo extends Model implements Transformable
{
    use TransformableTrait;
    protected  $table = 'users_info';

    protected $fillable = [
        'user_id',
        'country_id',
        'birthdate',
        'passport',
        'RG',
        'CPF',
        'CEP',
        'address',
        'number',
        'adjunct',
        'neighborhood',
        'city',
        'uploads'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

}
