<!DOCTYPE html>
<html>
<head>
    <title>Guimo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type='image/x-icon' href="{{asset('assets/images/ico/favicon.ico')}}">


    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/css/bootstrap-material-design.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/css/ripples.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/toastr/toastr.css')}}" rel="stylesheet" type="text/css" />


</head>
<body style="padding-top: 70px;">
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/images/logo_guimo_50px.png')}}" alt="Guimo" style="margin-top:-13px;"> </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{url('/')}}" class="navbar-brand">Guimo</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<div class="container-fluid">

    @if(!$is_valid)

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-danger">
                    <strong>Atenção!</strong><br>
                    Seu link para cadastro já foi acessado ou não existe. Por favor, entre em contato com algum dos administradores do site através do seguinte email: <strong>jorgejunior@guimo.toys</strong>
                </div>
            </div>
        </div>

    @else
        @if($is_valid && isset($message))
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-success">
                        <strong>Parabéns!</strong><br>
                        {!! $message !!}
                    </div>
                </div>
            </div>
        @else
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro de novo administrador</h3>
                    </div>
                    <div class="panel-body">
                        <p class="lead">Dados Gerais</p>
                        <form action="{{route('user.storeWithToken',['token'=>$token])}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('name')) has-error @endif">
                                    <label class="control-label" for="name">Nome</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                    @if($errors->has('name')) <p class="help-block">{{$errors->first('name')}}</p> @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('email')) has-error @endif">
                                    <label class="control-label" for="email">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}" required>
                                    @if($errors->has('email')) <p class="help-block">{{$errors->first('email')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('password')) has-error @endif">
                                    <label class="control-label" for="password">Senha</label>
                                    <input type="password" class="form-control" name="password" required>
                                    @if($errors->has('password')) <p class="help-block">{{$errors->first('password')}}</p> @endif
                                </div>
                            </div>
                            <hr>
                            <p class="lead">Dados Pessoais</p>

                            <div class="col-md-12">
                                <div class="form-group" @if($errors->has('profile_image')) has-error @endif>
                                    <label class="control-label" for="inputDefault">Imagem de Perfil</label>
                                    <input type="text" readonly="" class="form-control" placeholder="Browse...">
                                    <input type="file" name="profile_image" id="inputFile" value="{{old('profile_image')}}" required>
                                    @if($errors->has('password')) <p class="help-block">{{$errors->first('password')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group @if($errors->has('CPF')) has-error @endif" id="documentDiv">
                                    <label class="control-label" for="CPF">CPF</label>
                                    <input type="text" class="form-control" name="CPF" id="CPF" value="{{old('CPF')}}" data-inputmask="'mask': '999.999.999-99'" required>
                                    <p class="help-block" id="helpBlock2">@if($errors->has('CPF')) {{$errors->first('CPF')}} @endif</p>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group @if($errors->has('RG')) has-error @endif">
                                    <label class="control-label" for="RG">RG</label>
                                    <input type="text" class="form-control" name="RG" value="{{old('RG')}}" required>
                                    @if($errors->has('RG')) <p class="help-block">{{$errors->first('RG')}}</p> @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="passport">Passaporte</label>
                                    <input type="text" class="form-control" value="{{old('passport')}}" name="passport">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group @if($errors->has('country_id')) has-error @endif">
                                    <label class="control-label" for="inputDefault">País</label>
                                    <select name="country_id" id="country" class="form-control" required>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if(old('country_id') == $country->id) selected @endif>{{$country->name   }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('country_id')) <p class="help-block">{{$errors->first('country_id')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group @if($errors->has('birthdate')) has-error @endif">
                                    <label class="control-label" for="birthdate">Data de Nascimento</label>
                                    <input type="text" class="form-control" name="birthdate" value="{{old('birthdate')}}" data-inputmask="'mask': '99/99/9999'" required>
                                    @if($errors->has('birthdate')) <p class="help-block">{{$errors->first('birthdate')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label @if($errors->has('birthcity')) has-error @endif" for="birthcity" >Cidade de Nascimento</label>
                                    <input type="text" class="form-control" name="birthcity" value="{{old('birthcity')}}" required>
                                    @if($errors->has('birthcity')) <p class="help-block">{{$errors->first('birthcity')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-4 @if($errors->has('CEP')) has-error @endif">

                                <label class="control-label">CEP <small>onde você mora?</small></label>
                                <div class="input-group">
                                    <input type="text" name="CEP" id="CEP" value="{{old('CEP')}}" class="form-control">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-fab btn-fab-mini" id="searchCep">
                                          <i class="material-icons">search</i>
                                      </button>
                                    </span>
                                </div>
                                @if($errors->has('CEP')) <p class="help-block">{{$errors->first('CEP')}}</p> @endif
                            </div>

                            <div class="col-md-8">
                                <div class="form-group @if($errors->has('city')) has-error @endif">
                                    <label class="control-label" for="city">Cidade</label>
                                    <input type="text" class="form-control" name="city" value="{{old('city')}}" id="city" required>
                                    @if($errors->has('city')) <p class="help-block">{{$errors->first('city')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group @if($errors->has('address')) has-error @endif">
                                    <label class="control-label" for="address">Endereço</label>
                                    <input type="text" class="form-control" name="address" value="{{old('address')}}" id="address" required>
                                    @if($errors->has('address')) <p class="help-block">{{$errors->first('address')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group @if($errors->has('number')) has-error @endif">
                                    <label class="control-label" for="number">Número</label>
                                    <input type="text" class="form-control" name="number" value="{{old('number')}}" required>
                                    @if($errors->has('number')) <p class="help-block">{{$errors->first('number')}}</p> @endif
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="number">Complemento</label>
                                    <input type="text" class="form-control" value="{{old('adjunct')}}" name="adjunct">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group @if($errors->has('neighborhood')) has-error @endif">
                                    <label class="control-label" for="neighborhood">Bairro</label>
                                    <input type="text" class="form-control" name="neighborhood" value="{{old('neighborhood')}}" id="neighborhood" required>
                                    @if($errors->has('neighborhood')) <p class="help-block">{{$errors->first('neighborhood')}}</p> @endif
                                </div>
                            </div>
                            <br>
                            <div class="col-md-12">
                                <div class="alert alert-warning">
                                    <strong>Atenção!</strong><br>
                                    Após concluir o cadastro, você deve finaliza-lo fazendo seu login e acessendo a seção <strong>Meu Perfil</strong> na dashboard!<br>
                                    <strong>É muito importante você fazer o upload dos seus documentos na mesma seção acima, na aba Uploads!</strong>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-raised btn-block btn-info btn-lg">Cadastrar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            @endif
    @endif



</div>


<!-- /.container -->

<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/material.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootstrap.material-design/0.5.8/js/ripples.min.js"></script>
<script src="{{asset('materialadmin/assets/js/libs/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/js/input_mask/jquery.inputmask.js')}}"></script>

<script>
    $.material.init();
</script>
<script>
    $(document).ready(function(){
        $(":input").inputmask();
        $("#searchCep").click(function(){
            var cep = $("#CEP");
            toastr.options = {
                "closeButton":true,
                "progressBar":true,
                "positionClass":'toast-top-right',
                "showMethod":'fadeIn',
                "hideMethod":'fadeOut'
            };


            if(cep.val() != ''){
                $("#searchCep").attr('disabled',true);
                $("#searchCep").html('<i class="fa fa-spin fa-spinner"></i>');
                $.ajax({
                    url:'{{url('/')}}/api_v1/correios/consultaCep/'+cep.val()+'',
                    method:'GET',
                }).done(function(response){
                    if(!$.isEmptyObject(response)){
                        $("#address").val(response.logradouro);
                        $("#neighborhood").val(response.bairro);
                        $("#city").val(response.cidade+' - '+response.uf)
                    }else{
                        toastr.error("Atenção, nenhum endereço foi encontrado para este CEP, por favor, tente novamente!",'CEP');
                    }
                    $("#searchCep").attr('disabled',false);
                    $("#searchCep").html('<i class="fa fa-search"></i>');
                })
            }
        });

        $("#CEP").focusout(function(){
            var cep = $("#CEP");
            toastr.options = {
                "closeButton":true,
                "progressBar":true,
                "positionClass":'toast-top-right',
                "showMethod":'fadeIn',
                "hideMethod":'fadeOut'
            };
            if(cep.val() != ''){
                $.ajax({
                    url:'{{url('/')}}/api_v1/correios/consultaCep/'+cep.val()+'',
                    method:'GET',
                }).done(function(response){
                    if(!$.isEmptyObject(response)){
                        $("#address").val(response.logradouro);
                        $("#neighborhood").val(response.bairro);
                        $("#city").val(response.cidade+' - '+response.uf)
                    }else{
                        toastr.error("Atenção, nenhum endereço foi encontrado para este CEP, por favor, tente novamente!",'CEP');
                    }
                })
            }
        });

        var documentCPF = $("#CPF").val();
        form_data = {
            cpf: documentCPF
        };

        if(documentCPF != "") {

            $.ajax({
                url: '{{url('/')}}/api_v1/validate/cpf',
                method: 'POST',
                data: form_data
            }).done(function (response) {
                if (response.is_valid) {
                    $("#documentDiv").removeClass('has-error');
                    $("#documentDiv").addClass('has-success');
                    $("#helpBlock2").fadeOut(150);
                    $("#submitPersonal").attr('disabled', false);
                } else {
                    $("#documentDiv").removeClass('has-success');
                    $("#documentDiv").addClass('has-error');
                    $("#helpBlock2").html("CPF Inválido, por favor, tente novamente");
                    $("#helpBlock2").fadeIn(100);
                    $("#submitPersonal").attr('disabled', true);
                }
            });
        }

        $("#CPF").focusout(function(){
            documentCPF = $("#CPF").val();
            form_data = {
                cpf: documentCPF
            };
            $.ajax({
                url:'{{url('/')}}/api_v1/validate/cpf',
                method:'POST',
                data:form_data
            }).done(function(response){
                if(response.is_valid){
                    $("#documentDiv").removeClass('has-error');
                    $("#documentDiv").addClass('has-success');
                    $("#helpBlock2").fadeOut(150);
                    $("#submitPersonal").attr('disabled',false);
                }else{
                    $("#documentDiv").removeClass('has-success');
                    $("#documentDiv").addClass('has-error');
                    $("#helpBlock2").html("CPF Inválido, por favor, tente novamente");
                    $("#helpBlock2").fadeIn(100);
                    $("#submitPersonal").attr('disabled',true);
                }
            });
        });
    })
</script>


</body>
</html>