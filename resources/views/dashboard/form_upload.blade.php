{{csrf_field()}}
<div class="row">
    <div class="form-group">
        <div class="col-md-12">
            <button type="button" id="newupload" class="btn btn-accent ">Adicionar arquivo. <i class="fa fa-plus-circle"></i></button>
        </div>
    </div>
</div>
<div class="row">
    <div class='form-group'>
        <div class='col-md-12'>
            <label for="profile_image" class="control-label">Imagem de Perfil</label>
            <input type='file' name='profile_image' class='form-control'>
        </div>
    </div>
</div>
<hr>
<div id="upinputs">

</div>

<div class="row">
    <div class="col-md-3 col-md-offset-5">
        <button id="submitupload" class="btn btn-success ink-reaction btn-lg" type="submit" >Enviar Arquivos <i class="fa fa-upload"></i></button>
    </div>
</div>