@extends('layouts.dashboard2')
@section('style')
    <link href="{{asset('materialadmin/assets/css/theme-1/libs/DataTables/jquery.dataTables.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('assets/css/icheck/flat/blue.css')}}">
    <style>
        .hide-row{
            padding: 0 !important;
        }
    </style>
@endsection

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Versões do App para android até o momento!</h2>
        </div>
        <div class="section-body">
            @if(is_bool(session('destroy')))
                <div class="row">
                    <div class="col-md-12">
                        @if(!session('destroy'))

                            <div class="alert alert-success text-center">
                                {{session('message')}}
                            </div>

                        @else
                            <div class="alert alert-danger text-center">
                                {{session('message')}}
                            </div>
                    @endif
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card card-bordered style-primary">
                        <div class="card-head">
                            <header>Status</header>
                            <div class="tools">
                                <div class="btn-group">
                                    <a href="#" class="btn btn-accent" id="newversion">Nova Versão <i class="fa fa-plus-circle"></i></a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="card-body style-default-bright">
                            <div class="table-responsive">
                                <table class="table table-stripped responsive-utilities" id="apps_table">
                                    <thead>
                                    <tr class="headings">
                                        <th class="text-center">App</th>
                                        <th class="text-center">Versão</th>
                                        <th class="text-center">Descrição</th>
                                        <th class="text-center">Data do Upload</th>
                                        <th class="text-center" colspan="3">Ações</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    @forelse($apps as $appguimo)
                                        <tr>
                                            <input type="hidden" data-appname="{{$appguimo->name}}" id="{{$appguimo->id}}" value="{{nl2br($appguimo->changelog)}}">
                                            <td class="text-center">{{$appguimo->name}}</td>
                                            <td class="text-center">{{$appguimo->version}}</td>
                                            <td class="text-center">{{$appguimo->description}}</td>
                                            <td class="text-center">{{$appguimo->created_at->format('d/m/Y H:i:s')}}</td>
                                            <td width="50" class="text-center">
                                                <form action="{{route('apps.deleteandroid',['id'=>$appguimo->id])}}" method="post">
                                                    {{method_field("delete")}}
                                                    {{csrf_field()}}
                                                    <button type="submit" class="btn btn-floating-action ink-reaction btn-danger" data-toggle="tooltip" data-placement="top" data-original-title="Excluir"><i class="fa fa-close"></i></button>
                                                </form>
                                            </td>
                                            <td width="50" class="text-center">
                                                <a href="{{url("/app/$appguimo->version/$appguimo->filename")}}" class="btn btn-floating-action ink-reaction btn-info" data-toggle="tooltip" data-placement="top" data-original-title="Baixar"> <i class="fa fa-download"></i></a>
                                            </td>
                                            <td width="50" class="text-center">
                                                <a href="#" class="btn btn-floating-action ink-reaction btn-warning" id="moreinfo" data-appid="{{$appguimo->id}}" data-toggle="tooltip" data-placement="top" data-original-title="Informações Adicionais"> <i class="fa fa-info-circle"></i></a>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr class="info"><td colspan="5" class="text-center text-lg">Nenhum app para android foi encontrado.</td></tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalnewversion" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Enviar nova versão para teste</h4>
                </div>
                <form action="{{route('apps.androidupload')}}" enctype="multipart/form-data" onsubmit="return false;"  method="POST" id="formUpload" class="form" role="form">
                    <div class="modal-body">
                        <div id="output"></div>
                        @include('dashboard.form_upload_app')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary" id="submit">Submeter Versão</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalchangelog" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="changelogtitle">ChangeLog de:</h4>
                </div>
                <div class="modal-body" id="changelogbody">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Fechar</button>
                </div>

            </div>
        </div>
    </div>


@endsection
        @section('script')
            <script src="{{asset('assets/js/string_to_slug_apk.js')}}"></script>
            <script src="{{asset('materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('v2/js/jquery.form.js')}}"></script>
            <script>
                $(document).ready(function(){

                    var submit = $("#submit");
                    var close  = $("#close");
                    var txtfeedback = $("#txtfeedback").hide();
                    var pbardiv = $("#pbardiv");
                    var pbar = $("#pbar");
                    pbardiv.hide();
                    $("#formUpload").submit(function(){
                        var options = {
                            target:'#output',
                            beforeSubmit: beforeSubmit,
                            uploadProgress:OnProgress,
                            success:afterSuccess,
                            resetForm:true
                        };

                        $(this).ajaxSubmit(options);
                    });

                    function beforeSubmit(){
                        submit.html("<i class='fa fa-spinner fa-spin'></i> Trabalhando...");
                        submit.attr('disabled',true);
                        close.hide();
                        txtfeedback.html("Trabalhando no upload....");
                        txtfeedback.fadeIn();
                    }

                    function OnProgress(event, position, total, percentComplete){
                        pbardiv.fadeIn();
                        pbar.attr('aria-valuenow',percentComplete).css('width',percentComplete+"%").html(percentComplete+"%");

                    }

                    function afterSuccess(response){

                        submit.html("Submeter Versão");
                        submit.attr('disabled',false);
                        close.fadeIn();
                        if(response.error){
                            txtfeedback.removeClass("text-success");
                            txtfeedback.addClass("text-danger");
                            txtfeedback.html(response.message);
                        }else{
                            txtfeedback.removeClass("text-danger");
                            txtfeedback.addClass("text-success");
                            txtfeedback.html(response.message);
                        }

                    }


                    $("#name").change(function(){
                        var version = $("#version").val();
                        $("#filename").val(url_slug($(this).val())+"_"+url_slug(version)+".apk");
                    });

                    $("#name").keyup(function(){
                        var version = $("#version").val();
                        $("#filename").val(url_slug($(this).val())+"_"+url_slug(version)+".apk");
                    });

                    $("#version").change(function(){
                        var name = $("#name").val();
                        $("#filename").val(url_slug(name)+"_"+url_slug($(this).val())+".apk");
                    });

                    $("#version").keyup(function(){
                        var name = $("#name").val();
                        $("#filename").val(url_slug(name)+"_"+url_slug($(this).val())+".apk");
                    });

                    $("#newversion").click(function(){
                        $("#modalnewversion").modal('show');
                    });

                    $("a#moreinfo").click(function(){
                        var id = $(this).data('appid');
                        var changelog = $('#'+id);
                        var name = changelog.data('appname');
                        $("#changelogtitle").html("ChangeLog de "+name);
                        $("#changelogbody").html(changelog.val());
                        $("#modalchangelog").modal('show');
                    });


                    @if(count($apps)>0)
                    var oTable = $('#apps_table').dataTable({
                        "dom": 'T<"clear">lfrtip',
                        "order": [[0, 'asc']],
                        "colVis": {
                            "buttonText": "Columns",
                            "overlayFade": 0,
                            "align": "right"
                        },
                         'columns':[{
                             "orderable": true
                         }, {
                             "orderable": true
                         }, {
                             "orderable": true
                         }, {
                             "orderable": true
                         },
                             {
                             "orderable": false
                         }, {
                             "orderable": false
                         }, {
                             "orderable": false
                         }],
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Portuguese-Brasil.json",
                            "lengthMenu": '_MENU_ entries per page',
                            "search": '<i class="fa fa-search"></i>',
                            "paginate": {
                                "previous": '<i class="fa fa-angle-left"></i>',
                                "next": '<i class="fa fa-angle-right"></i>'
                            }
                        }

                    });
                    @endif


        });
        $('.collapse').on('show.bs.collapse', function () {
            $('.collapse.in').collapse('hide');
        });
    </script>
    @endsection