<!DOCTYPE html>
<html ng-app="guimoApp">
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="description" content="Guimo é o smart toy que veio para revolucionar. Conheça agora mesmo o brinquedo mais aguardado do ano!">
    <meta name="abstract" content="Guimo o smart toy revolucionario e educativo que você sempre quis ter">
    <meta name="keywords" content="smart,toy,guimo,smartoy,smart-toy,startup,revolucionar,startup,brinquedo,criança,child,inteligente,kids,educativo,divertido">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Guimo Toy</title>

    <!-- Bootstrap core CSS -->
    <link rel="shortcut icon" type='image/x-icon' href="{{asset('assets/images/ico/favicon.ico')}}">
    <link href="{{asset('assets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Startup CSS -->
    <link href="{{asset('assets/css/guimo.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">

    <link rel="dns-prefetch" href="http://guimo.toys/"/>
    <link rel="alternate" href="http://guimo.toys" hreflang="pt-br" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{asset('assets/js/html5shiv.js')}}"></script>
    <script src="{{asset('assets/js/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Favicons -->

    <!-- Custom Google Font : Open Sans -->

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="preloader"></div>
<!-- Parallax Header Starts -->
<main id="top" class="masthead" role="main">
    <div class="container">
        <!-- Startup Logo -->
        <div class="logo"> <a href="/"><img src="{{asset('assets/images/logo_guimo_black320.png')}}" alt="startup-logo"></a> </div>
        <!-- Hero Title -->
        <h1>O <strong>Smart Toy</strong> que veio para <span data-typer-targets="divertir,ensinar,revolucionar,ficar">ensinar</span></h1>
        <!-- Mailchimp Subscribe form -->
        <div class="row">
            <div class="col-md-6 col-sm-12 col-md-offset-3 subscribe">
                <form class="form-horizontal" name="emailForm">
                    <div class="form-group">
                        <div class="col-md-9 col-sm-6 col-sm-offset-1 col-md-offset-0">

                            <input class="form-control input-lg" name="email" id="email" type="email" placeholder="Ficou interessado? Deixe seu email conosco..." required="required">
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <button type="button" class="btn btn-success btn-lg"  id="enviar">Enviar</button>
                        </div>
                    </div>
                </form>
                <span id="message" class="alertMsg">

                </span>

            </div>
        </div>
    </div>
</main>
<!-- // Parallax Header Ends -->
<!-- Container -->
<div class="container">
    <div class="section-title">
        <h2>O que é Smart Toy?</h2>
        <!--<h4>Simple, elegant and intutive user interface</h4>-->
    </div>
    <section class="row breath">
        <div class="col-md-12 text-center">
            <p class="whatis">Smart Toys consistem em dois elementos interconectados: o video game (ou um aplicativo) e um ou mais brinquedos conectados. Os Smart Toys agora constituem um novo segmento de mercado que está entre os video games e a indústria de brinquedo.</p>
        </div>
    </section>
</div>
<hr>
<div class="container" id="explore">
    <!-- Hero Image -->
    <div class="section-title">
        <h2>Conheça o Guimo</h2>
        <!--<h4>Simple, elegant and intutive user interface</h4>-->
    </div>
    <section class="row heroimg breath">
        <div class="col-md-12 text-center thumbnail"> <img src="{{asset('assets/images/guimo_prototipo_resized.jpg')}}" alt="flat-mockup-template" ></div>
    </section>
    <p class="text-center"><small>Protótipo v2 - Não consiste no produto final</small></p>
    <!-- //Hero Image -->

    <!-- Features Section -->
    <div class="section-title">
        <h2>Suas principais features</h2>
        <!--<h4>With our advanced features, you'll fall in love with</h4>-->
    </div>
    <section class="row features">
        <!-- Feature 01 -->
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail"> <img src="{{asset('assets/images/modular_feature_resized.jpg')}}" class="img-circle" alt="modular-feature">
                <div class="caption">
                    <h3>Modular</h3>
                    <p>É possivel ter um brinquedo diferente apenas trocando alguma peça, por meio da modularidade, sua imaginação é o limite do Guimo! </p>
                </div>
            </div>
        </div>

        <!-- Feature 02 -->
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail"> <img src="{{asset('assets/images/game_feature_resized.jpg')}}" class="img-circle" alt="game-feature">
                <div class="caption">
                    <h3>Jogos</h3>
                    <p>Tenha jogos super divertidos e interativos em seu smartphone e tablet. Conecte seu Guimo e o coloque para brincar! </p>
                </div>
            </div>
        </div>

        <!-- Feature 03 -->
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail"> <img src="{{asset('assets/images/learning_feature_resized.jpg')}}" class="img-circle" alt="learning-feature">
                <div class="caption">
                    <h3>Aprendizado</h3>
                    <p>Com o Guimo você pode se divertir enquanto aprende sobre hardware e programação! </p>
                </div>
            </div>
        </div>

    </section>
    <div class="section-title">
        <h2>Guimo na mídia</h2>
        <h4>Veja aonde o guimo já foi destaque! </h4>
    </div>
    <section class="row clientlogo breath">
        <div class="col-md-12 text-center thumbnail">
            <a href="http://www.techtudo.com.br/noticias/noticia/2016/01/techtudo-recebe-campuseiros-criadores-de-apps-na-campus-party-2016.html" target="_blank"><img src="{{asset("assets/images/TechTudo50.png")}}" alt="midia-logos"></a>
        </div>
    </section>

</div>
<!-- // Container Ends -->

<!-- Grey Highlight Section -->
<div class="highlight testimonials">
    <div class="container">

        <!-- Testimonials -->
        <div class="section-title">
            <h2>O que os nossos usuários dizem sobre o guimo</h2>
            <!--<h4>Don't take our word. See our testimonials </h4>-->
        </div>
        <section class="row breath">
            <div class="col-md-6">
                <div class="testblock">Adorei brincar com o Guimo! Com ele os jogos ficaram muito mais divertidos e legais. Meus amigos sempre me pedem para brincar com ele também!</div>
                <div class="clientblock">
                    <p><strong>Matheus Couto</strong> <br>
                        9 anos </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="testblock">Com o guimo, meu filho passou a interagir mais, se divertir e ainda aprender assuntos que acho muito importante nos dias de hoje, como hardware e programação.</div>
                <div class="clientblock">
                    <p><strong>Celso Delgado</strong> <br>
                        62 anos, pai de Rafael Delgado, 8 anos.</p>
                </div>
            </div>
        </section>
        <!-- // End Testimonials -->

    </div>
</div>
<!-- // End Grey Highlight Section -->

<!-- Container -->
<div class="container">



    <!-- FAQ -->
    <div class="section-title">
        <h5>Perguntas frequentes (FAQ)</h5>
    </div>
    <section class="row faq breath">
        <div class="col-md-6">
            <h6>O que é o Guimo?</h6>
            <p>Guimo é um smart toy modular e interativo que veio mudar a maneira de brincar</p>
            <!--<h6>Como faço para adquirir um?</h6>
            <p>Nosso produto está em fase de desenvolvimento, mas logo estará disponível. Deixe seu email conosco e seja um dos primeiros a saber sobre seu lançamento</p>-->
        </div>
        <div class="col-md-6">
            <h6>Como faço para adquirir um?</h6>
            <p>Estamos desenvolvendo nosso produto final para comercialização e logo estará disponivel para todos. Deixe seu email conosco e seja um dos primeiros a saber sobre seu lançamento</p>
            <!--<h6>What payment types do you accept?</h6>
            <p>We accept payments from MasterCard, Visa, Visa Debit and American Express. We do not accept PayPal. Remember, you do not need to supply card details to start your free trial.</p>-->
        </div>
    </section>
    <!-- // End FAQ -->

</div>
<!-- // Container Ends -->

<!-- Parallax Footer Starts -->
<main class="footercta" role="main">
    <div class="container">

        <!-- Hero Title -->
        <!-- Mailchimp Subscribe form -->
        <div class="row">
            <div class="col-md-6">
                <h2><strong>Entre em contato:</strong></h2>
                <p class="footerlinks"><a href="mailto:contato@guimo.toys">contato@guimo.toys</a></p>
                <div class="col-md-4">
                    <div class="fb-like" data-href="https://www.facebook.com/guimotoy" data-layout="button_count" data-kid-directed-site="true" data-share="true"></div>
                </div>
                <div class="col-md-6">
                    <g:plusone size="standart" href="https://plus.google.com/114992593692098714096" annotation="bubble"></g:plusone>
                    <g:follow href="https://plus.google.com/114992593692098714096" rel="author"></g:follow>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <h3> Nascido no</h3>
                </div>
                <div class="row">
                    <img src="{{asset('assets/images/logo_ceu_branco.png')}}" alt="CEU" width="150">
                </div>
            </div>
        </div>
    </div>
</main>
<!-- // Parallax Footer Ends -->

<!-- Container -->
<div class="container">
    <section class="row breath">
        <div class="col-md-12 footerlinks"> <!--<a href="#">About</a> <a href="#">Pricing</a> <a href="blog.html">Blog</a> <a href="#">Help</a> <a href="#">Contact</a> <a href="#">Privacy</a> <a href="#">Terms</a>-->
            <p>&copy; {{date('Y')}} Guimo. All Rights Reserved</p>
        </div>
    </section>
    <!-- // End Client Logos -->

</div>
<!-- // Container Ends -->


<!-- JS and analytics only. -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<div id="fb-root"></div>

<script src="{{asset('assets/js/pace.js')}}"></script>
<script src="{{asset('assets/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/typer.js')}}"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-70734600-1', 'auto');
    ga('send', 'pageview');

</script>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
    $(document).ready(function(){
        $("#message").hide();
        $('[data-typer-targets]').typer();

        $("#enviar").click(function(){
            if($("#email").val() != '') {
                $("#enviar").attr("disabled",true);
                $("#enviar").html("Enviando... <i class='fa fa-spinner fa-spin'></i>").button("refresh");

                var form_data = {
                    email: $("#email").val(),
                    status: 1
                };
                $.ajax({
                    url: '{{url('/')}}/api_v1/emails',
                    method: 'POST',
                    data: form_data
                }).success(function (response) {
                    console.log(response);
                    if (response.success) {
                        $("#message").html(response.message);
                        $("#message").fadeIn();
                        $("#message").delay(2200).fadeOut();

                    }
                    if(response.error){
                        $("#message").html("Não foi possivel cadastrar o email informado, por favor, tente novamente.");
                        $("#message").fadeIn();
                        $("#message").delay(2200).fadeOut();
                    }
                }).then(function(){
                    $("#enviar").attr("disabled",false);
                    $("#enviar").html("Enviar").button("refresh");
                });

            }else{
                $("#message").html("O email não pode ficar em branco...");
                $("#message").fadeIn();
                $("#message").delay(2200).fadeOut();
            }
        });
    })
</script>

</body>
</html>
