<?php

namespace Guimo\Entities;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    //
    protected  $table = 'emails';

    protected $fillable = [
        'name',
        'from',
        'subject',
        'type',
        'status',
        'body',
        'obs'
    ];
    //protected $hidden = ['status'];
}
