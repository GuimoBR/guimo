@extends('layouts.dashboard2')

@section('content')
    <section>
        <div class="section-header">
            <h2 class="text-primary">Perfil de {{$profile->name}}</h2>
        </div>

    @if($errors->has())
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-callout alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p><strong>Atenção, alguns errros foram detectados:</strong></p>
                    <p>Por favor, verifique todos os campos que estão destacados e tente novamente!</p>
                </div>
            </div>
        </div>
    @endif
    @if(Session::has('message'))
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-callout alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p>{{Session::get('message')}}</p>
                </div>
            </div>
        </div>
    @endif

        <div class="row">
            <!-- BEGIN LAYOUT LEFT ALIGNED -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head">
                        <ul class="nav nav-tabs" data-toggle="tabs">
                            <li @if(!$errors->has()) class="active" @endif><a href="#dados"><i class="fa fa-info-circle" aria-expanded="true"></i> Dados</a></li>

                            @if($user->id == $profile->id)
                                <li @if($errors->has()) class="active" @endif><a href="#settings"><i class="fa fa-cog"></i> Configurações</a></li>
                                @if($user->privileges >= 10)
                                    <li><a href="#uploads"><i class="fa fa-upload"></i> Uploads</a></li>
                                @endif
                            @endif
                            @if($user->privileges >= 10)
                                <li><a href="#important"><i class="fa fa-exclamation-circle"></i> Informações importantes</a></li>
                                @endif
                        </ul>
                    </div><!--end .card-head -->
                    <div class="card-body tab-content">
                        <div class="tab-pane @if(!$errors->has()) active @endif" id="dados">
                            <p class="lead">Dados de cadastro</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2" style="border-right: 1px solid #000">
                                            @if(is_null($profile->info->profile_image))
                                                <img src="{{url('/')}}/files/{{$profile->id}}/default.png" alt="Perfil" height="150" class="img-circle card-outlined style-primary">
                                                @else
                                                <img src="{{url('/')}}/files/{{$profile->id}}/{{$profile->info->profile_image}}" alt="Perfil" width="150" class="img-circle card-outlined style-primary">
                                            @endif
                                        </div>
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-lg">
                                                        <strong>Nome: </strong>{{$profile->name}}
                                                    </p>
                                                    <p class="text-lg">
                                                        <strong>Email: </strong>{{$profile->email}}
                                                    </p>
                                                </div>
                                                @if(!is_null($profile->info))
                                                    <div class="col-md-6">
                                                        <p class="text-lg"><strong>País: </strong> {{$profile->info->country->name}}</p>
                                                        <p class="text-lg"><strong>Documento: </strong> {{$profile->info->CPF}}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            @if($user->id == $profile->id)
                                <hr>
                            <div class="row">
                                <div class="col-md-8" id="frete">
                                    <h2>Informações de frete para este endereço <i class="fa fa-truck"></i></h2>
                                    <p>Valores e prazos atualizados de acordo com o serviço de entrega utilizado pelo Guimo.</p>
                                    <div class="col-md-6" id="pac">

                                    </div>
                                    <div class="col-md-6" id="sedex">

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <p class="text-lg"><strong>Meu Endereço: <small>(aproximado)</small></strong></p>
                                    <div id="basic-map" class="border-gray height-10"></div>
                                </div>
                            </div>
                            @endif
                        </div>

                        @if($user->id == $profile->id)
                        <div class="tab-pane @if($errors->has()) active @endif" id="settings">
                            <p class="lead">Alteração dos dados</p>
                            <form class="form-horizontal form-label-left" id="form_general" method="POST" action="{{route('profile.update_general',['id'=>$profile->id])}}">
                                @include('dashboard.form_update_general')
                            </form>
                            <hr>
                            <p class="lead">Dados Pessoais</p>
                            <form class="form-horizontal form-label-left" id="form_personal" method="POST" action="{{route('profile.update_personal',['id'=>$profile->id])}}">
                                @include('dashboard.form_update_personal')
                            </form>
                        </div>
                        @endif
                        @if($user->privileges >= 10)
                            @if($user->id == $profile->id)
                            <div class="tab-pane" id="uploads">
                                <p class="lead">Uploads <small class="text-xs">- Tamanho máximo 1MB</small> </p>
                                <p>Clique no botão abaixo para adicionar a quantidade de arquivos que você deseja enviar.</p>
                                <form class="form-horizontal form-label-left" id="form_upload" action="{{route('profile.upload_files',['id'=>$profile->id])}}" method="POST" enctype="multipart/form-data" >
                                    @include('dashboard.form_upload')
                                </form>
                            </div>
                            @endif

                            <div class="tab-pane" id="important">
                                <p class="lead">Dados e Informações importantes</p>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <p class="text-lg"><strong>Nome: </strong>{{$profile->name}} </p>
                                        <p class="text-lg"><strong>Data de Nascimento: </strong>{{$profile->info->birthdate}} </p>
                                        <p class="text-lg"><strong>Cidade onde Nasceu: </strong>{{$profile->info->birthcity}} </p>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <p class="text-lg"><strong>RG: </strong>{{$profile->info->RG}} </p>
                                        <p class="text-lg"><strong>CPF: </strong>{{$profile->info->CPF}} </p>
                                        <p class="text-lg"><strong>Passaporte: </strong>{{$profile->info->passport}} </p>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <p class="text-lg"><strong>CEP: </strong>{{$profile->info->CEP}} </p>
                                        <p class="text-lg"><strong>Endereço: </strong>
                                            {{$profile->info->address.' '.$profile->info->number.' '.$profile->info->adjunct.' '.$profile->info->city.' '.$profile->info->country->name}}
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="lead">Bio </p>
                                        {!! $profile->info->bio !!}
                                    </div>
                                </div>
                                @if(!is_null($profile->info->uploads))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="lead">Uploads</p>
                                            @foreach($profile->info->uploads as $upload)
                                                <a href="{{url('/')}}/files/{{$profile->id}}/{{$upload}}" class="label label-info text-lg">{{$upload}}</a>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif

                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
        </div>
    </section>
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyAlzirnhqzFyxiBtT0m1uGHdJo5KpaZ0-Q&amp;sensor=false"></script>
    <script src="{{asset('materialadmin/assets/js/libs/gmaps/gmaps.js')}}"></script>
    <script src="{{asset('assets/js/input_mask/jquery.inputmask.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            CKEDITOR.config.customConfig = '{{asset("assets/ckeditor/configbasic.js")}}';
            CKEDITOR.replace("bio");

            $("#submitPersonal").attr('disabled',true);
            $(":input").inputmask();



            @if($profile->privileges >= 10)
                    documentCPF = $("#CPF").val();
            form_data = {
                cpf: documentCPF
            };

            if(documentCPF != "") {

                $.ajax({
                    url: '{{url('/')}}/api_v1/validate/cpf',
                    method: 'POST',
                    data: form_data
                }).done(function (response) {
                    if (response.is_valid) {
                        $("#documentDiv").removeClass('has-error');
                        $("#documentDiv").addClass('has-success');
                        $("#helpBlock2").fadeOut(150);
                        $("#submitPersonal").attr('disabled', false);
                    } else {
                        $("#documentDiv").removeClass('has-success');
                        $("#documentDiv").addClass('has-error');
                        $("#helpBlock2").html("CPF Inválido, por favor, tente novamente");
                        $("#helpBlock2").fadeIn(100);
                        $("#submitPersonal").attr('disabled', true);
                    }
                });
            }

            $("#CPF").focusout(function(){
                documentCPF = $("#CPF").val();
                form_data = {
                    cpf: documentCPF
                };
                $.ajax({
                    url:'{{url('/')}}/api_v1/validate/cpf',
                    method:'POST',
                    data:form_data
                }).done(function(response){
                    if(response.is_valid){
                        $("#documentDiv").removeClass('has-error');
                        $("#documentDiv").addClass('has-success');
                        $("#helpBlock2").fadeOut(150);
                        $("#submitPersonal").attr('disabled',false);
                    }else{
                        $("#documentDiv").removeClass('has-success');
                        $("#documentDiv").addClass('has-error');
                        $("#helpBlock2").html("CPF Inválido, por favor, tente novamente");
                        $("#helpBlock2").fadeIn(100);
                        $("#submitPersonal").attr('disabled',true);
                    }
                });
            });

            $("#deleteinput").live('click',function(){
                $(this).closest('.row').remove();
            });


            $("#newupload").click(function(){

                $("#upinputs").append("<div class='row'>" +
                        "<div class='form-group'>" +
                            "<div class='col-md-11'>" +
                            "   <input type='file' name='arquivo[]' class='form-control'>" +
                            "</div>" +
                            "<div class='col-md-1'>" +
                            "<button class='btn btn-danger btn-floating-action btn-sm ink-reaction' id='deleteinput' type='button'><i class='fa fa-times'></i></button>" +
                            "</div>" +
                        "</div>" +
                        "</div>");
            });
                    @endif
            var card = $("#frete");
            @if(!is_null($profile->info))
            $.ajax({
                url:'{{url('/')}}/api_v1/correios/consultaPreco/{{$profile->info->CEP}}',
                method:'GET',
                beforeSend:function(){
                    materialadmin.AppCard.addCardLoader(card);
                }
            }).done(function(response){
                $("#pac").html("<p class='text-xl text-bold text-info'> PAC <span class='text-xs'>Correios</span></p>" +
                        "<p class='text-lg'><strong>Preço:</strong> R$ "+response.pac.valor.toFixed(2)+" <strong>Prazo:</strong> "+response.pac.prazo+" dia(s) uteis</p>")
                $("#sedex").html("<p class='text-xl text-bold text-warning'> SEDEX</p>" +
                        "<p class='text-lg'><strong>Preço:</strong> R$ "+response.sedex.valor.toFixed(2)+" <strong>Prazo:</strong> "+response.sedex.prazo+" dia(s) uteis</p>");
                materialadmin.AppCard.removeCardLoader(card);
            });
            @endif

            $("#submitPersonal").click(function(){
                $("#form_personal").submit();
            });

            $("#submitGeneral").click(function(){
                $("#form_general").submit();
            });

            $("#searchCep").click(function(){
                var cep = $("#CEP");
                toastr.options = {
                    "closeButton":true,
                    "progressBar":true,
                    "positionClass":'toast-top-right',
                    "showMethod":'fadeIn',
                    "hideMethod":'fadeOut'
                };

                $("#searchCep").attr('disabled',true);
                $("#searchCep").html('<i class="fa fa-spin fa-spinner"></i>');
                if(cep.val() != ''){
                    $.ajax({
                        url:'{{url('/')}}/api_v1/correios/consultaCep/'+cep.val()+'',
                        method:'GET',
                    }).done(function(response){
                        if(!$.isEmptyObject(response)){
                            $("#address").val(response.logradouro);
                            $("#neighborhood").val(response.bairro);
                            $("#city").val(response.cidade+' - '+response.uf)
                        }else{
                            toastr.error("Atenção, nenhum endereço foi encontrado para este CEP, por favor, tente novamente!",'CEP');
                        }
                        $("#searchCep").attr('disabled',false);
                        $("#searchCep").html('<i class="fa fa-search"></i>');
                    })
                }
            });

            $("#CEP").focusout(function(){
                var cep = $("#CEP");
                toastr.options = {
                    "closeButton":true,
                    "progressBar":true,
                    "positionClass":'toast-top-right',
                    "showMethod":'fadeIn',
                    "hideMethod":'fadeOut'
                };
                if(cep.val() != ''){
                    $.ajax({
                        url:'{{url('/')}}/api_v1/correios/consultaCep/'+cep.val()+'',
                        method:'GET',
                    }).done(function(response){
                        if(!$.isEmptyObject(response)){
                            $("#address").val(response.logradouro);
                            $("#neighborhood").val(response.bairro);
                            $("#city").val(response.cidade+' - '+response.uf)
                        }else{
                            toastr.error("Atenção, nenhum endereço foi encontrado para este CEP, por favor, tente novamente!",'CEP');
                        }
                    })
                }
            });




            var geocoder = new google.maps.Geocoder();
            var addr = '';
                @if(!is_null($profile->info)){
                addr = '{{$profile->info->address}} {{$profile->info->number}} {{$profile->info->adjunct}}, {{$profile->info->city}}, {{$profile->info->country->name}}';
            }
            @else
                    addr = 'Brazil';
            @endif
            geocoder.geocode({
                address: addr,
                region: 'no'
            },function(results,status){

                if (status.toLowerCase() == 'ok') {
                    var coords = new google.maps.LatLng(
                            results[0]['geometry']['location'].lat(),
                            results[0]['geometry']['location'].lng()
                    );
                    var basicMap = new GMaps({
                        div:'#basic-map',
                        lat: coords.lat(),
                        lng:coords.lng(),
                    });
                    basicMap.addMarker({
                        lat: coords.lat(),
                        lng:coords.lng(),
                        title: '{{$user->name}}',
                        infoWindow: {
                            content: '<p>'+addr+'</p>'
                        }
                    })
                }
            });
        });
    </script>
@endsection
